import { ROUTER_CHANGE } from '../../constants/constants';
import { getUserProfile } from '../actions/user';
import { push } from 'connected-react-router';
import {
    userCantGoTo,
    adminCantGoTo,
    guestCantGoto,
} from '../../constants/saveRoutes';

export default ({ dispatch, getState }) => (next) => (action) => {

    if (action.type === ROUTER_CHANGE) {
        const { pathname } = action.payload.location;
        const isUser = localStorage.getItem('TOKEN');
        const isAdmin = localStorage.getItem('ADMINTOKEN');

        if (!isUser && !isAdmin && guestCantGoto.includes(pathname)) {
            console.log('action1', action);
            dispatch(push('/login'));
        }

        if (isUser || isAdmin) {
            dispatch(getUserProfile());
        }

        if (isUser && userCantGoTo.includes(pathname)) {
            dispatch(push('/profile'));
        }
        if (isAdmin && adminCantGoTo.includes(pathname)) {
            dispatch(push('/admin/dashboard'));
        }


    }
    next(action);
};
