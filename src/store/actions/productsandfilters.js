import api from '../../sevices/exchange/index';
import { PUT_ALL_PRODUCTS } from '../../constants/constants';
import { productsRestructure } from '../../helpers/functions';
const putFilters = data => ({ type: PUT_ALL_PRODUCTS, payload: data });

export const fetchAllProductsData = () => {
    return async dispatch => {
        const {
            data: { results },
        } = await api.getAllFProducts();

        const reStructuredResult = productsRestructure(results);
        dispatch(putFilters(reStructuredResult));
    };
};
