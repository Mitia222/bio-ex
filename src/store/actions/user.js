import {
    LOGOUT,
    FETCH_USER_DATA,
    PUT_PROFILE_DATA,
    PRELOADER,
    TWO_FACTOR_STEP,
    RESET_USER_MODEL,
} from '../../constants/constants';
import api from '../../sevices/users/userRequests';
import { verifyAddress } from '../../sevices/exchange/google';
import { notification } from 'antd';
import { push, replace } from 'connected-react-router';
import { transformWarehouseStateToQuery } from '../../helpers/functions';
import moment from 'moment';
import 'moment/locale/uk';

const putProfileData = (user) => ({ type: PUT_PROFILE_DATA, payload: user });

const userPreloader = (payload) => ({ type: PRELOADER, payload });

const putUserData = (payload) => ({
    type: FETCH_USER_DATA,
    payload,
});
export const reset = () => ({ type: RESET_USER_MODEL });

export const putTwoFactor = (payload) => ({ type: TWO_FACTOR_STEP, payload });

export const Logout = () => (dispatch) => {
    if (localStorage.getItem('ADMINTOKEN')) {
        localStorage.removeItem('ADMINTOKEN');
        localStorage.removeItem('TOKENREFRESH');
    } else {
        localStorage.removeItem('TOKEN');
        localStorage.removeItem('TOKENREFRESH');
    }
    dispatch(push('/login'));
    dispatch({
        type: LOGOUT,
    });
};

const fetchUserProfileData = async () =>
    await Promise.all([api.userLoginHistory(1), api.GetUserProfile()]);

export const Registration = (payload) => {
    return async (dispatch) => {
        dispatch(userPreloader(true));
        try {
            await api.Registration(payload);
            notification.success({
                message:
                    'Лист з активацією аккаунта відправленно на Ваш email.',
            });
            dispatch(userPreloader(false));
            dispatch(push('/login'));
            // }
        } catch (e) {
            console.log('catch', e);
            dispatch(userPreloader(false));
            if (e.response) {
                notification.error({
                    message: e.response.statusText,
                    description: Object.values(e.response.data)[0],
                });
            } else {
                notification.error({
                    message: 'Помилка сервера.',
                    description: "Спробуйте пізніше"
                });
            }
        }
    };
};

export const getUserProfile = () => {
    return async (dispatch, getState) => {
        try {

            const [history, userData] = await fetchUserProfileData();

            const user = {
                ...userData.data,
                authHistory: history.data.results.map((ipHistory) => ({
                    ...ipHistory,
                    loginDatetime: moment(ipHistory.loginDatetime)
                        .locale('uk')
                        .format('L'),
                })),
            };

            dispatch(putUserData(user));

            return userData;
        } catch (e) {
            if (!e.response) dispatch(Logout())
            if (e.response && e.response.status === 401) {
                dispatch(Logout())
            }
            // notification.error({
            //     message: "asd",
            //     description: 'Помилка сервера!',
            // });
        }
    };
};

export const Login = (payload) => async (dispatch) => {
    dispatch(userPreloader(true));
    try {
        const { data } = await api.login(payload);
        const role = data.isStaff ? 'ADMINTOKEN' : 'TOKEN';

        localStorage.setItem(role, JSON.stringify(data.access));
        localStorage.setItem('TOKENREFRESH', JSON.stringify(data.refresh));

        dispatch(getUserProfile()).then(({ data }) => {
            if (data.isStaff) {
                dispatch(replace('/admin/dashboard'));
            } else {
                dispatch(replace('/profile'));
            }
        });
    } catch (e) {
        console.log('ERROR in CaTch', e.response);
        dispatch(userPreloader(false));
        if (e.response) {
            notification.error({
                message: Object.values(e.response.data)[0],
                description: 'Спробуйте ще раз!',
            });
        } else {
            notification.error({
                message: 'Помилка сервера.',
                description: "Спробуйте пізніше"

            });
        }
    }
};

export const editUserPRofile = (userData) => {
    return async (dispatch, getState) => {
        dispatch(userPreloader(true));

        try {
            const { data } = await api.editProfileInfo(userData);
            notification.success({
                message: 'Профіль оновлено!',
            });
            dispatch(putProfileData(data));
        } catch (e) {
            dispatch(userPreloader(false));
            if (e.response) {
                Object.keys(e.response.data).forEach((el) => {
                    const innerField = e.response.data[el];
                    if (
                        Array.isArray(innerField) ||
                        typeof innerField === 'string'
                    ) {
                        notification.error({
                            message: innerField || 'Error',
                        });
                    } else if (Object.keys(innerField).length > 0) {
                        Object.keys(innerField).forEach((el2) => {
                            notification.error({
                                message: innerField[el2][0],
                            });
                        });
                    } else {
                        notification.error({
                            message: 'Помилка сервера!',
                        });
                    }
                });
            } else {
                notification.error({
                    message: 'Помилка сервера!',
                });
            }
        }
    };
};

export const verifyWarehouse = (wareHouseData) => {
    return async (dispatch) => {
        // const newState = { ...wareHouseData };
        // delete newState.isVerified;
        const query = transformWarehouseStateToQuery(wareHouseData);

        try {
            const { data } = await verifyAddress(query);

            const [googleresults] = data.results;
            if (data.status === 'OK') {
                const newValues = {
                    // ...values,
                    stock: {
                        ...wareHouseData,
                        stockCoordinates: {
                            latitude: googleresults.geometry.location.lat.toFixed(
                                8,
                            ),
                            longitude: googleresults.geometry.location.lng.toFixed(
                                8,
                            ),
                        },
                        isVerified: true,
                    },
                };

                const { data: updatedData } = await api.editProfileInfo(
                    newValues,
                );
                dispatch(putProfileData(updatedData));

                notification.success({
                    message: 'Склад веріфіковано успішно!!',
                });
            } else {
                notification.error({
                    message: 'Такої адреси не знайдено.',
                });
            }
        } catch (e) {
            console.log(e);
        }
    };
};
