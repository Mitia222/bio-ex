import { TOOGLE_PRELOADER } from '../../constants/constants';

export const tooglePreloader = payload => ({ type: TOOGLE_PRELOADER, payload });
