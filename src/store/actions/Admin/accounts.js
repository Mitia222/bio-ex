import {
    PUT_ACCOUNTS
} from '../../../constants/constants';
import { onChangeFilters } from './filters';
import api from '../../../sevices/adminSide/Accounts';
import { makeQueryFromState } from '../../../helpers/functions';

const putAccounts = payload => ({ type: PUT_ACCOUNTS, payload });


export const fetchAccounts = page => async (dispatch, getState) => {
    try {
        page && dispatch(onChangeFilters('page', page));
        const filters = getState().filtersReducer;

        const quer =
            Object.keys(filters).length > 0
                ? makeQueryFromState(filters)
                : undefined;
        const newQuery = quer !== undefined && quer ? `?${quer}` : '';
        const { data } = await api.getAllAccountItems(newQuery);

        dispatch(putAccounts({ data: data.results, count: data.count }));
    } catch (e) {
        console.log(e);
    }
};
