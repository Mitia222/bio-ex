import { PUT_COMMISSIONS } from '../../../constants/constants';
import api from '../../../sevices/adminSide/commission';
import { putCommission } from '../Users/commission';
import { notification } from 'antd';
import { toogleModal } from '../modal';
import { makeQueryFromState } from '../../../helpers/functions';

export const putCommissions = payload => ({
    type: PUT_COMMISSIONS,
    payload,
});

export const fetchCommissions = filters => async (dispatch, getState) => {
    try {
        const filters = getState().filtersReducer;
        const quer =
            Object.keys(filters).length > 0
                ? makeQueryFromState(filters)
                : undefined;
        const newQuery = quer !== undefined && quer ? `?${quer}` : '';
        const { data } = await api.getCommissions(newQuery);
        dispatch(putCommissions({ count: data.count, data: data.results }));
    } catch (e) {
        console.log(e);
    }
};

export const updatecommission = amount => async dispatch => {
    try {
        const { data } = await api.updateCommission(amount);
        notification.success({
            message: 'Комісію біржі змінено!',
        });
        dispatch(putCommission(data));
        dispatch(toogleModal({ type: '', isOpen: false, data: null }));
    } catch (e) {
        console.log(e);
    }
};
// export const clearFilters = () => ({type: CLEAR_FILTERS})
