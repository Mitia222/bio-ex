import api from '../../../sevices/adminSide/users';
import {
    PUT_ALL_USERS,
} from '../../../constants/constants';
import { makeQueryFromState } from '../../../helpers/functions';

const putAllUsers = payload => ({
    type: PUT_ALL_USERS,
    payload,
});


export const getAdminUsers = () => {
    return async (dispatch, getState) => {
        try {
            const filters = getState().filtersReducer;

            const query =
                Object.keys(filters).length > 0
                    ? makeQueryFromState(filters)
                    : undefined;
            const newQuery = query !== undefined && query ? `?${query}` : '';

            const { data } = await api.getUsers(newQuery);
            dispatch(putAllUsers({ total: data.count, users: data.results }));
        } catch (e) {
            console.log(e);
        }
    };
};
