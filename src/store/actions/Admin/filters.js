import {
    ON_CHANGE_FILTERS,
    SET_INITIAL_FILTERS,
} from '../../../constants/constants';

export const onChangeFilters = (field, value) => ({
    type: ON_CHANGE_FILTERS,
    payload: { field, value },
});
export const setInitialFilters = (payload) => ({
    type: SET_INITIAL_FILTERS,
    payload: payload,
});
