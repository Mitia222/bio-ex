import api from '../../../sevices/exchange/index';
import { PUT_COMMISSION, PUT_RATING } from '../../../constants/constants';

export const putCommission = payload => ({ type: PUT_COMMISSION, payload });
const putRating = payload => ({ type: PUT_RATING, payload });
// const putCommissionError = payload => ({ type: PUT_COMMISSION_ERROR, payload });

export const fetchCommission = () => {
    return dispatch =>
        api
            .getCommission()
            .then(({ data }) => dispatch(putCommission(data)))
            .catch(err => console.log('commission error', err));
};

export const fetchRating = id => {
    return dispatch =>
        api
            .getRating(id)
            .then(({ data }) => dispatch(putRating(data)))
            .catch(err => console.log('rating error', err));
};
