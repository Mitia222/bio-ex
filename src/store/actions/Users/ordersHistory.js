import api from '../../../sevices/exchange/index';
import {
    PUT_SUCCESS_CLOSED_HISTORY,
    PUT_TRANSACTION_HISTORY,
    PUT_ALL_HISTORY,
    TOGGLE_HISTORY_LOADER,
} from '../../../constants/constants';

const putOrderHistoryData = (payload) => ({
    type: PUT_SUCCESS_CLOSED_HISTORY,
    payload,
});
const toggle_loader = (payload) => ({
    type: TOGGLE_HISTORY_LOADER,
    payload,
});
const putTransactionsHistoryData = (payload) => ({
    type: PUT_TRANSACTION_HISTORY,
    payload,
});

const putAllHistoryDta = (payload) => ({ type: PUT_ALL_HISTORY, payload });

export const getOrderHistory = () => {
    return (dispatch) =>
        api
            .getOrdersHistory()
            .then(({ data: { results } }) =>
                dispatch(putOrderHistoryData(results)),
            )
            .catch((err) => console.log(err));
};

export const getTransactionsHistory = () => {
    return (dispatch) =>
        api
            .getTransactionHistory()
            .then(({ data }) => {
                dispatch(putTransactionsHistoryData(data));
            })
            .catch((err) => console.log(err));
};

export const getAllHistroyORders = (params) => {
    return (dispatch) => {
        dispatch(toggle_loader(true));
        api.getOpenOrdersHistory(params)
            .then(({ data }) => {

                dispatch(
                    putAllHistoryDta({
                        total: 0,
                        items: data,
                    }),
                );
                dispatch(toggle_loader(false));
            })
            .catch((err) => {
                console.log(err);
                dispatch(toggle_loader(false));
            });
    };
};
