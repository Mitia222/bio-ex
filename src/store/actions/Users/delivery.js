import api from '../../../sevices/exchange/index';
import { PUT_DELIVERIES } from '../../../constants/constants';

const putDeliveryData = payload => ({ type: PUT_DELIVERIES, payload });

export const getDelivery = () => {
    return dispatch =>
        api
            .getDelivery()
            .then(({ data: { results } }) => dispatch(putDeliveryData(results)))
            .catch(err => console.log(err));
};
