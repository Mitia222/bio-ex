import { PUT_PUBLIC_NEWS } from '../../../constants/constants';
import api from './../../../sevices/adminSide/news';

const putPublicNews = (payload) => ({ type: PUT_PUBLIC_NEWS, payload });

export const fetchNews = () => async (dispatch) => {
    try {
        const {
            data: { results },
        } = await api.getNews();

        dispatch(putPublicNews(results));
    } catch (e) {
        console.log('err news', e);
    }
};
