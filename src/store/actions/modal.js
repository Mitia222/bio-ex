import { TOOGLE_MODAL } from '../../constants/constants';
import { putCurrentChoosedPoduct } from './Users/ordersActionsCreators';

export const toogleModal = payload => ({ type: TOOGLE_MODAL, payload });

export const openModal = (type, prod, data) => dispatch => {
    prod && dispatch(putCurrentChoosedPoduct(prod));
    dispatch(
        toogleModal({
            isOpen: true,
            type,
            data: data ? data : null,
        }),
    );
};
