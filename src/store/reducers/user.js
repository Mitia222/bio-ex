import {
    // FETCH_LOGIN,
    LOGIN_ERROR,
    LOGOUT,
    FETCH_USER_DATA,
    PUT_PROFILE_DATA,
    PRELOADER,
    TWO_FACTOR_STEP,
    RESET_USER_MODEL,
} from '../../constants/constants';

const initialState = {
    loading: false,
    account: null,
    authHistory: [],
    documentImages: [],
    isStuff: false,
    email: '',
    firstName: null,
    lastName: null,
    nin: null,
    organization: null,
    patronymic: null,
    phone: '',
    role: 0,
    imageBase64: null,
    stock: null,
    twoFactorAuthEnabled: false,
    verificationStatus: null,
    id: null,
    authorized: false,
    authError: false,
};

const userReducer = (state = initialState, { type, payload }) => {
    switch (type) {
        case FETCH_USER_DATA:
            return {
                ...state,
                ...payload,
                loading: false,
                authorized: true,
                authError: null,
            };
        case PUT_PROFILE_DATA:
            return { ...state, ...payload, authError: null, loading: false };
        case PRELOADER:
            return { ...state, loading: payload };
        case LOGIN_ERROR:
            return {
                ...initialState,
                authorized: false,
                loading: false,
                authError: payload,
            };
        case TWO_FACTOR_STEP:
            return {
                ...state,
                twoFactorAuthEnabled: payload.twoFactorAuthEnabled,
                imageBase64: payload.image,
                email: payload.email,
            };
        case LOGOUT:
            return {
                ...initialState,
                authorized: false,
                authError: null,
                loading: false,
            };
        case RESET_USER_MODEL:
            return initialState;
        default:
            return state;
    }
};

export default userReducer;
