import {PUT_RATING} from '../../constants/constants';

const initalState = [];

const ratingReducer = (state = initalState, { type, payload }) => {
    switch (type) {
        case PUT_RATING:
            return payload;

        default:
            return state;
    }
};

export default ratingReducer;
