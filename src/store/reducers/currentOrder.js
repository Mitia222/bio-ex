import { PUT_CHOOSED_PRODUCT } from '../../constants/constants';

const initalState = {};

const currentOrderReducer = (state = initalState, { type, payload }) => {
    switch (type) {
        case PUT_CHOOSED_PRODUCT:
            return payload;
        default:
            return state;
    }
};

export default currentOrderReducer;
