import { TOOGLE_PRELOADER } from '../../constants/constants';

const initial_state = false;

const preloader = (state = initial_state, { type, payload }) => {
    switch (type) {
        case TOOGLE_PRELOADER:
            return payload;
        default:
            return state;
    }
};

export default preloader;
