import {
    PUT_OPEN_OREDER,
    PUT_ACTIVE_ORDER,
    PUT_ONE_OPEN_ORDER_WHEN_CREATED,
    PUT_ONE_ACTIVE_ORDER_WHEN_CREATED,
    DELETE_FROM_ACTIVE,
    DELETE_FROM_OPENED,
} from '../../constants/constants';

const initalState = {
    activeOrders: [],
    openOrders: [],
    error: null,
};

const activesOrdersReducer = (state = initalState, { type, payload }) => {
    switch (type) {
        case PUT_OPEN_OREDER:
            return { ...state, openOrders: payload, error: null };
        case PUT_ACTIVE_ORDER:
            return { ...state, activeOrders: payload, error: null };
        case PUT_ONE_ACTIVE_ORDER_WHEN_CREATED:
            return {
                ...state,
                activeOrders: [payload, ...state.activeOrders],
                error: null,
            };
        case PUT_ONE_OPEN_ORDER_WHEN_CREATED:
            return {
                ...state,
                openOrders: [payload, ...state.openOrders],
                error: null,
            };
        case DELETE_FROM_ACTIVE:
            const newActiveOrders = state.activeOrders.filter(
                ord => ord.id !== payload,
            );
            return { ...state, activeOrders: newActiveOrders, error: null };
        case DELETE_FROM_OPENED:
            const newOpenOrders = state.openOrders.filter(
                ord => ord.id !== payload,
            );
            return { ...state, openOrders: newOpenOrders, error: null };
        default:
            return state;
    }
};

export default activesOrdersReducer;
