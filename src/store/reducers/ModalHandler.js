import { TOOGLE_MODAL } from '../../constants/constants';

const initalState = {
    type: '',
    isOpen: false,
    data: null,
};

const ModalReducer = (state = initalState, { type, payload }) => {
    switch (type) {
        case TOOGLE_MODAL:
            return payload;
        default:
            return state;
    }
};

export default ModalReducer;
