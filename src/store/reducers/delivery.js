import { PUT_DELIVERIES } from '../../constants/constants';

const initalState = [];

const deliveryReducer = (state = initalState, { type, payload }) => {
    switch (type) {
        case PUT_DELIVERIES:
            return payload;
        default:
            return state;
    }
};

export default deliveryReducer;
