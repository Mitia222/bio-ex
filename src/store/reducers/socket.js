import { PUT_SOCKET, REMOVE_SOCKET } from '../../constants/constants';

const initialState = null;

const orderReducer = (state = initialState, { type, payload }) => {
    switch (type) {
        case PUT_SOCKET:
            return payload;
        case REMOVE_SOCKET:
            return initialState;
        default:
            return state;
    }
};

export default orderReducer;
