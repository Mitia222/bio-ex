import { combineReducers } from 'redux';
import userReducer from './user';
import orderReducer from './orders';
import productsReducer from './products';
import socketReducer from './socket';
import activesOrdersReducer from './activeorders';
import currentOrderReducer from './currentOrder';
import allusersReducer from './Admin/allusers';
import deliveryReducer from './delivery';
import ratingReducer from './rating';
import ModalReducer from './ModalHandler';
import ordersHistoryReducer from './ordersHistory';
import contractReducer from './Admin/contracts';
import commissionReducer from './commission';
import filtersReducer from './Admin/filters';
import adminCommissionReducer from './Admin/commission';
import preloader from './preloader';
import accounts from './Admin/accounts';
import newsReducer from './news';
import { connectRouter } from 'connected-react-router';

export default (history) =>
    combineReducers({
        router: connectRouter(history),
        filtersReducer: filtersReducer,
        accounts,
        adminCommissions: adminCommissionReducer,
        user: userReducer,
        orders: orderReducer,
        products: productsReducer,
        socket: socketReducer,
        activeOrders: activesOrdersReducer,
        news: newsReducer,
        currentChoosedOrder: currentOrderReducer,
        allUsers: allusersReducer,
        delivery: deliveryReducer,
        rating: ratingReducer,
        modal: ModalReducer,
        orderHistory: ordersHistoryReducer,
        commission: commissionReducer,
        contracts: contractReducer,
        preloader,
    });
