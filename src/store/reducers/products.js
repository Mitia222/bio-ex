import { PUT_ALL_PRODUCTS } from '../../constants/constants';

const initalState = [];

const productsReducer = (state = initalState, { type, payload }) => {
    switch (type) {
        case PUT_ALL_PRODUCTS:
            return payload;
        default:
            return state;
    }
};

export default productsReducer;
