import { PUT_PUBLIC_NEWS } from '../../constants/constants';

const initial_state = [];

const newsReducer = (state = initial_state, { type, payload }) => {
    switch (type) {
        case PUT_PUBLIC_NEWS:
            return payload;
        default:
            return state;
    }
};

export default newsReducer;
