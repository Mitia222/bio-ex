import { PUT_ALL_USERS } from '../../../constants/constants';

const initalState = {
    users: [],
    total: 0,
};

const allUsersReducer = (state = initalState, { type, payload }) => {
    switch (type) {
        case PUT_ALL_USERS:
            return payload;

        default:
            return state;
    }
};

export default allUsersReducer;
