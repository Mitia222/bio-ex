import {
    ON_CHANGE_FILTERS,
    ON_SUBMIT_FILTERS_DATA,
    SET_INITIAL_FILTERS,
} from '../../../constants/constants';

const initalState = {};

const filtersReducer = (state = initalState, { type, payload }) => {
    switch (type) {
        case SET_INITIAL_FILTERS:
            return { ...payload };
        case ON_CHANGE_FILTERS:
            return payload.field !== 'page'
                ? {
                      ...state,
                      [payload.field]: payload.value,
                      page: 1,
                  }
                : {
                      ...state,
                      page: payload.value,
                  };
        // case CLEAR_FILTERS:
        //     return {}
        case ON_SUBMIT_FILTERS_DATA:
            return state;
        default:
            return state;
    }
};

export default filtersReducer;
