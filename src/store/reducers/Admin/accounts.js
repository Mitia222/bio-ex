import {
    PUT_ACCOUNTS,

} from '../../../constants/constants';

const initalState = {
        data: [],
        count: 0,
};

const accounts = (state = initalState, { type, payload }) => {
    switch (type) {

        case PUT_ACCOUNTS:
            return { data: payload.data, count: payload.count};
        default:
            return state;
    }
};

export default accounts;
