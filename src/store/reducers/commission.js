import { PUT_COMMISSION } from '../../constants/constants';

const initalState = {};

const commissionReducer = (state = initalState, { type, payload }) => {
    switch (type) {
        case PUT_COMMISSION:
            return payload;

        default:
            return state;
    }
};

export default commissionReducer;
