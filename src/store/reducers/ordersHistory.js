import {
    PUT_SUCCESS_CLOSED_HISTORY,
    PUT_TRANSACTION_HISTORY,
    PUT_ALL_HISTORY,
    TOGGLE_HISTORY_LOADER,
} from '../../constants/constants';

const initialState = {
    transactionHistory: [],
    successClosedHistory: [],
    allHistory: { items: [], total: 0, loading: false },
};

const ordersHistoryReducer = (state = initialState, { type, payload }) => {
    switch (type) {
        case TOGGLE_HISTORY_LOADER:
            return {
                ...state,
                allHistory: { ...state.allHistory, loading: payload },
            };
        case PUT_SUCCESS_CLOSED_HISTORY:
            return { ...state, successClosedHistory: payload };
        case PUT_TRANSACTION_HISTORY:
            return { ...state, transactionHistory: payload };
        case PUT_ALL_HISTORY:
            return {
                ...state,
                allHistory: { items: payload.items, total: payload.total },
            };
        default:
            return state;
    }
};

export default ordersHistoryReducer;
