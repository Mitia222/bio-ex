import { createStore, applyMiddleware } from 'redux';
import createRootReducer from './reducers/ROOT';
import ReduxThunk from 'redux-thunk';
import { composeWithDevTools } from 'redux-devtools-extension';
import { createBrowserHistory } from 'history';
import { routerMiddleware } from 'connected-react-router';
import checkPermmissions from "./middlewres/permmissions"

export const history = createBrowserHistory();

const store = createStore(
    createRootReducer(history),
    {},
    composeWithDevTools(
        applyMiddleware(
            routerMiddleware(history),
            ReduxThunk,
            checkPermmissions,
        ),
    ),
);
export default store;
