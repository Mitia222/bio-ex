import { useEffect, useState } from 'react';

const useFilters = (initialState, callback) => {
    const [values, setState] = useState(initialState);
    useEffect(() => {
        callback(values);
    }, [values]);
    const onChange = (field, value) => setState({ ...values, [field]: value });
    const reset = () => setState({ ...initialState });
    const onPageChange = (page) => setState({ ...values, page });
    return [values, onChange, reset, onPageChange];
};
export default useFilters;
