import { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { fetchNews } from '../store/actions/Users/news';
import { fetchCommission } from '../store/actions/Users/commission';
import { getDelivery } from '../store/actions/Users/delivery';
import { fetchAllProductsData } from '../store/actions/productsandfilters';
import { initSocket } from '../store/actions/Users/orders';
import PublicLayout from '../Layouts/PublicLayout';
import UserLayout from '../Layouts/UserLayout';
import AdminLayout from '../Layouts/AdminLayout';

const useInitApp = () => {
    const [role, setRole] = useState(null);
    const dispatch = useDispatch();

    const user = useSelector((state) => state.user);
    useEffect(() => {
        const userRole = localStorage.getItem('TOKEN');
        const adminRole = localStorage.getItem('ADMINTOKEN');

        if (!Boolean(userRole) && !Boolean(adminRole)) {
            setRole('guest');
            dispatch(fetchNews());
        }

        if (user.authorized) {

            dispatch(fetchCommission());
            dispatch(getDelivery());
            dispatch(fetchAllProductsData());
            if (user.isStaff) {
                setRole('admin');
            } else {
                dispatch(fetchNews());
                dispatch(initSocket());
                setRole('user');
            }
        }
    }, [user.authorized]);

    const Layout =
        role === 'guest'
            ? PublicLayout
            : role === 'user'
            ? UserLayout
            : role === 'admin'
            ? AdminLayout
            : null;
    return [Layout, role];
};
export default useInitApp;
