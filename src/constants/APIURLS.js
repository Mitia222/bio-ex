const BASE_URL_VIA_ENV = /dev|localhost|herokuapp/.test(window.location.origin)
    ? 'api.dev.bioenergyhub.com.ua'
    : 'api.bioenergyhub.com.ua';
export const BASE_URL = `https://${BASE_URL_VIA_ENV}/api/v1/`;

// https://6bd5a2ace04c.ngrok.io
export const SOCKET_URL = `wss://${BASE_URL_VIA_ENV}/chat/stream/`;

export const BASE_IMG_URL = `https://${BASE_URL_VIA_ENV}`;

//USER
export const LOGIN = 'users/login/';
export const REGISTRATION = 'users/register/';
export const PASSWORD_CHANGE = 'users/password_change/';
export const PASSWORD_RESET = 'users/password_reset/';
export const COUNTRIES = 'countries';
export const REGIONS = 'location/admin';
export const USER_PROFILE = 'users/profile/';
export const USER_LOGIN_HISTORY = 'users/audit_entry/';
export const USER_TWO_FACTOR = 'users/check_two_factor/';
export const USER_TWO_FACTOR_VALIDATE = 'users/qrcode/';
export const USER_PROFILE_TWO_FACTOR = 'users/update_two_factor/';
export const GET_USER_BALANCE = 'users/balance_info/';
export const REFRESH_TOKEN = 'users/token_refresh/';

//ORDERS

export const ORDERS = 'orders';
export const MY_OPEN_ORDERS = 'orders/me';
export const TRADE_HISTORY = 'orders/me';
export const DELIVERY = 'delivery/';
export const COMMISSION = 'payments/get_commission/';
export const CREATE_RATING = 'orders/create_ratings/';

// adminSide

export const ALL_NEWS = 'admins/news/';
export const LEGALS = 'admins/legal_data/';
export const ALL_USERS = 'admins/users/';
export const ADMINS = 'admins/access_rights/';
export const DASHBOARD_STATISCTIC = 'admins/dashboard/';
export const CONTRACTS_STATISTIC = 'admins/contracts_statistic/';
export const CONTRACTS = 'admins/contracts/';
export const UPDATE_USER_BALANCE = 'admins/update_user_balance/';
export const CHANGE_USER_STATUS = 'admins/change_user_status/';
export const MAIN_REGISTRATION_STATUS = 'admins/get_register/';
export const UPDATE_MAIN_REGISTER = 'admins/change_register/';
export const COMMISSIONS = 'admins/balance_transactions/';
export const UPDATE_COMMISSIONS = 'admins/update_commission/';
export const GET_MIN_MAX_FILTERS_VALUES = 'admins/filters_values/';

export const ALL_PRODUCTS = 'products/';
export const UNITS = 'products/units_to_category/';
export const ORDER_HISTORY = 'orders/history/';
export const ALL_HISTORY = 'orders/orders_history/';
export const TRANSACTION_HISTORY = 'payments/get_transactions/';
export const INVOICE = 'payments/generate_invoice/';
export const DOWNLOAD_CONTRACT = 'orders/download_contract/';
export const BILLSLIST = 'admins/bill_transactions/';
export const UPDATE_STATUS = 'admins/update_bill_status/';
export const BILL_STATISTIC = 'admins/bill_statistic/';
