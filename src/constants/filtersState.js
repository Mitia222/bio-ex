export default {
    '/admin/contracts': {
        id: null,
        category: null,
        status: null,
        date: null,
        distance_min: 0,
        distance_max: 1000,
        delivery_price_min: 0,
        delivery_price_max: 0,
        total_min: 0,
        total_max: 1000,
        page: 1,
    },

    '/admin/users': {
        search: null,
        verification_status: null,
        is_active: null,
        date: null,
        page: 1,
    },
    '/admin/commissions': {
        search: null,
        page: 1,
        type: null,
        created: null,
        commission_min: 0,
        commission_max: null,
    },
    '/admin/accounts': {
        search: null,
        page: 1,
        status: null,
        date: null,

    }
};
