export const organizationMap = [
    {
        id: '1',
        name: 'ТОВ',
    },
    {
        id: '2',
        name: 'ПП',
    },
];
export const orgLeader = [
    {
        id: '1',
        name: 'Директор',
    },
    {
        id: '2',
        name: 'Зам.Директора',
    },
    {
        id: '3',
        name: 'Виконавець',
    },
];
