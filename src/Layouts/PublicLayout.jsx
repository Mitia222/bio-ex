import React, { Fragment } from 'react';
import Header from '../components/Header/Header';
import Footer from '../components/Footer';

const PublicLayout = ({ children }) => {
    return (
        <Fragment>

            <Header roles="public" />
            {children}

            <Footer />
        </Fragment>
    );
};

export default PublicLayout;
