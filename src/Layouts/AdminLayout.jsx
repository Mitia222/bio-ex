import React from 'react';
import Header from '../containers/AdministratorSide/components/Header/Header';
import NavBar from '../containers/AdministratorSide/components/NavBar/NavBar';

const AdminLayout = ({ children }) => {
    return (
        <div className="admin-side">
            <Header />

            <div className="admin-size-container container">
                <NavBar />

                <div className="admin-content">{children}</div>
            </div>
        </div>
    );
};

export default AdminLayout;
