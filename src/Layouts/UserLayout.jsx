import React, { Fragment } from 'react';
import Header from '../components/Header/Header';
import Footer from '../components/Footer';

const UserLayout = ({ children }) => {
    return (
        <Fragment>

            <Header roles="user" />
            <div className="size-container content">{children}</div>
            <Footer />
        </Fragment>
    );
};

export default UserLayout;
