import React, { memo } from 'react';


const MemoizedRoute = memo(({ component: Component, ...rest }) => {
    return <Component {...rest} />;
});
export default MemoizedRoute;
