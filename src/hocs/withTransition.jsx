import React from 'react';

const withTransition = (Component, transitionName) => (props) => {
    const className = transitionName ? transitionName : 'start-transition';
    return (
        <div className={className}>
            <Component {...props} />
        </div>
    );
};

export default withTransition;
