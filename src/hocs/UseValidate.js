import { useState } from 'react';
import api from '../sevices/adminSide/admins';
import { notification } from 'antd';

export const useValidate = (initialState, type, successCallback, data) => {
    const [state, changeState] = useState(initialState);
    function handleChange(e) {
        changeState({
            ...state,
            [e.target.name]: e.target.value,
        });
    }
    function validate() {
        let err = {};
        Object.keys(state).forEach(field => {
            if (!state[field].trim()) {
                err[field] = `Помилка при вводі ${field}!`;
            }
        });
        if (state.password !== state.confirmPassword) {
            err.confirm = 'Паролі не співпадають!';
        }
        return err;
    }
    function createAdminSucces() {
        successCallback();
        type === 'admins-update' && data.update();
        notification.success({
            message:
                type === 'admins-create'
                    ? 'Адміністратор створенний!'
                    : 'Дані відредагованно!',
        });
    }
    function errorHandler(err) {
        if (err.response) {
            Object.keys(err.response.data).forEach(er => {
                notification.error({
                    message: err.response.data[er][0],
                });
            });
        }
    }
    function handleCreateAdmin() {
        const validateErr = validate();
        const err = Object.keys(validateErr);

        if (err.length === 0) {
            if (type === 'admins-create') {
                api.createAdmin(state)
                    .then(createAdminSucces)
                    .catch(errorHandler);
            } else {
                api.updateAdmin(data.id, state)
                    .then(createAdminSucces)
                    .catch(errorHandler);
            }
        } else {
            err.forEach(errfield => {
                notification.error({
                    title: 'Помилка при валідацій!',
                    message: validateErr[errfield],
                });
            });
        }
    }
    return { state, handleChange, handleCreateAdmin };
};
