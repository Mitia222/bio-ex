import { useState } from 'react';

export const useFilters = initialState => {
    const [state, setState] = useState(initialState);

    const onChange = (field, value) => {
        setState({ ...state, [field]: value });
    };
    const resetFilters = () => {
        setState(initialState);
    };
    return { state, onChange, resetFilters };
};
