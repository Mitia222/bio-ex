import React, { useEffect, useState } from 'react';
import LoginForm from './LoginForm';
import api from '../../sevices/exchange';
import { Login } from '../../store/actions/user';
import { connect } from 'react-redux';
import { Modal, Icon } from 'antd';
import { getUserProfile, reset } from '../../store/actions/user';

import TopBlock from './TopBlock';

const LoginPage = ({
    Login,
    user,
    history,
    location: { pathname },
}) => {
    const [visible, toogleModal] = useState(false);
    const [status, setStatus] = useState(true);
    let timeout;
    // const match = useRouteMatch();
    const handleCancel = () => {
        clearTimeout(timeout);
        history.replace('/login');
        toogleModal(false);
    };
    const confirm = async () => {
        const splitedUrl = pathname
            .split('/')
            .filter((el) => el && el !== 'login');
        const url = splitedUrl.join('/') + '/';

        try {
            const { data } = await api.registrationConfirmation(url);
            setStatus(data.verified);
        } catch (e) {
            console.log(e);
        }
    };
    useEffect(() => {
        const splitedUrl = pathname.split('/').filter((el) => el && el);
        if (splitedUrl.length > 1) {
            confirm();
            toogleModal(true);
            timeout = setTimeout(() => {
                handleCancel();
            }, 4000);
        }
    }, []);

    return (
        <div className="login-page separate-form">
            <TopBlock title="Вхід" />
            <LoginForm
                login={Login}
                // isLoggedIn={isLoggedIn}
                loading={user.loading}
            />
            <Modal
                title={false}
                visible={visible}
                footer={false}
                centered
                // onOk={this.handleOk}
                onCancel={handleCancel}
            >
                <div className="confirm-registration">
                    <p>
                        {status
                            ? 'Ваш аккаунт верифіковано!'
                            : 'Посилання невалідне.'}
                    </p>
                    {status ? (
                        <Icon className="success" type="check-circle" />
                    ) : (
                        <Icon className="error" type="minus-circle" />
                    )}
                </div>
            </Modal>
        </div>
    );
};

const mapStateToProps = (state) => ({
    user: state.user,
});

const mapDispatchToProps = (dispatch) => ({
    Login: (user, history) => dispatch(Login(user, history)),
    getUserProfile: () => dispatch(getUserProfile()),
    reset: () => dispatch(reset()),
});

export default connect(mapStateToProps, mapDispatchToProps)(LoginPage);
