import React, { Fragment } from 'react';
import { Icon } from 'antd';
import logo from '../../assets/img/logo_head.svg';

export default ({title}) => {
    return (
        <Fragment>
            <div className="go-back-btn" onClick={() => window.history.back()}>
                <Icon type="left" />
                Назад
            </div>

            <div className="logo">
                <img src={logo} alt="" />
            </div>

            <div className="form-title">
                <hr />
                <span>{title}</span>
                <hr />
            </div>
        </Fragment>
    );
};
