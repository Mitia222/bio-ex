import React  from 'react';
import { withFormik } from 'formik';
import { LoginSchema } from '../../schema/user';
import { Form } from 'formik';
import { NavLink } from 'react-router-dom';
import Input from '../../components/Input';
import Recaptch from '../../components/Recaptcha';
// import Recaptcha from 'react-recaptcha';

const LoginForm = ({
    errors,
    setFieldValue,
    loading,
    setTouched,
    touched,
    values,
}) => {
    const wrappedHandleChange = ({ target: { value, name } }) => {
        setTouched({ ...touched, [name]: true });
        setFieldValue(name, value);
    };
    return (
        <Form>
            <Input
                label="Ваш Email"
                type="email"
                name="email"
                handleChange={wrappedHandleChange}
                value={values.email}
                withCached
                placeholder="Наприклад: example@gmail.com"
                errors={touched.email && errors.email}
            />
            <Input
                label="Ваш пароль"
                type="password"
                name="password"
                handleChange={wrappedHandleChange}
                placeholder="*******"
                errors={touched.password && errors.password}
            />

            <div className="go-to-reset-pas">
                <NavLink to="/reset_password">Забули пароль?</NavLink>
            </div>
            <Recaptch
                verifyCallback={(e) => {
                    setTouched({ ...touched, captch: true });
                    setFieldValue('captch', true);
                }}
            />
            {touched.captch && errors.captch && (
                <div className="inputs-error-text">{errors.captch}</div>
            )}
            <button
                type="submit"
                className="btn authentication-action-btn"
                disabled={loading}
            >
                Увійти
            </button>
            <div className="go-to-login">
                <NavLink to="/registration">
                    У мене ще немає облікового запису
                </NavLink>
            </div>
        </Form>
    );
};

export default withFormik({
    mapPropsToValues: () => ({
        email: '',
        password: '',
        captch: true,
    }),

    handleSubmit: (values, { props: { login } }) => {
        login({ email: values.email, password: values.password });
    },
    validationSchema: LoginSchema,
    displayName: 'LoginForm',
})(LoginForm);
