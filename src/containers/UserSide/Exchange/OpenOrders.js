import React, { useState, useEffect } from 'react';
import { Table } from 'antd';
import { useSelector } from 'react-redux';
import columns from '../../../helpers/columns';
import { connect } from 'react-redux';
// import moment from 'moment';

const OpenOrders = ({ openOrders, socket }) => {
    const userEmail = useSelector((state) => state.user.email);
    const declineOpenOrder = (id) => {
        socket.send(
            JSON.stringify({
                command: 'open',
                action: 'unsubscribe',
                message: {
                    order_id: id,
                },
            }),
        );
    };
    const acceptOpenOrder = (id) => {
        socket.send(
            JSON.stringify({
                command: 'active',
                action: 'subscribe',
                message: {
                    order_id: id,
                },
            }),
        );
    };

    const [loading, toogleLoader] = useState(true);

    useEffect(() => {
        if (socket) {
            toogleLoader(false);
        }
    }, [socket]);
    return (
        <div className="open-orders">
            <div className="user-table">
                <div className="title-order-type table-title">
                    Відкриті замовлення
                </div>

                <Table
                    rowKey={(record) => record.id}
                    loading={loading}
                    dataSource={openOrders}
                    scroll={{ x: 1050 }}
                    columns={columns.openOrdersColumns(
                        declineOpenOrder,
                        acceptOpenOrder,
                        userEmail,
                    )}
                    pagination={false}
                />
            </div>
        </div>
    );
};

const mapStatetoProps = (state) => ({
    openOrders: state.activeOrders.openOrders,
    socket: state.socket,
});

export default connect(mapStatetoProps)(OpenOrders);
