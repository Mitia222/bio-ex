import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { openModal } from '../../../store/actions/modal';
import OrdersList from './OrdersList';


const EchangeTables = ({ buyOrders, showModal, sellOrders, socket, user }) => {
    const [loading, toogleLoader] = useState(true);
    const handleBestDeal = (val, type) => {
        if (val) {
            socket.send(
                JSON.stringify({
                    command: 'best_deal',
                    message: {
                        depth: type === 'buy' ? '1' : '2',
                    },
                }),
            );
        } else {
            socket.send(
                JSON.stringify({
                    command: 'join',
                    depth: type === 'buy' ? '1' : '2',
                }),
            );
        }
    };
    useEffect(() => {
        if (socket) {
            toogleLoader(false);
        }
    }, [socket]);

    return (
        <div className="order-block">

            <OrdersList
                loading={loading}
                type="buy"
                user={user}
                data={buyOrders}
                onOpenCreateWindow={showModal}
                handleBestDeal={handleBestDeal}
            />

            <OrdersList
                loading={loading}
                type="sell"
                user={user}
                data={sellOrders}
                onOpenCreateWindow={showModal}
                handleBestDeal={handleBestDeal}
            />
        </div>
    );
};

const mapStateToProps = (state) => ({
    buyOrders: state.orders.buyOrders,
    sellOrders: state.orders.sellOrders,
    user: state.user,
    socket: state.socket,
});

const mapDispatchToPropt = (dispatch) => ({
    showModal: (type, prod) => dispatch(openModal(type, prod)),
});

export default connect(mapStateToProps, mapDispatchToPropt)(EchangeTables);
