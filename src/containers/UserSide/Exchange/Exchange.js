import React from 'react';
import Filters from './Filters';
import OpenOrders from './OpenOrders';

import EchangeTables from './EchangeTables';

const Exchange = () => {
    return (
        <div className="exchange-page wr-padding">
            <Filters />
            <OpenOrders />
            <EchangeTables />
        </div>
    );
};

export default Exchange;
