import React, { useState, useEffect } from 'react';
import { Cascader } from 'antd';
import regions from '../../../constants/regions';
import { connect } from 'react-redux';
// import { fetchAllProductsData } from '../../../store/actions/productsandfilters';
import api from '../../../sevices/exchange/index';
import CustomSelect from '../../CustomSelect';
import { sliceFalseValuesFromList } from '../../../helpers/functions';

const moneyForm = [
    {
        name: 'З НДС',
        id: '1',
    },
    {
        name: 'БЕЗ НДС',
        id: '0',
    },
];

const Filters = ({ fetchForProducts, products, socket }) => {
    //to exclude first socket join on mount
    const [renderCount, setRenderCount] = useState(0);
    const [raw, setRaw] = useState([]);

    const [units, setUnit] = useState({
        currentUnit: undefined,
        unitsOptions: [],
    });

    const [region, setRegion] = useState('');
    const [pdv, setPdv] = useState('');

    const handleSocket = (filter, reset) => {
        if (reset && socket.readyState === 1) {
            socket.send(
                JSON.stringify({
                    command: 'join',
                }),
            );
        } else {
            socket.send(
                JSON.stringify({
                    command: 'filter',
                    message: filter,
                }),
            );
        }
    };

    // useEffect(() => {
    //     if (!products.length) {
    //         fetchForProducts();
    //     }
    // }, []);

    useEffect(() => {
        const [productId, rawMaterialId, categoryId] = raw;
        const val = {
            productId,
            rawMaterialId,
            categoryId,
            units: raw.length !== 0 ? units.currentUnit : null,
            region,
            pdv: pdv === '1' ? true : pdv === '0' ? false : null,
        };
        const sliceFalseValues = sliceFalseValuesFromList(val);

        if (socket) {
            if (Object.keys(sliceFalseValues).length !== 0) {
                handleSocket(sliceFalseValues);
            } else {
                renderCount > 0 && handleSocket(null, 'reset');
            }
        }
        setRenderCount(renderCount + 1);
    }, [raw, units.currentUnit, region, pdv]);

    const handleUnitsChange = (e) => {
        setUnit({ ...units, currentUnit: e });
    };

    const handleFiltersChange = async (e) => {
        setRaw(e);
        if (e.length > 0) {
            const { data } = await api.getUnits(e[2]);
            setUnit({ currentUnit: null, unitsOptions: data });
        }
    };
    return (
        <div className="exchange-filters-block">
            <div className="size-container">
                <div className="form-item">
                    <label htmlFor="">Сировина</label>
                    <Cascader
                        options={products}
                        fieldNames={{
                            label: 'name',
                            value: 'id',
                            children: 'children',
                        }}
                        onChange={handleFiltersChange}
                        placeholder="обрати"
                        displayRender={(label) => label.join(', ')}
                    />
                </div>
                <CustomSelect
                    options={regions}
                    placeholder="обрати"
                    value={region || undefined}
                    allowClear
                    label="Локація"
                    handleChange={(val) => setRegion(val)}
                />

                <CustomSelect
                    options={units.unitsOptions}
                    handleChange={handleUnitsChange}
                    label="Одиниця виміру / Вид фасування"
                    placeholder="обрати"
                    allowClear
                    value={units.currentUnit || undefined}
                    disabled={raw.length < 1}
                    tooltipTitle="Необхідно обрати сировину!"
                />

                <CustomSelect
                    options={moneyForm}
                    placeholder="обрати"
                    value={pdv || undefined}
                    allowClear
                    handleChange={(val) => setPdv(val)}
                    label="Форма розрахунку"
                />
            </div>
        </div>
    );
};

const mapStateToProps = (state) => ({
    products: state.products,
    socket: state.socket,
});
// const mapDispatchToProps = dispatch => ({
//     fetchForProducts: () => dispatch(fetchAllProductsData()),
// });

export default connect(
    mapStateToProps,
    // mapDispatchToProps,
)(Filters);
