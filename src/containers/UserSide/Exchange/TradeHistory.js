import React, { useEffect } from 'react';
import { Table } from 'antd';
import columns from '../../../helpers/columns';
import { connect } from 'react-redux';
import { getOrderHistory } from '../../../store/actions/Users/ordersHistory';

const TradeHistory = ({
    successClosedHistory = [],
    getOrderHistory = () => {},
}) => {
    useEffect(() => {
        getOrderHistory();
    }, []);
    console.log(successClosedHistory)
    return (
        <div className="trade-history">
            <div className="user-table">
                <div className="title-order-type table-title">
                    Історія торгів
                </div>

                <Table
                    rowKey={record => record.id}
                    dataSource={successClosedHistory}
                    columns={columns.tradeHistoryColumn}
                    pagination={false}
                />
            </div>
        </div>
    );
};

const mapStateToProps = state => ({
    successClosedHistory: state.orderHistory.successClosedHistory,
});
const mapDispatchToProps = dispatch => ({
    getOrderHistory: () => dispatch(getOrderHistory()),
});

export default connect(
    mapStateToProps,
    mapDispatchToProps,
)(TradeHistory);
