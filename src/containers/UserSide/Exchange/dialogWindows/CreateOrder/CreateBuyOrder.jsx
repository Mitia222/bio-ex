import React from 'react';
import CustomSelect from '../../../../CustomSelect';
import { Icon } from 'antd';
import Input from '../../../../../components/Input';
import Checkbox from '../../../../../components/Checkbox';
import Preloader from '../../../../../components/Preloader';

const CreateBuyOrder = ({
    handleCreate,
    order,
    products,
    handleChangeInput,
    selectedProduct,
    categories,
    units,
    sum,
    minUnitChoice,
    balance,
    loading,
    errors
}) => {

    return (
        <form className="create-order buy" onSubmit={handleCreate}>
            {loading && <Preloader inner />}
            <div className="window-title">Створення замовлення на купівлю</div>

            <div className="modal-form">
                <div className="first-column column">
                    <CustomSelect
                        handleChange={e => handleChangeInput('productId', e)}
                        value={order.productId || undefined}
                        options={products}
                        label="Продукт"
                        placeholder="Обрати"
                        errors={errors && errors.productId}
                    />

                    <CustomSelect
                        handleChange={e => handleChangeInput('rawId', e)}
                        value={order.rawId || undefined}
                        placeholder="Обрати"
                        disabled={!order.productId}
                        tooltipTitle="Необхідно обрати продукт."
                        options={
                            selectedProduct ? selectedProduct.children : []
                        }
                        label="Матеріал"
                        errors={errors && errors.rawId}
                    />

                    <CustomSelect
                        handleChange={e => handleChangeInput('categoryId', e)}
                        value={order.categoryId || undefined}
                        disabled={!order.rawId}
                        placeholder="Обрати"
                        tooltipTitle="Необхідно обрати матеріал."
                        options={categories}
                        label="Категорія"
                        errors={errors && errors.categoryId}
                    />

                    <CustomSelect
                        handleChange={e => handleChangeInput('unitId', e)}
                        value={order.unitId || undefined}
                        placeholder="Обрати"
                        tooltipTitle="Необхідно обрати категорію."
                        disabled={!order.categoryId}
                        options={units}
                        label="Одиниця виміру / Вид фасування"
                        errors={errors && errors.unitId}
                    />

                    <Input
                        label={`Об'єм ${minUnitChoice &&
                            '(' + minUnitChoice.shortName + ')'}`}
                        placeholder="-"
                        type="number"
                        // min="0"
                        value={order.volume}
                        handleChange={({ target: { value } }) => {
                            +value >= 0 && handleChangeInput('volume', value);
                        }}
                        errors={errors && errors.volume}
                    />

                    <Input
                        label="Вартість, грн за одиницю товару"
                        placeholder="-"
                        name="price"
                        // min="0"
                        type="number"
                        value={order.price}
                        handleChange={({ target: { value } }) =>
                            +value >= 0 && handleChangeInput('price', value)
                        }
                        errors={errors && errors.price}
                    />

                    <Input
                        label="Максимальна вартість доставки"
                        name="deliveryPrice"
                        type='number'
                        // min="0"
                        placeholder="-"
                        values={order.deliveryPrice}
                        handleChange={({ target: { value } }) =>
                            +value >= 0 &&handleChangeInput('deliveryPrice', value)
                        }
                    />
                    <Checkbox
                        placeholder="-"
                        label="Податок на додану вартість, (ПДВ)"
                        checked={order.pdv}
                        handleChange={({ target: { checked } }) =>
                            handleChangeInput('pdv', checked)
                        }
                    />
                </div>

                <div className="second-column column">
                    <Input
                        placeholder="-"
                        label="Розмір комісії біржі"
                        disabled
                        value={+sum.toFixed(2) + 'грн'}
                        className="commission"
                    />
                    <span>На вашому рахунку наразі {balance} грн</span>
                    {minUnitChoice && (
                        <div style={{ color: '#53be4f', marginTop: 30 }}>
                            Рекомендованний обьем продукції{' '}
                            {minUnitChoice.minVolume.join(' або ')}{' '}
                            {minUnitChoice.shortName}
                        </div>
                    )}
                </div>
            </div>

            <button type="submit" className="btn">
                Підтвердити <Icon type="arrow-right" />
            </button>
        </form>
    );
};

export default CreateBuyOrder;
