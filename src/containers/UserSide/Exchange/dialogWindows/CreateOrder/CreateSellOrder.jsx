import React from 'react';
import CustomSelect from '../../../../CustomSelect';
import { Icon } from 'antd';
import UploadData from './UploadData';
import Input from '../../../../../components/Input';
import Checkbox from '../../../../../components/Checkbox';
import Preloader from '../../../../../components/Preloader';

const CreateSellOrder = ({
    handleCreate,
    order,
    products,
    handleChangeInput,
    selectedProduct,
    categories,
    units,
    onDrop,
    cert,
    photo,
    sum,
    certObjForDisplayName,
    minUnitChoice,
    balance = 0,
    loading,
    errors,
}) => {
    return (
        <div className="create-order sell">
            {loading && <Preloader inner />}
            <div className="window-title">Створення запиту на продаж</div>

            <div className="modal-form">
                <div className="first-column column">
                    <CustomSelect
                        options={products}
                        value={order.productId || undefined}
                        label="Продукт"
                        placeholder="обрати"
                        handleChange={(e) => handleChangeInput('productId', e)}
                        errors={errors && errors.productId}
                    />

                    <CustomSelect
                        disabled={!order.productId}
                        options={
                            selectedProduct ? selectedProduct.children : []
                        }
                        tooltipTitle="Необхідно обрати продукт."
                        value={order.rawId || undefined}
                        placeholder="обрати"
                        label="Матеріал"
                        handleChange={(e) => handleChangeInput('rawId', e)}
                        errors={errors && errors.rawId}
                    />

                    <CustomSelect
                        disabled={!order.rawId}
                        tooltipTitle="Необхідно обрати матеріал."
                        options={categories}
                        placeholder="обрати"
                        value={order.categoryId || undefined}
                        label="Категорія"
                        handleChange={(e) => handleChangeInput('categoryId', e)}
                        errors={errors && errors.categoryId}
                    />

                    <CustomSelect
                        disabled={!order.categoryId}
                        options={units}
                        tooltipTitle="Необхідно обрати категорію."
                        value={order.unitId || undefined}
                        placeholder="обрати"
                        label="Одиниця виміру / Вид фасування"
                        handleChange={(e) => handleChangeInput('unitId', e)}
                        errors={errors && errors.unitId}
                    />

                    <Input
                        label={`Об'єм ${
                            minUnitChoice && '(' + minUnitChoice.shortName + ')'
                        }`}
                        // name="volume"
                        type="number"
                        placeholder="-"
                        // min="0"
                        value={order.volume}
                        handleChange={({ target: { value } }) => {
                            +value >= 0 && handleChangeInput('volume', value);
                        }}
                        errors={errors && errors.volume}
                    />
                    <Input
                        label="Вартість, грн за одиницю товару"
                        type="number"
                        name="price"
                        // min="0"
                        placeholder="-"
                        value={order.price}
                        handleChange={({ target: { value } }) =>
                            +value >= 0 && handleChangeInput('price', value)
                        }
                        errors={errors && errors.price}
                    />
                    <Checkbox
                        label="Податок на додану вартість, (ПДВ)"
                        checked={order.pdv}
                        handleChange={({ target: { checked } }) =>
                            handleChangeInput('pdv', checked)
                        }
                    />
                    {/* <Input
                        type="checkbox"
                        label="ПДВ"
                        name="pdv"
                        // values={order.price}
                        handleChange={({ target: { checked } }) =>
                            handleChangeInput('pdv', checked)
                        }
                    /> */}
                </div>

                <UploadData
                    minUnitChoice={minUnitChoice}
                    certObjForDisplayName={certObjForDisplayName}
                    onDrop={onDrop}
                    photo={photo}
                    cert={cert}
                />
            </div>

            <div className="total-price">
                {/* <Input
                    label="Розмір комісії біржі"
                    className="commission"
                    type="text"
                    name="price"
                    disabled
                    value={sum.toFixed(2) + 'грн'}
                    // handleChange={({ target: { value } }) =>
                    //     handleChangeInput('price', value)
                    // }
                /> */}
                <span>На вашому рахунку наразі {balance} грн</span>
            </div>

            <button className="btn" onClick={handleCreate}>
                Підтвердити <Icon type="arrow-right" />
            </button>
        </div>
    );
};

export default CreateSellOrder;
