import React from 'react';
import { Icon } from 'antd';
import Dropzone from 'react-dropzone';

const UploadData = ({
    minUnitChoice,
    onDrop,
    photo,
    cert,
    certObjForDisplayName,
}) => {
    return (
        <div className="second-column column">
            <div className="select-image">
                <label htmlFor="">Зображення товару</label>
                <Dropzone onDrop={e => onDrop(e, 'photo')}>
                    {({ getRootProps, getInputProps, isDragActive }) => {
                        return (
                            <div {...getRootProps()}>
                                <input {...getInputProps()} />

                                <div
                                    className={`default-img ${
                                        photo ? 'active' : ''
                                    }`}
                                >
                                    {photo ? (
                                        <img src={photo} alt="asd" />
                                    ) : (
                                        <Icon type="camera" theme="filled" />
                                    )}
                                </div>

                                <button className="btn drop-btn">
                                    <span>Обрати файл</span>
                                </button>
                            </div>
                        );
                    }}
                </Dropzone>
            </div>

            <div className="select-file">
                <label htmlFor="">Додати сертифікат</label>

                <div className="dropzone-block">
                    <Icon type="file-pdf" />
                    <Dropzone onDrop={e => onDrop(e, 'cert')}>
                        {({ getRootProps, getInputProps, isDragActive }) => {
                            return (
                                <div {...getRootProps()}>
                                    <input {...getInputProps()} />
                                    <button className="btn drop-btn">
                                        <span>
                                            {certObjForDisplayName
                                                ? 'Завантаженно!'
                                                : 'Обрати файл'}
                                        </span>
                                    </button>
                                </div>
                            );
                        }}
                    </Dropzone>
                </div>
            </div>
            {certObjForDisplayName && (
                <div className="cert-name">{certObjForDisplayName.name}</div>
            )}
            {minUnitChoice && (
                <div style={{ color: '#53be4f', marginTop: 30 }}>
                    Рекомендованний обьем продукції{' '}
                    {minUnitChoice.minVolume.join(' або ')}{' '}
                    {minUnitChoice.shortName}
                </div>
            )}
        </div>
    );
};

export default UploadData;
