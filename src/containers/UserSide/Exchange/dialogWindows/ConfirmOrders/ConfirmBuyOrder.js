import React from 'react';
import { Icon, notification } from 'antd';

const ConfirmBuyOrder = ({
    step,
    nextStep,
    deliveryprice,
    data,
    user,
    confirmOrder,
    unitsName,
}) => {
    console.log('data',step, data);
    const validate = () => {
        if (user.stock && user.stock.isVerified) {
            nextStep(1);
        } else {
            notification.warning({
                message:
                    'Для прийняття ордеру необхідно веріфікувати склад у вашому профілі!',
            });
        }
    };

    if (step === 0) {
        return (
            <div className="buy-order confirm-order">
                <div className="window-title">
                    {data.applicationData.category.name + ' '+ data.applicationData.product.name}

                    {/*{data.applicationData.product.name}*/}
                </div>



                <div className="order-information">

                    <div className="item">
                        <div className="title">Ціна:</div>

                        <div className="value">{data.price}</div>
                    </div>
                    <div className="item">
                        <div className="title">Обсяг:</div>

                        <div className="value">
                            {data.applicationData.purchaseVolume} {unitsName}
                        </div>
                    </div>
                    <div className="item">
                        <div className="title">Товар:</div>

                        <div className="value">
                            {data.applicationData.product.name}
                        </div>
                    </div>
                    <div className="item">
                        <div className="title">Сировина:</div>

                        <div className="value">
                            {data.applicationData.rawMaterial.name}
                        </div>
                    </div>
                    <div className="item">
                        <div className="title">Одиниця виміру:</div>

                        <div className="value">
                            {data.applicationData.units.name}
                        </div>
                    </div>
                    <div className="item">
                        <div className="title">Відстань:</div>

                        <div className="value">
                            {data.distance && data.distance.text}
                        </div>
                    </div>
                    <div className="item">
                        <div className="title">
                            Орієнтовна вартість доставки:
                        </div>

                        <div className="value">{deliveryprice} грн</div>
                    </div>
                </div>

                <div className="price">{data.sum} грн</div>

                <button className="btn" onClick={validate}>
                    Прийняти <Icon type="arrow-right" />
                </button>
            </div>
        );
    } else if (step === 1) {
        return (
            <div className="confirm-order">
                <h1>Підтвердження адреси складу</h1>
                <div className="order-information" style={{ marginBottom: 40 }}>
                    <div className="item">
                        <div className="title">Місто</div>

                        <div className="value">
                            {user.stock ? user.stock.stockCity : '-'}
                        </div>
                    </div>
                    <div className="item">
                        <div className="title">Область</div>

                        <div className="value">
                            {user.stock ? user.stock.stockRegion : '-'}
                        </div>
                    </div>
                    <div className="item">
                        <div className="title">Країна</div>

                        <div className="value">
                            {user.stock ? user.stock.stockCountry : '-'}
                        </div>
                    </div>
                    <div className="item">
                        <div className="title">Вулиця</div>

                        <div className="value">
                            {user.stock
                                ? `${user.stock.stockStreet}, ${
                                      user.stock.stockBuilding
                                  } `
                                : '-'}
                        </div>
                    </div>
                </div>
                <button
                    className="btn"
                    onClick={() =>
                        confirmOrder(
                            data.id,
                            data.email,
                            data.isOpen,
                            deliveryprice,
                        )
                    }
                >
                    Підтвердити <Icon type="arrow-right" />
                </button>
            </div>
        );
    }
};

export default ConfirmBuyOrder;
