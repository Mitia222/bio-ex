import React from 'react';
import ConfirmBuyOrder from './ConfirmBuyOrder';
import ConfirmSellOrder from './ConfirmSellOrder';
import { notification } from 'antd';
import { connect } from 'react-redux';
import { deliveryService } from '../../../../../helpers/deliveryService';
import { checkInActiveOrders } from '../../../../../helpers/functions';

const ConfirmOrder = ({
    delivery,
    modalType,
    step,
    nextStep,
    close,
    currentChoosedOrder,
    socket,
    commission,
    user,
    isAccepted,
}) => {
    console.log('isAccepted', isAccepted);
    const confirmOrder = (id, orderUserEmail, isOpen, deliverPrice = 0) => {
        const errMsg = isAccepted
            ? 'Цей заказ вже був прийнятий вами!'
            : user.userEmail === orderUserEmail
            ? 'Ви не можете прийняти свою пропозицію!'
            : user.verificationStatus === 'N' || !user.verificationStatus
            ? 'Для прийнятя заказу необхідно веріфікувати ваш аккаунт!'
            : null;
        if (errMsg) {
            notification.warning({
                message: errMsg,
            });
        } else {
            socket.send(
                JSON.stringify({
                    command: 'open',
                    action: 'subscribe',
                    message: {
                        order_id: id,
                        deliveryPrice: deliverPrice,
                    },
                }),
            );
        }
    };
    const deliveryPrice = deliveryService(currentChoosedOrder, delivery);
    const unitsName = currentChoosedOrder.applicationData.units.shortName;
    if (modalType === 'buy-confirm') {
        return (
            <ConfirmBuyOrder
                step={step}
                nextStep={nextStep}
                confirmOrder={confirmOrder}
                close={close}
                data={currentChoosedOrder}
                unitsName={unitsName}
                deliveryprice={deliveryPrice}
                user={user}
            />
        );
    } else if (modalType === 'sell-confirm') {
        return (
            <ConfirmSellOrder
                step={step}
                nextStep={nextStep}
                confirmOrder={confirmOrder}
                close={close}
                data={currentChoosedOrder}
                unitsName={unitsName}
                deliveryprice={deliveryPrice}
                commission={commission}
                user={user}
            />
        );
    }
};

const mapStateToProps = (state) => ({
    currentChoosedOrder: state.currentChoosedOrder,
    delivery: state.delivery,
    isAccepted: checkInActiveOrders(
        state.activeOrders.openOrders,
        state.currentChoosedOrder.id,
    ),
    socket: state.socket,
    commission: state.commission.amount,
    user: state.user,
});

export default connect(mapStateToProps)(ConfirmOrder);
