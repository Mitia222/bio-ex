import React from 'react';
import { Icon, notification } from 'antd';
import { BASE_IMG_URL } from '../../../../../constants/APIURLS';
import defaultImg from '../../../../../assets/img/no-image.png';

const ConfirmSellOrder = ({
    step,
    nextStep,
    close,
    data,
    deliveryprice,
    confirmOrder,
    unitsName,
    commission,
    user,
}) => {
    const validate = () => {
        if (user.stock && user.stock.isVerified) {
            nextStep(1);
        } else {
            notification.warning({
                message:
                    'Для прийняття ордеру необхідно веріфікувати склад у вашому профілі!',
            });
        }
    };
    let commissionOrder =
        +data.applicationData.purchaseVolume *
            +data.price *
            (+commission / 100) || 0;
    const image = data.productImage
        ? BASE_IMG_URL + data.productImage
        : defaultImg;
    if (step === 0) {
        return (
            <div className="confirm-order sell-order ">
                <div className="window-title">
                    {data.applicationData.category.name}
                    {'  '}
                    {data.applicationData.product.name}
                </div>

                {/* <div className="description-order">
                    <span className="position">
                        <Icon type="environment" />
                        Киевская область, г.Белая церковь, ул. Верхняя 28а
                    </span>

                    <span className="rating">
                        <img src={star} alt="" />
                        {data.applicationData.rating}
                    </span>
                </div> */}

                <div className="order-information">
                    <div className="info">
                        <div className="item">
                            <div className="title">Ціна:</div>

                            <div className="value">{data.price} грн</div>
                        </div>
                        <div className="item">
                            <div className="title">Обсяг:</div>

                            <div className="value">
                                {data.applicationData.purchaseVolume}{' '}
                                {unitsName}
                            </div>
                        </div>
                        <div className="item">
                            <div className="title">Товар:</div>

                            <div className="value">
                                {data.applicationData.product.name}
                            </div>
                        </div>
                        <div className="item">
                            <div className="title">Сировина:</div>

                            <div className="value">
                                {data.applicationData.rawMaterial.name}
                            </div>
                        </div>
                        <div className="item">
                            <div className="title">Одиниця виміру:</div>

                            <div className="value">
                                {data.applicationData.units.name}
                            </div>
                        </div>
                        <div className="item">
                            <div className="title">Відстань:</div>

                            <div className="value">
                                {data.distance && data.distance.text}
                            </div>
                        </div>
                        <div className="item">
                            <div className="title">
                                Орієнтовна вартість доставки:
                            </div>

                            <div className="value">{deliveryprice}грн</div>
                        </div>
                    </div>
                    <div className="image-block">
                        <img src={image} alt="" />
                    </div>
                </div>

                <div className="price">{data.sum} грн</div>

                <div className="footer-window">
                    {/* <div>
                        <Icon type="file-pdf" />
                        Завантажити сертифікат
                    </div> */}

                    <button className="btn" onClick={validate}>
                        Прийняти <Icon type="arrow-right" />
                    </button>
                </div>
            </div>
        );
    } else if (step === 1) {
        return (
            <div className="confirm-order sell-order two-step">
                <div className="window-title">Умови бронювання</div>

                <div className="description">
                    З вашого рахунку Bioexchange буде списано % від вартості{' '}
                    <br /> угоди, для того щоб забронювати ваше замовлення
                </div>

                <div className="order-card">
                    <div className="order-info">
                        <div>
                            <div className="order-name">
                                {data.applicationData.category.name}
                                {'  '}
                                {data.applicationData.product.name}
                            </div>
                            <div>
                                Ціна: {data.price} грн / {unitsName}
                            </div>
                            <div>
                                Кількість: {data.applicationData.purchaseVolume}{' '}
                                {unitsName}
                            </div>
                        </div>
                        <div className="img-block">
                            <img src={BASE_IMG_URL + image} alt="" />
                        </div>
                    </div>

                    <div className="order-price">
                        <div className="sum">
                            <div>Сума замовлення</div>
                            {data.sum} грн
                        </div>

                        <div className="advance">
                            <div>Аванс бронювання</div>
                            {commissionOrder} грн
                        </div>
                    </div>
                </div>
                {/* <div className="calculator">
                    <div className="form-item">
                        <label htmlFor="">
                            Введіть вартість доставки
                            <Popover content={content} title={null}>
                                <Icon type="question-circle" theme="filled" />
                            </Popover>
                        </label>
                        <input type="text" />
                    </div>
                    <button className="btn">Сформувати рахунок</button>
                </div> */}
                <div className="buttons">
                    {/* <button className="btn cancel">Скасувати</button> */}
                    {/* <button className="btn" onClick={() => nextStep(2)}>
                        Приймаю
                    </button> */}
                    <button
                        className="btn"
                        onClick={() =>
                            confirmOrder(
                                data.id,
                                data.email,
                                data.isOpen,
                                deliveryprice,
                            )
                        }
                    >
                        Приймаю
                    </button>
                </div>
            </div>
        );
    }
    //  else if (step === 2) {
    //     return (
    //         <div className="confirm-sell-order two-step third-step">
    //             <div className="window-title">Замовлення заброньовано</div>

    //             <div className="order-card">
    //                 <div className="order-info">
    //                     <div className="img-block">
    //                         <img src={image} alt="" />
    //                     </div>

    //                     <div>
    //                         <div className="order-name">
    //                             {data.applicationData.category.name}
    //                             {'  '}
    //                             {data.applicationData.product.name}
    //                         </div>
    //                         <div>Ціна: {data.price} грн/м³</div>
    //                         <div>
    //                             Кількість: {data.applicationData.purchaseVolume}
    //                             м³
    //                         </div>
    //                     </div>
    //                 </div>

    //                 <div className="order-price">
    //                     <div className="sum">
    //                         <div>Сума замовлення</div>
    //                         12400 грн
    //                     </div>

    //                     <div className="advance">
    //                         <div>Аванс бронювання</div>
    //                         {data.applicationData.sum}
    //                     </div>
    //                 </div>
    //             </div>

    //             <div className="window-title">Сформувати рахунок</div>

    //             <div className="confirm-form">
    //                 <div className="form-item">
    //                     <label htmlFor="">Організація</label>
    //                     <input type="text" />
    //                 </div>
    //                 <div className="form-item">
    //                     <label htmlFor="">ЕДРПОУ</label>
    //                     <input type="text" />
    //                 </div>
    //                 <div className="form-item">
    //                     <label htmlFor="">Назва банку</label>
    //                     <input type="text" />
    //                 </div>
    //                 <div className="form-item">
    //                     <label htmlFor="">МФО</label>
    //                     <input type="text" />
    //                 </div>
    //                 <div className="form-item">
    //                     <label htmlFor="">Розрахунковий рахунок</label>
    //                     <input type="text" />
    //                 </div>
    //             </div>

    //             <div className="calculator">
    //                 <div className="form-item">
    //                     <label htmlFor="">
    //                         Введіть вартість доставки
    //                         <Popover content={content} title={null}>
    //                             <Icon type="question-circle" theme="filled" />
    //                         </Popover>
    //                     </label>
    //                     <input type="text" />
    //                 </div>
    //                 <button className="btn" onClick={close}>
    //                     Сформувати рахунок
    //                 </button>
    //             </div>
    //         </div>
    //     );
    // }
};

export default ConfirmSellOrder;
