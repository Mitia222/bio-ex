import React, { useState, useEffect } from 'react';
import CreateOrder from './CreateOrder/CreateOrder';
import ConfirmOrder from './ConfirmOrders/ConfirmOrder';
import Balance from './Balance';
import ActiveOrderAccept from './ActiveOrderAccept';
import TwoFactorAuth from '../../../../components/TwoFactorAuth';
import CommissionPopupInput from '../../../AdministratorSide/Commissions/CommissionPopupInput';
import AdminsForm from '../../../AdministratorSide/Admins/AdminsForm';

const DialogWindowsHandler = ({
    handleCreateOrder,
    modalState,
    handleCloseModal,
    isOpen,
    data,
    handleCloseTwoFactor,
}) => {
    const [confirmStep, handleConfirmStep] = useState(0);

    const nextStep = step => {
        handleConfirmStep(step);
    };

    useEffect(() => {
        if (!isOpen) {
            handleConfirmStep(0);
        }
    }, [isOpen]);

    if (modalState === 'buy' || modalState === 'sell') {
        return (
            <CreateOrder
                type={modalState}
                isOpen={isOpen}
                // visible={modalState.visible}
            />
        );
    } else if (modalState === 'buy-confirm' || modalState === 'sell-confirm') {
        return (
            <ConfirmOrder
                modalType={modalState}
                step={confirmStep}
                nextStep={nextStep}
                close={handleCloseModal}
            />
        );
    } else if (modalState === 'accept-active') {
        return <ActiveOrderAccept />;
    } else if (modalState === 'show-balance') {
        return <Balance close={handleCloseModal} />;
    } else if (modalState === 'two-factor') {
        return (
            <TwoFactorAuth handleCloseModal={handleCloseModal} type="update" />
        );
    } else if (modalState === 'commission-input') {
        return <CommissionPopupInput type={modalState} data={data} />;
    } else if (
        modalState === 'admins-create' ||
        modalState === 'admins-update'
    ) {
        return (
            <AdminsForm
                handleCloseModal={handleCloseModal}
                type={modalState}
                data={data}
            />
        );
    }
    return <div style={{ opacity: 0 }}></div>;
};

export default DialogWindowsHandler;
