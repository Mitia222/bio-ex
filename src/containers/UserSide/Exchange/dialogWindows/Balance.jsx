import React, { useState } from 'react';
import api from '../../../../sevices/exchange/index';
import { fileDownloader } from '../../../../sevices/FileDownloder';
import { notification } from 'antd';

const Balance = ({ close }) => {
    const [sum, setSum] = useState('');
    const [loading, setloading] = useState(false);
    const sendInvoice = () => {
        if (!sum) {
            return;
        }
        setloading(true);
        api.transactionBalanceInvoice({ amount: sum })
            .then(({ data }) => {
                // console.log(data);
                fileDownloader(data);
                setloading(false);
                notification.success({
                    message: 'Рахунок сформованно успішно',
                });
                close();
            })
            .catch((err) => {
                setloading(false);
                if (err.response) {
                    notification.error({
                        message: err.response.data.error,
                    });
                }
            });
    };
    const handleChange = ({ target: { value } }) => {
        +value > 0 && setSum(+value);
    };
    return (
        <div className="form-item balance-invoice-block">
            <h2>Введіть сумму поповнення</h2>
            <p>
                * Ви отримаєте рахунок-фактуру, за яким необхідно внести платіж.
                Надходження грошових коштів буде відображено після підтвердження
                адміністратора біржі.
            </p>
            <input
                type="number"
                placeholder="сумма"
                value={sum}
                onChange={handleChange}
                className="balance-input"
            />
            <button disabled={loading} className="btn" onClick={sendInvoice}>
                Поповнити
            </button>
        </div>
    );
};
export default Balance;
