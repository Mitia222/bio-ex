import React, { useEffect, useState } from 'react';
import DepositHistory from './DepositHistory';
import CostsHistory from './CostsHistory';
import { connect } from 'react-redux';
import { openModal } from '../../../store/actions/modal';
import api from '../../../sevices/users/userRequests';
import { getTransactionsHistory } from '../../../store/actions/Users/ordersHistory';
import './balance.scss';


const Balance = ({
    getTransactionsHistory,
    transactionHistory: { buy, spend },
    showModal,
}) => {
    const [userBalance, setBalance] = useState({
        balance: 0,
        frozenBalance: 0,
    });
    // console.log(balance, frozenBalance);
    useEffect(() => {
        api.getUserBalance().then(({ data }) => setBalance(data));
        getTransactionsHistory();
    }, []);
    return (
        <div className="balance-page wr-padding">
            <div className="page-title">Баланс</div>

            <div className="total-balance">
                <div className="total-wr">
                    <div>
                        <p className="title">Баланс на рахунку</p>
                        <p className="title">Зарезервованні кошти</p>
                    </div>
                    <div className="count">
                        <p className="value">
                            <span className="total-price">
                                {Number(userBalance.balance).toFixed(2)}
                            </span>{' '}
                            грн
                        </p>
                        <p className="value">
                            <span className="total-reserved">
                                {Number(userBalance.frozenBalance).toFixed(2)}
                            </span>{' '}
                            грн
                        </p>
                    </div>
                </div>
                <button
                    className="btn"
                    onClick={() => showModal('show-balance')}
                >
                    Поповнити
                </button>
            </div>

            <DepositHistory dataSource={buy} />

            <CostsHistory dataSource={spend} />
        </div>
    );
};

const mapStateToProps = state => {
    return {
        transactionHistory: state.orderHistory.transactionHistory,
    };
};

const mapDispatchToProps = dispatch => ({
    getTransactionsHistory: () => dispatch(getTransactionsHistory()),
    showModal: (type, prod) => dispatch(openModal(type, prod)),
});

export default connect(
    mapStateToProps,
    mapDispatchToProps,
)(Balance);
