import React from 'react';
import { Table } from 'antd';
import columns from '../../../helpers/columns';

const DepositHistory = ({ dataSource = [] }) => {
    return (
        <div className="deposit-history">
            <div className="user-table">
                <div className="table-title">Історія моїх поповнень</div>

                <Table
                    scroll={{ x: 600 }}
                    dataSource={dataSource}
                    rowKey={row => row.id}
                    columns={columns.depositHistoryColumns}
                    pagination={{
                        pageSize: 15,
                        total: dataSource.length,

                    }}
                />
            </div>
        </div>
    );
};

export default DepositHistory;
