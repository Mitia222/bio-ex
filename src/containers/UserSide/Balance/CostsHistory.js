import React from 'react';
import { Table } from 'antd';
import columns from '../../../helpers/columns';

const CostsHistory = ({ dataSource = [] }) => {
    return (
        <div className="costs-history">
            <div className="user-table">
                <div className="table-title">Витрати</div>

                <Table
                    scroll={{ x: 600 }}
                    dataSource={dataSource}
                    columns={columns.costsColumns}
                    rowKey={row => row.id}
                    pagination={{
                        pageSize: 15,
                        total: dataSource.length,

                    }}
                />
            </div>
        </div>
    );
};

export default CostsHistory;
