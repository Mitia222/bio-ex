import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import HistoryOrders from '../Contracts/HistoryOrders';
import OpenOrders from '../Exchange/OpenOrders';
import {
    SocketHandler,
    SocketErrorHandler,
} from '../../../store/actions/Users/orders';
import handleSockets from '../../../helpers/sockets';
import './orders.scss';

const Orders = ({ userId, socket, SocketHandler, SocketErrorHandler }) => {
    useEffect(
        () => {
            if (userId && !socket) {
                console.log('reconnect');
                handleSockets(SocketHandler, SocketErrorHandler, userId);
            }
        },
        [userId],
    );
    return (
        <div className="orders-page">
            <div className="page-title">Замовлення</div>

            <OpenOrders />

            <HistoryOrders />
        </div>
    );
};

const mapStateToProps = state => ({
    userId: state.user.id,
    socket: state.socket,
});

const mapDispatchToProps = dispatch => ({
    SocketHandler: (val, socket) => dispatch(SocketHandler(val, socket)),
    SocketErrorHandler: err => dispatch(SocketErrorHandler(err)),
});
export default connect(
    mapStateToProps,
    mapDispatchToProps,
)(Orders);
