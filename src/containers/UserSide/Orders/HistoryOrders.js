import React, { useEffect } from 'react';
import { Table } from 'antd';
import columns from '../../../helpers/columns';
import { connect } from 'react-redux';
import { getAllHistroyORders } from '../../../store/actions/Users/ordersHistory';

const HistoryOrders = ({ allOrdersHistory = [], getAllHistroyORders }) => {
    useEffect(() => {
        getAllHistroyORders();
    }, []);

    return (
        <div className="orders-history">
            <div className="user-table">
                <div className="table-title">Історія замовленнь</div>

                <Table
                    dataSource={allOrdersHistory}
                    columns={columns.orderHistory}
                    pagination={false}
                />
            </div>
        </div>
    );
};

const mapStateToProps = state => ({
    allOrdersHistory: state.orderHistory.allHistory,
});
const mapDispatchToProps = dispatch => ({
    getAllHistroyORders: () => dispatch(getAllHistroyORders()),
});

export default connect(
    mapStateToProps,
    mapDispatchToProps,
)(HistoryOrders);
