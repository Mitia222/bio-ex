import React from 'react';
import { Divider, List, Typography } from 'antd';
import moment from 'moment';
import { useSelector } from 'react-redux';

const { Text, Title } = Typography;

const UserNews = () => {
    const news = useSelector(state => state.news);
    return (
        <div className="user-news wr-padding">
            <Divider orientation="left">Новини</Divider>
            <List
                header={false}
                footer={false}
                dataSource={news.map((item) => item.text)}
                renderItem={(item, idx) => (
                    <List.Item>
                        <Text  mark strong>
                            {moment(news[idx].createdDate).format('L')}
                        </Text >{' '}
                        <Title level={4} ellipsis={{ rows: 1 }}>
                            {news[idx].title}
                        </Title>
                        <div dangerouslySetInnerHTML={{ __html: item }} />
                    </List.Item>
                )}
            />
        </div>
    );
};
export default UserNews;
