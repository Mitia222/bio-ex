import React, { useEffect, useState } from 'react';
import { Table } from 'antd';
import columns from '../../../helpers/columns';
import { connect } from 'react-redux';
import { getAllHistroyORders } from '../../../store/actions/Users/ordersHistory';
import { v4 as uuidv4 } from 'uuid';
import HistoryFilters from './HistoryFilters';
import useFilters from '../../../Hooks/useFilters';

const initial_state = {
    page: 1,
    id_raw_material: undefined,
    type: undefined,
    start_date: undefined,
    end_date: undefined,
};

const HistoryOrders = ({
    history_items = [],
    loading,
    total,
    getAllHistroyORders,
}) => {
    const [values, onChange, reset, onPageChange] = useFilters(
        initial_state,
        getAllHistroyORders,
    );
    return (
        <div className="orders-history">
            <div className="user-table">
                <div className="table-title">Історія торгів</div>
                <HistoryFilters
                    values={values}
                    onChange={onChange}
                    resetFilters={reset}
                />
                <Table
                    rowKey={() => uuidv4()}
                    dataSource={history_items}
                    columns={columns.orderHistory}
                    scroll={{x: 1100}}
                    loading={loading}
                    pagination={{
                        pageSize: 10,
                        total,
                        onChange: onPageChange,
                    }}
                />
            </div>
        </div>
    );
};

const mapStateToProps = (state) => ({
    loading: state.orderHistory.allHistory.loading,
    total: state.orderHistory.allHistory.total,
    history_items: state.orderHistory.allHistory.items,
});
const mapDispatchToProps = (dispatch) => ({
    getAllHistroyORders: (query) => dispatch(getAllHistroyORders(query)),
});

export default connect(mapStateToProps, mapDispatchToProps)(HistoryOrders);
