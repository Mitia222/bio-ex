import React, { useEffect, useState } from 'react';
import { connect } from 'react-redux';
import { getOrdersSum } from '../../../helpers/functions';
import ActiveOrder from '../Exchange/ActiveOrders';
import HistoryOrders from './HistoryOrders';
import './contracts.scss';
import { ALL_HISTORY } from '../../../constants/APIURLS';
import http from '../../../sevices/API';

const Contracts = ({ activeOrdersSum, openOrdersSum }) => {
    const [endedBuyState, setBuyEnded] = useState(0);
    const [endedSellState, setSellEnded] = useState(0);
    const fetchEnderdOrders = () => {
        http('get', ALL_HISTORY, {}, { status: "3" }).then(({data}) => {
            const endedBuy = data.reduce((acc, el) => {
                if (el.itemHistory.status === 'Закритий' && el.itemHistory.type === "Купівля") {
                    acc += +el.itemHistory.total
                }
                return acc
            }, 0)
            const endedSell = data.reduce((acc, el) => {
                if (el.itemHistory.status === 'Закритий' && el.itemHistory.type === "Продажа") {
                    acc += +el.itemHistory.total
                }
                return acc
            }, 0)
            setBuyEnded(endedBuy)
            setSellEnded(endedSell)
            // console.log('res', endedSell, endedBuy);
        })
    }

    useEffect(() => {
        fetchEnderdOrders()
    }, []);
    return (
        <div className="contracts-page wr-padding">
            <div className="total-wrapper">
                <div className="total-part-content">
                    <div className="page-title">Сума по активним угодам</div>
                    <div className="total-block">
                        <div className="total-buy">
                            <span className="title">Купівля</span>
                            <span className="value">
                                <span>{activeOrdersSum.buySum.toFixed(2)}</span>{' '}
                                грн
                            </span>
                        </div>
                        <div className="total-sell">
                            <span className="title">Продаж</span>
                            <span className="value">
                                <span>
                                    {activeOrdersSum.sellSum.toFixed(2)}
                                </span>{' '}
                                грн
                            </span>
                        </div>
                    </div>
                </div>
                <div className="total-part-content">
                    <div className="page-title">Сума по виконаним угодам</div>
                    <div className="total-block">
                        <div className="total-buy">
                            <span className="title">Купівля</span>
                            <span className="value">
                                <span>{ endedBuyState }</span>{' '}
                                грн
                            </span>
                        </div>
                        <div className="total-sell">
                            <span className="title">Продаж</span>
                            <span className="value">
                                <span>{endedSellState}</span>{' '}
                                грн
                            </span>
                        </div>
                    </div>
                </div>
            </div>
            <ActiveOrder />
            {/*<MyFilters />*/}
            <HistoryOrders />
        </div>
    );
};

const mapStateToProps = (state) => ({
    activeOrdersSum: getOrdersSum(state.activeOrders.activeOrders),
    openOrdersSum: getOrdersSum(state.activeOrders.openOrders),
});

export default connect(mapStateToProps)(Contracts);
