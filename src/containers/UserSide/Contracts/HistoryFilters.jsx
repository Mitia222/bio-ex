import React from 'react';
import { DatePicker } from 'antd';
import CustomSelect from '../../CustomSelect';
import { userStatusCodes, types } from '../../../helpers/status';
import { flatToUniqueRwMterials } from '../../../helpers/functions';
import { useSelector } from 'react-redux';
import locale from 'antd/es/date-picker/locale/uk_UA';
import moment from 'moment';


const HistoryFilters = ({ values, onChange, resetFilters }) => {
    const productsForFilter = useSelector((state) =>
        flatToUniqueRwMterials(state.products),
    );
    return (
        <div className="filters-wrapper">
            <div className="history-filters">
                <CustomSelect
                    options={types}
                    value={values.type}
                    handleChange={(val) => onChange('type', val)}
                    placeholder="Тип"
                    label="Тип"
                />
                <CustomSelect
                    options={productsForFilter}
                    value={values.id_raw_material}
                    handleChange={(val) => onChange('id_raw_material', val)}
                    placeholder="Сировина"
                    label="Сировина"
                />

                <CustomSelect
                    options={userStatusCodes}
                    value={values.status}
                    handleChange={(val) => onChange('status', val)}
                    placeholder="Статус"
                    label="Статус угоди"
                />
                <div className="form-item">
                    <label htmlFor="">Дата</label>
                    <div className="form-item__filters-date-block">
                        <DatePicker
                            placeholder='Від'
                            locale={locale}
                            value={values.start_date ? moment(values.start_date) : undefined}
                            onChange={(moment, date) => {
                                onChange('start_date', date);
                            }}
                        />
                        <DatePicker
                            placeholder='До'
                            locale={locale}

                            value={values.start_date ? moment(values.end_date) : undefined}
                            onChange={(moment, date) => {
                                onChange('end_date', date);
                            }}
                        />
                    </div>
                </div>
            </div>

            <button className="btn reset" onClick={resetFilters}>
                Скасувати фільтри
            </button>
        </div>
    );
};

export default HistoryFilters;
