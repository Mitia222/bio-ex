import React from 'react';
import { Icon } from 'antd';
import Dropzone from 'react-dropzone';
import { BASE_IMG_URL } from '../../../constants/APIURLS';

const Documents = ({ documents, onDrop,deleteImage }) => {
    return (
        <div className="documents section">
            <div className="block-title">
                Скани документів
                <Dropzone onDrop={onDrop} accept='.jpg, .jpeg, .png, .gif'>
                    {({ getRootProps, getInputProps, isDragActive }) => {
                        return (
                            <div {...getRootProps()}>
                                <input {...getInputProps()} />
                                <button className="btn">Додати документ</button>
                            </div>
                        );
                    }}
                </Dropzone>
            </div>

            <div className="documents-list">
                {[0, 1, 2, 3, 4, 5].map(i => (
                    <div className="document" key={i}>
                        <label htmlFor="">Документ {i + 1}</label>
                        <div className="default-img">
                            {documents[i] ? (
                                <div className='document-image'>
                                    <img
                                        src={
                                            BASE_IMG_URL + documents[i].imageDecoded
                                        }
                                        alt=""
                                    />
                                    <div className='delete-layout'>
                                        <button className='btn' onClick={() => deleteImage(i)}>Видалити</button>
                                    </div>
                                </div>
                            ) : (
                                <Icon type="camera" theme="filled" />
                            )}
                        </div>
                    </div>
                ))}
            </div>
        </div>
    );
};

export default Documents;
