import React from 'react';
import icon from '../../../assets/img/padlock.svg';
import { Link } from 'react-router-dom';

const ChangePassword = () => {
    return (
        <div className="change-password-page section">
            <img src={icon} alt="icon#2" />
            <span>Змінити пароль</span>
            <button className="btn">
                <Link to="/change_password">Змінити пароль</Link>
            </button>
        </div>
    );
};

export default ChangePassword;
