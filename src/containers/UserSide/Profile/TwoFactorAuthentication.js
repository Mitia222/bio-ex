import React from 'react';
import { Switch, notification } from 'antd';
import { connect } from 'react-redux';
import icon from '../../../assets/img/authentication.svg';
import { openModal } from '../../../store/actions/modal';
import api from '../../../sevices/users/userRequests';
import { putTwoFactor } from '../../../store/actions/user';

const TwoFactorAuthentication = ({
    defaultValue,
    value,
    email,
    showModal,
    initiateTwoFactorAuth,
}) => {
    const handleChangeTwoAuth = val => {
        // console.log(val);
        api.updateTwoFactor({ twoFactorAuthEnabled: val })
            .then(({ data }) => {
                if (data.imageBase64) {
                    initiateTwoFactorAuth({
                        twoFactorAuthEnabled: true,
                        image: data.imageBase64,
                        email: email,
                    });
                    showModal('two-factor');
                } else {
                    initiateTwoFactorAuth({
                        twoFactorAuthEnabled: false,
                        imageBase64: '',
                        email: email,
                    });
                    notification.success({
                        message: 'Аудентифікацію вимкнено!',
                    });
                }
            })
            .catch(err => console.log(err));
    };
    return (
        <div className="two-factor-block section">
            <div className="title-block">
                <img src={icon} alt="" />
                2-факторна аутентифікація
            </div>
            <div className="switch-block">
                <div>
                    Включити двухфакторную <br />
                    аутентифікацію
                </div>

                <Switch
                    disabled
                    defaultChecked={defaultValue}
                    checked={defaultValue}
                    onChange={handleChangeTwoAuth}
                />
            </div>
        </div>
    );
};
const mapDispatchToProps = dispatch => ({
    showModal: (type, prod, imgUrl) => dispatch(openModal(type, prod, imgUrl)),
    initiateTwoFactorAuth: payload => dispatch(putTwoFactor(payload)),
});
export default connect(
    null,
    mapDispatchToProps,
)(TwoFactorAuthentication);
