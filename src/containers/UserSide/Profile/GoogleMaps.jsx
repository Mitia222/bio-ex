import React from 'react';
import {
    withScriptjs,
    withGoogleMap,
    GoogleMap,
    Marker,
} from 'react-google-maps';

const GoogleMapComponent = withScriptjs(
    withGoogleMap(props => (
        <GoogleMap
            defaultZoom={6}
            defaultCenter={{ lat: 48.3794, lng: 31.1656 }}
        >
            {props.isMarkerShown && <Marker />}
        </GoogleMap>
    )),
);
class MyGoogleMapComponent extends React.PureComponent {
    state = {
        isMarkerShown: false,
    };

    componentDidMount() {
        this.delayedShowMarker();
    }

    delayedShowMarker = () => {
        setTimeout(() => {
            this.setState({ isMarkerShown: true });
        }, 3000);
    };

    handleMarkerClick = () => {
        this.setState({ isMarkerShown: false });
        this.delayedShowMarker();
    };

    render() {
        return (
            <GoogleMapComponent
                googleMapURL="https://maps.googleapis.com/maps/api/js?key=AIzaSyAyUvxMRDClpENiCPC8ueXlqc93Yd8jR3o&v=3.exp&types=region&libraries=geometry,drawing,places"
                loadingElement={<div style={{ height: `100%` }} />}
                containerElement={<div style={{ height: `400px` }} />}
                mapElement={<div style={{ height: `100%` }} />}
                isMarkerShown={this.state.isMarkerShown}
                onMarkerClick={this.handleMarkerClick}
            />
        );
    }
}

//   googleMapURL="https://maps.googleapis.com/maps/api/js?v=3.exp&libraries=geometry,drawing,places"
//   loadingElement={<div style={{ height: `100%` }} />}
//   containerElement={<div style={{ height: `400px` }} />}
//   mapElement={<div style={{ height: `100%` }}

export default MyGoogleMapComponent;
