import React from 'react';
import ChangePassword from './ChangePassword';
import { connect } from 'react-redux';
import { Logout, editUserPRofile, verifyWarehouse } from '../../../store/actions/user';
import ShortInformation from './ShortInformation';
// import TwoFactorAuthentication from './TwoFactorAuthentication';
import UserInfo from './UserInfoForms/UserInfo';
import Documents from './Documents';
import { removeSocket } from '../../../store/actions/Users/ordersActionsCreators';

import './Profile.scss';
import { notification } from 'antd';

const Profile = ({ logOut, editProfile, user, removeSocket, socket, submitWarehouses }) => {
    const handleDropImage = images => {
        const reader = new FileReader();
        reader.readAsDataURL(images[0]);
        reader.onloadend = () => {
            if (images[0].size >= 1500000) {
                notification.error({
                    message: "Файл має бути меншим 1.5mb!"
                })
            } else {
                editProfile({
                    documentImages: [...user.documentImages,{ imageDecoded: reader.result }],
                });
            }

        };
    };
     const deleteImage = (idx) => {
         const imagesWithoutDeleted = user.documentImages.filter((el,index) => index !== idx);

         editProfile({
             documentImages: imagesWithoutDeleted,
         });
     }
    const logout = () => {
        if (socket) {
            socket && socket.close();
            removeSocket();
        }
        logOut();
    };

    return (
        <div className="profile-page wr-padding">
            <ShortInformation user={user} onLogout={logout} />


            <ChangePassword />

            <UserInfo
                updateProfile={editProfile}
                user={user}
                submitWarehouses={submitWarehouses}
            />

            <Documents
                documents={user.documentImages}
                onDrop={handleDropImage}
                deleteImage={deleteImage}
            />
        </div>
    );
};

const mapStateToProps = state => ({
    user: state.user,
    socket: state.socket,
});

const mapDispatchToProps = dispatch => ({
    logOut: () => dispatch(Logout()),
    editProfile: data => dispatch(editUserPRofile(data)),
    removeSocket: () => dispatch(removeSocket()),
    submitWarehouses: (data) => dispatch(verifyWarehouse(data))
});

export default connect(
    mapStateToProps,
    mapDispatchToProps,
)(Profile);
