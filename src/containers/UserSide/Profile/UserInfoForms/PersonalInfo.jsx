import React from 'react';
import CustomSelect from '../../../CustomSelect';
import Input from '../../../../components/Input';
import userTypes from '../../../../constants/userRoles';

const PersonalInfo = ({
    errors,
    values,
    setFieldValue,
    handleChange,
    user,
}) => {
    return (
        <div className="personal-inf">
            <div className="block-title">Персональна інформація</div>
            <CustomSelect
                options={userTypes}
                label="Роль"
                value={values.userRole}
                handleChange={(type) => setFieldValue('userRole', type)}
            />

            <Input
                label="Ім'я"
                handleChange={handleChange}
                value={values.firstName}
                name="firstName"
                placeholder="-"
                errors={errors.firstName}
            />
            <Input
                label="Прізвище"
                handleChange={handleChange}
                value={values.lastName}
                name="lastName"
                placeholder="-"
                errors={errors.lastName}
            />
            <Input
                label="Email"
                type="email"
                value={values.email}
                name="email"
                placeholder="-"
                handleChange={handleChange}
                errors={errors.email}
            />
            <Input
                label="Телефон"
                type="phone"
                value={values.phone}
                name="phone"
                placeholder="Наприклад: +380982992323 або 80982923232"
                handleChange={handleChange}
                errors={errors.phone}
            />
            <Input
                label="ІНН"
                value={values.nin}
                name="nin"
                type="number"
                placeholder="-"
                handleChange={(e) =>
                    e.target.value.length <= 10 && handleChange(e)
                }
                errors={errors.nin}
            />
            <Input
                label="Країна"
                value={user.userCountry}
                placeholder="-"
                disabled
                handleChange={handleChange}
            />
        </div>
    );
};

export default PersonalInfo;
