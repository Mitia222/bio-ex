import React from 'react';
import regions from '../../../../constants/regions';
import CustomSelect from '../../../CustomSelect';
import Input from '../../../../components/Input';
import { withFormik } from 'formik';
import { warehouseSchema } from '../../../../schema/user';
import { useSelector } from 'react-redux';

const headStyle = { marginTop: '40px' };

const Warehouse = ({ values, handleSubmit, setFieldValue, errors }) => {
    const stock = useSelector((state) => state.user.stock);
    const submit = (e) => {
        e.preventDefault();
        e.stopPropagation();
        handleSubmit(e);
    };
    return (
        <form onSubmit={submit}>
            <div className="block-title" style={headStyle}>
                Локація складу
            </div>
            <Input
                placeholder="-"
                label="Місто"
                value={values.stockCity}
                name="stockCity"
                handleChange={({ target: { value } }) =>
                    setFieldValue('stockCity', value)
                }
                errors={errors.stockCity}
            />
            <CustomSelect
                placeholder="Область"
                options={regions}
                label="Область"
                value={values.stockRegion}
                handleChange={(id) => setFieldValue('stockRegion', id)}
                errors={errors.stockRegion}
            />

            <Input
                placeholder="-"
                label="Країна"
                value={values.stockCountry}
                name="stockCountry"
                handleChange={({ target: { value } }) =>
                    setFieldValue('stockCountry', value)
                }
                errors={errors.stockCountry}
            />
            <Input
                label="Вулиця"
                placeholder="-"
                value={values.stockStreet}
                name="stockStreet"
                handleChange={({ target: { value } }) =>
                    setFieldValue('stockStreet', value)
                }
                errors={errors.stockStreet}
            />
            <Input
                label="Будинок"
                placeholder="-"
                value={values.stockBuilding}
                name="stockBuilding"
                handleChange={({ target: { value } }) =>
                    setFieldValue('stockBuilding', value)
                }
                errors={errors.stockBuilding}
            />
            {stock && stock.isVerified ? (
                <div className="verification-status">Веріфіковано</div>
            ) : (
                <div className="verification-status notVerified">
                    Неверіфіковано
                </div>
            )}
            <button className="btn verify_btn" type="submit">
                Веріфікувати Склад
            </button>
        </form>
    );
};
export default withFormik({
    mapPropsToValues: ({ values }) => ({
        stockCity: values.stock ? values.stock.stockCity : '',
        stockRegion: values.stock ? values.stock.stockRegion : '',
        stockCountry: values.stock ? values.stock.stockCountry : '',
        stockStreet: values.stock ? values.stock.stockStreet : '',
        stockBuilding: values.stock ? values.stock.stockBuilding : '',
    }),
    validateOnChange: false,
    validationSchema: warehouseSchema,
    handleSubmit: (values, { props: { submitWarehouses } }) => {
        // console.log('submiting', values, props);
        submitWarehouses(values);
        // removeNulls(values);
        // updateProfile(values);
    },

    displayName: 'WarehouseForm',
})(Warehouse);

// const mapDispatchToProps = dispatch => ({
//     fetchForWarehouses: data => dispatch(verifyWarehouse(data)),
// });
//
// export default connect(null, mapDispatchToProps)(Warehouse);
