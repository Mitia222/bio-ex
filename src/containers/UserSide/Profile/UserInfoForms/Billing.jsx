import React, { Fragment } from 'react';
import Input from '../../../../components/Input';

const Billing = ({ errors, values, setFieldValue }) => {
    return (
        <Fragment>
            <div className="block-title">Платіжна інформація</div>
            <Input
                label="Назва банку"
                value={values.account ? values.account.bank : ''}
                name="bank"
                placeholder="-"
                handleChange={(e) =>
                    setFieldValue('account.bank', e.target.value)
                }
                errors={errors && errors.bank}
            />
            <Input
                label="МФО"
                value={values.account ? values.account.sortCode : ''}
                name="sortCode"
                placeholder="-"
                handleChange={(e) =>  e.target.value.length <= 6 &&
                    setFieldValue('account.sortCode', e.target.value)
                }
                type='number'
                errors={errors && errors.sortCode}
            />
            <Input
                label="Розрахунковий рахунок"
                value={values.account ? values.account.currentAccount : ''}
                name="currentAccount"
                placeholder="-"
                type='number'
                handleChange={(e) => e.target.value.length <= 20 &&
                    setFieldValue('account.currentAccount', e.target.value)
                }
                errors={errors && errors.currentAccount}
            />
        </Fragment>
    );
};

export default Billing;
