import React from 'react';
import { withFormik } from 'formik';
import PersonalInfo from './PersonalInfo';
import { removeNulls } from '../../../../helpers/functions';
import Company from './Company';
import Billing from './Billing';
import Warehouse from './Warehouse';
import { updateprofileSchema } from '../../../../schema/user';

const UserForm = ({
    values,
    handleChange,
    setFieldValue,
    handleSubmit,
    user,
    submitWarehouses,
    errors,
}) => {
    console.log('errors', errors);
    return (
        <form
            className="user-information-block section"
            onSubmit={handleSubmit}
        >
            <PersonalInfo
                errors={errors}
                user={user}
                values={values}
                setFieldValue={setFieldValue}
                handleChange={handleChange}
            />
            <Company
                values={values}
                setFieldValue={setFieldValue}
                errors={errors.organization}
            />
            <div className="billing-inf">
                <Billing
                    errors={errors.account}
                    values={values}
                    setFieldValue={setFieldValue}
                />
                <Warehouse
                    values={values}
                    submitWarehouses={submitWarehouses}
                    // setFieldValue={setFieldValue}
                />
                <button
                    className="btn profile_btn"
                    type="submit"
                    disabled={user.loading || Object.keys(errors).length > 0}
                >
                    Оновити дані
                </button>
            </div>
        </form>
    );
};

const UserInfo = withFormik({
    enableReinitialize: true,
    mapPropsToValues: ({ user }) => ({
        firstName: user.firstName,
        lastName: user.lastName,
        phone: user.phone,
        nin: user.nin,
        organization: user.organization,
        account: user.account,
        stock: user.stock,
        role: user.role,
        email: user.email,
        userRole: user.userRole,
    }),
    validateOnChange: true,
    validationSchema: updateprofileSchema,
    handleSubmit: (values, { setSubmitting, props: { updateProfile } }) => {
        const emptyStringSlice = removeNulls(values);
        updateProfile(emptyStringSlice);
    },

    displayName: 'ProfileForm',
})(UserForm);
export default UserInfo;
