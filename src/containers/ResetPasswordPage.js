import React  from 'react';
import { Icon } from 'antd';
import { NavLink } from 'react-router-dom';
import api from '../sevices/users/userRequests';
import logo from '../assets/img/logo_head.svg';
import { notification } from 'antd';
import { withFormik } from 'formik';
import { ResetPasswordSchema } from '../schema/user';
import { Form } from 'formik';
import Input from '../components/Input';
import Recaptch from '../components/Recaptcha';

const ResetPasswordPage = ({ handleChange, errors, setFieldValue, values }) => {
    console.log('errors', errors);
    return (
        <div className="reset-password-page separate-form">
            <div className="go-back-btn" onClick={() => window.history.back()}>
                <Icon type="left" />
                Назад
            </div>

            <div className="logo">
                <img src={logo} alt="" />
            </div>

            <div className="form-title">
                <hr />
                <span>Відновлення пароля</span>
                <hr />
            </div>

            <div className="description">
                Введіть ваш email і ми надішлемо вам новий пароль
            </div>

            <Form>
                <Input
                    label="Email"
                    type="email"
                    name="email"
                    handleChange={handleChange}
                    placeholder="Наприклад: example@gmail.com"
                    errors={errors.email}
                />

                <Recaptch
                    verifyCallback={(e) => setFieldValue('captch', true)}
                />

                <button
                    type="submit"
                    disabled={values.loading}
                    className="btn authentication-action-btn"
                >
                    Надіслати
                </button>

                <div className="go-to-login">
                    <NavLink to="/login">Увійти</NavLink>
                </div>
            </Form>
        </div>
    );
};

export default withFormik({
    mapPropsToValues: () => ({
        email: '',
        loading: false,
        captch: false,
    }),
    handleSubmit: (values, { setFieldValue, props: { login, history } }) => {
        // setFieldValue('loading', true);
        if (!values.captcha) {
            return notification.error({ message: "Заповніть captch'у." });
        }
        api.ResetPassword({ email: values.email })
            .then((el) => {
                notification.success({
                    message: 'Пароль був відправленний на ваш e-mail!',
                });
                history.push('/login');
            })
            .catch((err) => {
                if (err) {
                    console.log(err.response);
                    setFieldValue('loading', false);
                    notification.error({
                        message: err.response.data.email,
                    });
                }
            });
        // login({ email: values.email, password: values.password }, history);
    },

    validationSchema: ResetPasswordSchema,
    displayName: 'ResetForm',
})(ResetPasswordPage);
