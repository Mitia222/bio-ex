import React, { useState } from 'react';
import { Modal } from 'antd';
import { Link } from 'react-router-dom';
import ReactPlayer from 'react-player';
import img from '../assets/fire_group.png';
import video_img from '../assets/video-block-img.png';
import line from '../assets/video-line.png';
import about from '../assets/about.png';
import how_1 from '../assets/how_1.svg';
import how_2 from '../assets/how_2.svg';
import how_3 from '../assets/how_3.svg';
import how_about_1 from '../assets/how-about_1.png';
import how_about_2 from '../assets/how-about_2.png';
import boy from '../assets/boy.png';
import mail from '../assets/mail.svg';
import phone from '../assets/entypo_phone.svg';
import why_1 from '../assets/why_icon_1.png';
import why_2 from '../assets/why_icon_2.png';
import why_3 from '../assets/why_icon_3.png';
import why_4 from '../assets/why_icon_4.png';
import why_5 from '../assets/why_icon_5.png';
import why_6 from '../assets/why_icon_6.png';
import arrowright from '../assets/arrow-rigth.png';
import arrowleft from '../assets/arrow-left.png';
import arrowbottom from '../assets/arrow-bottom.png';

const HomePage = () => {
    const [state, toogle] = useState(false);
    const showVideo = () => toogle((state) => !state);

    return (
        <>
            <div className="main">
                <div className="container welcome-block">
                    <div className="welcome-block__left">
                        <h1 className="welcome-block__title">
                            Biofuel Energy HUB
                        </h1>
                        <p className="welcome-block__subtitle">
                            Сучасна платформа для купівлі та продажу біопалива
                        </p>
                        <Link to="/registration" className="btn-base filled">
                            Реєстрація
                        </Link>
                    </div>
                    <div className="welcome-block__right">
                        <img src={img} alt="default-img" />
                    </div>
                </div>
            </div>
            <div className="video">
                <Modal
                    title={
                        <h3 className="subtitle">Платформа Biofuel Energy HUB</h3>
                    }
                    visible={state}
                    centered
                    footer={false}
                    onCancel={showVideo}
                    className="video__iframe"
                >
                    <ReactPlayer
                        // config={{}}
                        url="https://www.youtube.com/watch?v=sCH99RSNUUM"
                    />
                </Modal>
                <div className="container video__block" onClick={showVideo}>
                    <div className="video__image">
                        <img src={video_img} alt="default-img" />
                    </div>
                    <div className="video__lines">
                        <img src={line} alt="default-img" />
                        <img src={line} alt="default-img" />
                        <img src={line} alt="default-img" />
                        <img src={line} alt="default-img" />
                    </div>
                    <div className="video__play">
                        <svg
                            width="95"
                            height="95"
                            viewBox="0 0 95 95"
                            fill="none"
                            xmlns="http://www.w3.org/2000/svg"
                        >
                            <circle
                                cx="47.5"
                                cy="47.5"
                                r="47.5"
                                fill="#EC615B"
                            />
                            <path
                                d="M36.0524 67.1051L36.0524 29L69.0524 48.0525L36.0524 67.1051Z"
                                fill="white"
                            />
                        </svg>
                    </div>
                    <div className="video__text">
                        {' '}
                        <p>
                            Приділіть трохи часу та подивіться відео про те, як
                            працює платформа.
                        </p>
                    </div>
                </div>
            </div>
            <div className="container why-us">
                <div className="why-us__arrow-container desktop" />
                <div className="why-us__arrow-container mobile">
                    <img src={arrowright} alt="default-img" />
                    <img src={arrowleft} alt="default-img" />
                    <img src={arrowright} alt="default-img" />
                    <img src={arrowleft} alt="default-img" />
                    <img src={arrowright} alt="default-img" />
                    <img src={arrowbottom} alt="default-img" />
                </div>
                <h2 className="title-m">Чому обирають нас?</h2>
                <div className="why-us__block">
                    <div className="why-us__item">
                        <img src={why_1} alt="default-img" />
                        <p>Тільки верифіковані користувачі</p>
                    </div>
                    <div className="why-us__item">
                        <img src={why_2} alt="default-img" />
                        <p>Супровід протягом всього часу угоди</p>
                    </div>
                    <div className="why-us__item">
                        <img src={why_3} alt="default-img" />
                        <p>Допомога з оформленням документів</p>
                    </div>
                    <div className="why-us__item">
                        <img src={why_4} alt="default-img" />
                        <p>Зручна механіка платформи</p>
                    </div>
                    <div className="why-us__item">
                        <img src={why_5} alt="default-img" />
                        <p>Рейтинг користувачів на основі реальних відгуків</p>
                    </div>
                    <div className="why-us__item">
                        <img src={why_6} alt="default-img" />
                        <p>Збереження історіі всіх проведених угод</p>
                    </div>
                </div>
            </div>
            <div className="container about">
                <h2 className=" title-m">Про нас</h2>
                <div className="about__wrapper">
                    <div className="about__image">
                        <img src={about} alt="default-img" />
                    </div>
                    <div className="about__text">
                        <p>
                            <span className='subtitle'>BioEnergy HUB</span> - це платформа, на якій продавець і
                            покупець без посередників можуть провести операцію
                            мінімізуючи ризики.
                        </p>
                        <p>Основний принцип платформи - прозорість.</p>
                        <p>
                            {' '}
                            Покупець і продавець зможуть побачити рейтинг
                            один одного, наявність сертифікатів і варіанти
                            співробітництва.
                        </p>
                        <p>
                            Ми ж простежимо, за легальної частиною - допоможемо
                            при укладанні Договору, надамо допомогу юриста,
                            допоможемо з пошуком транспорту.
                        </p>
                        <p>
                            <span className='subtitle'>BioEnergy HUB</span> - це весь ринок твердого біопалива в
                            декількох кліках.
                        </p>
                    </div>
                </div>
            </div>
            <div className="container how">
                <h2 className="title-m">Як це працює?</h2>
                <h3 className="subtitle">Реєстрація та верифікація</h3>
                <div className="how__auth-block">
                    <div className="how__content">
                        <img src={how_1} alt="default-img" />
                        <p>
                            Ви реєструєтесь на платформі та додаєте документи до
                            особистого кабінету{' '}
                        </p>
                    </div>

                    <div className="how__content">
                        <img src={how_2} alt="default-img" />
                        <p>
                            Адміністратор перевіряє ваші дані та верифікує
                            кабінет
                        </p>
                    </div>

                    <div className="how__content">
                        <img src={how_3} alt="default-img" />
                        <p>
                            Після верифікації ви можете користуватись основним
                            функціоналом платформи
                        </p>
                    </div>
                </div>
                <div className="how__about">
                    <h3 className="subtitle">Купівля та продаж </h3>
                    <div className="how__about-blocks-block">
                        <div className="how__about-content">
                            <img src={how_about_1} alt="default-img" />
                            <p>
                                На сторінці платформи ви можете додати лот на
                                купівлю або продаж товару
                            </p>
                        </div>
                        <div className="how__about-content">
                            <img src={how_about_2} alt="default-img" />
                            <p>Інший користувач може прийняти ваш лот </p>
                        </div>
                        <div className="how__about-content">
                            <img src={how_about_2} alt="default-img" />
                            <p>
                                Після отримання пропозиції ви приймаєте її та
                                отримуєте контакти користувача
                            </p>
                        </div>
                        <div className="how__about-content">
                            <img src={how_about_2} alt="default-img" />
                            <p>
                                Після підписання договору та проведення угоди ви
                                закриваєте угоду на платфомі та можете залишити
                                відгук :){' '}
                            </p>
                        </div>
                    </div>
                </div>
            </div>
            <div className="contacts">
                <div className="container contacts__block">
                    <div className="contacts__info">
                        <h2 className="title-m">Контакты</h2>
                        <p>
                            <img src={phone} alt="default-img" />
                            +380 73 19 87 246
                        </p>
                        <p>
                            <img src={mail} alt="default-img" />
                            jfdisjs@jfklsk.ua
                        </p>
                    </div>
                    <div className="contacts__image">
                        <img src={boy} alt="default-img" />
                    </div>
                </div>
            </div>
        </>
    );
};

export default HomePage;
