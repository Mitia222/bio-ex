import React from 'react';
import error_face from '../assets/img/robot.png';
import { Link } from 'react-router-dom';

const defaultErrorDescription = 'Сторінку не знайдено або була видаленно.';

const ErrorPage = ({
    statusCode = '404',
    statusText = defaultErrorDescription,
}) => {
    return (
        <div className="error-page">
            <div className="error-content">
                <h1>{statusCode}</h1>
                <p>{statusText}</p>
                <Link to={'/'} className="btn">
                    На головну
                </Link>
            </div>
            <img src={error_face} alt="" />
        </div>
    );
};

export default ErrorPage;
