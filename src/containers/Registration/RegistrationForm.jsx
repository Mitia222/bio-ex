import React, { useEffect, useRef } from 'react';
import { RegistrationSchema } from '../../schema/user';
import { withFormik, Form  } from 'formik';
import CustomSelect from '../CustomSelect';
import Input from '../../components/Input';
import Recaptcha from 'react-recaptcha';
import { NavLink } from 'react-router-dom';
import roles from '../../constants/userRoles';

const RegistrationForm = ({
    errors,
    setFieldValue,
    values,
    loading,
    touched,
    setTouched,
}) => {
    const captchaRef = useRef(null);

    const wrappedHandleChange = ({ target: { value, name } }) => {
        setTouched({ ...touched, [name]: true });
        setFieldValue(name, value);
    };
    useEffect(() => {
        captchaRef.current.reset();
    }, []);
    return (
        <Form>
            <Input
                handleChange={wrappedHandleChange}
                label="Ваше ім'я"
                placeholder="Ваше ім'я"
                name="username"
                errors={touched.username && errors.username}
            />
            <CustomSelect
                label="Роль користувача"
                handleChange={(value) => setFieldValue('userRole', value)}
                placeholder="Виберіть роль"
                options={roles}
                value={values.userRole}
            />
            <Input
                label="Email"
                type="email"
                name="email"
                handleChange={wrappedHandleChange}
                placeholder="Наприклад: example@gmail.com"
                errors={touched.email && errors.email}
            />
            <Input
                label="Пароль"
                type="password"
                name="password"
                handleChange={wrappedHandleChange}
                placeholder="******"
                errors={touched.password && errors.password}
            />
            <Input
                label="Підтвердження пароля"
                type="password"
                name="repeatPassword"
                handleChange={wrappedHandleChange}
                placeholder="******"
                errors={touched.repeatPassword && errors.repeatPassword}
            />
            <Input
                label="Телефон"
                type="phone"
                name="phone"
                handleChange={wrappedHandleChange}
                placeholder="+380982992323"
                errors={touched.phone && errors.phone}
            />

            <div className="confirm">
                <Input
                    label="Я приймаю умови конфіденційності"
                    className="checkbox"
                    labelOnclick={() => window.open('/support')}
                    type="checkbox"
                    name="permission"
                    handleChange={(e) => {
                        const checked = e.target.checked;
                        setTouched({ ...touched, permission: true });
                        setFieldValue('permission', checked);
                    }}
                    errors={touched.permission && errors.permission}
                />

                <Recaptcha
                    ref={captchaRef}
                    sitekey="6LeHnuMUAAAAAPsx_PucUJkJ0LIzo3esvyocC6SR"
                    render="explicit"
                    verifyCallback={(e) => {
                        setTouched({ ...touched, captch: true });
                        setFieldValue('captch', true);
                    }}
                />
                {touched.captch && errors.captch && (
                    <div className="inputs-error-text">{errors.captch}</div>
                )}
            </div>

            <button
                // onClick={() => validateForm().then(validate)}
                type="submit"
                className="btn authentication-action-btn"
                disabled={loading}
            >
                Зарeєструватися
            </button>

            <div className="go-to-login">
                <NavLink to="/login">Я вже зареєстрованний</NavLink>
            </div>
        </Form>
    );
};

export default withFormik({
    mapPropsToValues: () => ({
        email: '',
        password: '',
        repeatPassword: '',
        username: '',
        permission: false,
        captch: true,
        userRole: 'BS',
        phone: '',
        onChangeTwoFactor: false,
        // country: '',
    }),
    handleSubmit: (values, { setErrors, props: { registration, history } }) => {
        if (values.password !== values.repeatPassword) {
            setErrors({
                password: 'Паролі не співпадають',
                repeatPassword: 'Паролі не співпадають',
            });
            return;
        }
        registration(
            {
                email: values.email,
                firstName: values.username,
                confirmPassword: values.repeatPassword,
                password: values.password,
                phone: values.phone,
                twoFactorAuthEnabled: values.onChangeTwoFactor,
                userRole: values.userRole,
                // userCountry: values.country,
            },
            // history,
        );
    },

    validationSchema: RegistrationSchema,
    displayName: 'RegistrationForm',
})(RegistrationForm);
