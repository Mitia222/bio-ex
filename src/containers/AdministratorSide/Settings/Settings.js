import React, { useState, useEffect } from 'react';
import api from '../../../sevices/adminSide/settings';
import './settings.scss';

import RegistrationSettings from './RegistrationSettings';

import { notification } from 'antd';

const Settings = () => {
    const [mainRegisterState, setRegister] = useState(false);
    useEffect(() => {
        fetchMainRegister();
    }, []);
    const fetchMainRegister = () =>
        api
            .getRegistrationStatus()
            .then(({ data }) => setRegister(data.allowRegister));
    const handleChangeMainRegister = (val) => {
        api.updateRegister({ allowRegister: val }).then(({ data }) => {
            if (data.allowRegister) {
                notification.success({
                    message: 'Регістрація ввімкнуто!',
                });
            } else {
                notification.success({
                    message: 'Регістрація вимкнуто!',
                });
            }
            setRegister(data.allowRegister);
        });
    };
    return (
        <div className="settings-page">
            <RegistrationSettings
                onChange={handleChangeMainRegister}
                value={mainRegisterState}
            />

            {/*<EmailSettings*/}
            {/*    params={[]}*/}
            {/*    // onChange={this.handlerChangeEmailSettings}*/}
            {/*    // openEmail={this.handleOpenEmail}*/}
            {/*/>*/}
        </div>
    );
};

export default Settings;
