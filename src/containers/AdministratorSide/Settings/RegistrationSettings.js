import React from 'react';
import { Switch } from 'antd';

const RegistrationSettings = ({ onChange, value }) => {
    return (
        <div className="registration-settings section">
            <h1>Реєстрація</h1>

            <label>Вимкнута</label>
            <Switch
                className="settings-switch"
                checked={value}
                onChange={val =>  onChange(val)}
            />

            <label>Увімкнена</label>
        </div>
    );
};

export default RegistrationSettings;
