import React from 'react';
import { BASE_IMG_URL } from '../../../../constants/APIURLS';

const UserDocuments = ({ documents }) => {
    return (
        <div className="admin-table user-documents">
            <div className="title-table">Скани документів</div>
            {documents.map(({ imageDecoded }, idx) => (
                <img
                    height="150"
                    width="200"
                    key={idx}
                    src={BASE_IMG_URL + imageDecoded}
                    alt={""}
                />
            ))}
            {!documents.length && <div style={{fontSize: 30, textAlign: "center"}}>Відсутні</div>}
        </div>
    );
};

export default UserDocuments;
