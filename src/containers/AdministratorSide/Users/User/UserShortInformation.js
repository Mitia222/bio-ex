import React, { Fragment, useState } from 'react';
import { Icon } from 'antd';

const UserShortInformation = ({ user, editUserProfile, updateUserStatus }) => {
    const [reason, setReason] = useState('');
    const userInitials =
        user.firstName && user.lastName
            ? `${user.firstName} ${user.lastName}`
            : user.firstName
            ? user.firstName
            : user.lastName
            ? user.lastName
            : `Ім'я не вказанно`;

    return (
        <div className="short-info section">
            <div className="user-id">
                <span>id {user.id}</span>
                <span>{user.createdDate}</span>
            </div>

            <div className="user-name">
                <span className="name">{userInitials}</span>
                <div>
                    {user.verificationStatus === 'V' ? (
                        <Fragment>
                            <span className="verification-status">
                                <Icon type="check" /> Верифікований
                            </span>
                            <span
                                className="verification-action"
                                onClick={editUserProfile({
                                    verificationStatus: 'N',
                                })}
                            >
                                Скасувати
                            </span>
                        </Fragment>
                    ) : (
                        <Fragment>
                            <span className="verification-status notVerified">
                                Неверифікований
                            </span>
                            <span
                                className="verification-action"
                                onClick={editUserProfile({
                                    verificationStatus: 'V',
                                })}
                            >
                                Увімкнути
                            </span>
                        </Fragment>
                    )}
                </div>
            </div>

            <div className="user-email">
                <div>
                    <div>Email:</div>
                    <span>{user.email}</span>
                </div>

                <span>
                    <Icon
                        type={user.twoFactorAuthEnabled ? 'check' : 'close'}
                        style={{ color: !user.twoFactorAuthEnabled && 'red' }}
                    />
                    2-факторна аутентифікація
                </span>
            </div>

            {/* <div className="calculation-select">
                <div className="form-item">
                    <label htmlFor="">Форма розрахунку</label>
                    <Select placeholder="Оберіть">
                        <Option value="Оберіть">Оберіть</Option>
                    </Select>
                </div>
            </div> */}

            <div className="blocked-block">
                <div className="status">
                    <label htmlFor="">Статус</label>
                    {user.isActive ? (
                        <span>Активний</span>
                    ) : (
                        <span style={{ color: 'red' }}>НеАктивний</span>
                    )}
                </div>
                {user.isActive ? (
                    <div className="form-item">
                        <input
                            type="text"
                            placeholder="Причина"
                            onChange={(e) => setReason(e.target.value)}
                            value={reason}
                        />
                        <button
                            className="btn btn-red"
                            onClick={() =>
                                updateUserStatus({ isActive: false, reason })
                            }
                        >
                            Заблокувати
                        </button>
                    </div>
                ) : (
                    <div className="form-item">
                        <p className="status-reason">
                            {user.reason || 'Не вказанно'}
                        </p>
                        <button
                            className="btn btn-red"
                            onClick={editUserProfile({ isActive: true })}
                        >
                            Активувати
                        </button>
                    </div>
                )}
            </div>
        </div>
    );
};

export default UserShortInformation;
