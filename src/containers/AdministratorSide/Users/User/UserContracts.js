import React from 'react';
import { Table } from 'antd';
import columns from '../../../../helpers/columns';

const UserContracts = ({ contracts }) => {
    return (
        <div className="admin-table">
            <div className="title-table">Угоди</div>
            <Table
                dataSource={contracts}
                scroll={{ y: 240 }}
                columns={columns.userAdminsContractsColumns}
                pagination={false}
            />
        </div>
    );
};

export default UserContracts;
