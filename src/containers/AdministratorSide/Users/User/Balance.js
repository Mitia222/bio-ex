import React from 'react';

const Balance = ({ user, onOpenModal }) => {
    return (
        <div className="balance section">
            <div className="title">Баланс</div>

            <div className="balance-action">
                <div className="total-balance">
                    <span>Всі:</span>
                    <div>{user.balance} грн</div>
                </div>
                <div className="frozen-balance">
                    <span>Зарезервованні:</span>
                    <div>{user.frozenBalance} грн</div>
                </div>
                <button className="btn" onClick={onOpenModal}>
                    Поповнити
                </button>
            </div>
        </div>
    );
};

export default Balance;
