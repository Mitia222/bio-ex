import React from 'react';
import { Table } from 'antd';
import columns from '../../../../helpers/columns';

const UserBalance = ({ balance }) => {
    return (
        <div className="admin-table">
            <div className="title-table">Баланс</div>
            <Table
                dataSource={balance}
                scroll={{ y: 240 }}
                columns={columns.userAdminsBalanceCol}
            />
        </div>
    );
};

export default UserBalance;
