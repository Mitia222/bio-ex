import React, { useState } from 'react';

const UserBalanceForm = ({ updateUserBalance }) => {
    const [balance, setBalance] = useState('');
    const writeIn = () => {
        if (!balance.trim()) return;
        updateUserBalance({ amount: balance });
    };
    const writeOff = () => {
        if (!balance.trim()) return;
        updateUserBalance({ amount: '-' + balance });
    };
    return (
        <div className="top-up-balance">
            <div className="modal-title">Поповнити рахунок</div>

            <div className="form-item">
                <input
                    type="number"
                    value={balance}
                    onChange={e => setBalance(e.target.value)}
                />
                <label htmlFor="">Сума поповнення або списання</label>
            </div>

            <div className="buttons">
                <button className="btn btn-orange" onClick={writeOff}>
                    Списати з рахунку
                </button>
                <button className="btn" onClick={writeIn}>
                    Зарахувати
                </button>
            </div>
        </div>
    );
};
export default UserBalanceForm;
