import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { Tabs, Modal, notification } from 'antd';
import api from '../../../../sevices/adminSide/users';
import './user.scss';
import UserShortInformation from './UserShortInformation';
import Balance from './Balance';
import Accounts from './Accounts';
import KYC from './KYC';
import UserOrders from './UserOrders';
import UserContracts from './UserContracts';
import UserAccounts from './UserAccounts';
import UserDocuments from './UserDocuments';
import UserBalance from './UserBalance';
import UserBalanceForm from './UserBalanceForm';

const TabPane = Tabs.TabPane;

class User extends Component {
    state = {
        user: {
            account: {},
            createdDate: '',
            documentImages: [],
            email: '',
            firstName: '',
            id: '',
            isActive: false,
            isStaff: false,
            lastName: '',
            nin: '',
            organization: {},
            phone: '',
            stock: {},
            twoFactorAuthEnabled: false,
            userCountry: '',
            verificationStatus: null,
        },
        balance: [],
        contracts: [],
        orders: [],
        bills: [],
        visible: false,
    };
    componentDidMount() {
        const { id } = this.props.match.params;
        this.fetchUser(id);
    }
    fetchUser = async id => {
        try {
            const { data } = await api.getUserById(id);
            this.setState({
                user: data.profile,
                balance: data.balance,
                contracts: data.contracts,
                orders: data.orders,
                bills: data.bills
            });
        } catch (e) {
            console.log(e);
        }
    };
    showModal = () => {
        this.setState({
            visible: true,
        });
    };

    handleCancel = e => {
        this.setState({
            visible: false,
        });
    };
    editUserProfile = value => async () => {
        const { id } = this.props.match.params;
        try {
            const { data } = await api.updateUserProfile(id, value);
            console.log(data);
            this.setState({
                user: data,
            });
        } catch (e) {
            console.log(e);
        }
    };
    updateUserStatus = async userStatus => {
        try {
            const { data } = await api.updateUserStatus(
                this.state.user.id,
                userStatus,
            );
            this.setState({
                ...this.state,
                user: {
                    ...this.state.user,
                    isActive: data.isActive,
                    reason: data.reason,
                },
            });
        } catch (e) {
            console.log(e);
        }
    };
    updateUserBalance = async newbalanceData => {
        try {
            const { data } = await api.updateBalance(
                this.state.user.id,
                newbalanceData,
            );
            this.setState({
                user: { ...this.state.user, balance: data.balance },
                visible: false,
            });
            if (+newbalanceData > 0) {
                notification.success({
                    message: 'Кошти зарахованно',
                });
            } else {
                notification.success({
                    message: 'Кошти списанно',
                });
            }
        } catch (e) {
            if (e.response) {
                notification.error({
                    message: e.response.data.error,
                });
            }
        }
    };

    render() {
        const { user, balance, contracts, orders,bills } = this.state;
        // console.log('STATE USER', this.state);
        return (
            <div className="user-page">
                <div className="back-link">
                    <Link to="/admin/users">Користувачі</Link>
                    <span> > {user.firstName || `Куристувач ${user.id}`}</span>
                </div>

                <UserShortInformation
                    user={user}
                    editUserProfile={this.editUserProfile}
                    updateUserStatus={this.updateUserStatus}
                />

                <Balance onOpenModal={this.showModal} user={user} />

                <Accounts />

                <div className="tabs-block">
                    <Tabs defaultActiveKey="1" animated={false}>
                        <TabPane tab="KYC" key="1">
                            <KYC user={user} />
                        </TabPane>

                        <TabPane tab="Замовлення" key="2">
                            <UserOrders orders={orders} />
                        </TabPane>

                        <TabPane tab="Угоди" key="3">
                            <UserContracts contracts={contracts} />
                        </TabPane>

                        <TabPane tab="Рахунки" key="4">
                            <UserAccounts bills={bills}/>
                        </TabPane>

                        <TabPane tab="Скани документів" key="5">
                            <UserDocuments documents={user.documentImages} />
                        </TabPane>

                        <TabPane tab="Баланс" key="6">
                            <UserBalance balance={balance} />
                        </TabPane>
                    </Tabs>
                </div>

                <Modal
                    footer={null}
                    visible={this.state.visible}
                    onOk={this.handleOk}
                    onCancel={this.handleCancel}
                >
                    <UserBalanceForm
                        closeModal={this.handleCancel}
                        updateUserBalance={this.updateUserBalance}
                    />
                </Modal>
            </div>
        );
    }
}

export default User;
