import React from 'react';
import { Table } from 'antd';
import columns from '../../../../helpers/columns';

const UserOrders = ({ orders }) => {
    return (
        <div className="admin-table">
            <div className="title-table">Замовлення</div>
            <Table
                dataSource={orders}
                scroll={{ y: 240 }}
                columns={columns.userAdminsColumns}
                pagination={false}
            />
        </div>
    );
};

export default UserOrders;
