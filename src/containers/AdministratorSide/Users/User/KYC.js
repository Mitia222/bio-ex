import React from 'react';
import _ from 'lodash';
import regions from '../../../../constants/regions';
const organizationMap = ['ТОВ', 'ПП'];
// const userTypes = ['Невказанно', 'Продавець', 'Покупець', 'Продавець-покупець'];

const KYC = ({ user }) => {
    console.log(user);
    const lookForRegion =
        user.stock && regions.find(reg => reg.id === user.stock.stockRegion)
            ? regions.find(reg => reg.id === user.stock.stockRegion).name
            : '-';

    return (
        <div className="kyc-block">
            <div>
                <div className="block-title">
                    Персональна <br />
                    інформація
                </div>

                <div className="item">
                    <label htmlFor="">Ім'я</label>
                    <div className="value">{user.firstName || '-'}</div>
                </div>
                <div className="item">
                    <label htmlFor="">Прізвище</label>
                    <div className="value">{user.lastName || '-'}</div>
                </div>
                <div className="item">
                    <label htmlFor="">Email</label>
                    <div className="value">{user.email || '-'}</div>
                </div>
                <div className="item">
                    <label htmlFor="">Телефон</label>
                    <div className="value">{user.phone || '-'}</div>
                </div>
                <div className="item">
                    <label htmlFor="">ІНН</label>
                    <div className="value">{user.nin || '-'}</div>
                </div>
            </div>

            <div>
                <div className="block-title">Юридичні данні</div>

                <div className="item">
                    <label htmlFor="">Організаційно-правова форма</label>
                    <div className="value">
                        {
                            organizationMap[
                                _.get(
                                    user,
                                    'organization.organizationType',
                                    '-',
                                ) - 1
                            ]
                        }
                    </div>
                </div>
                <div className="item">
                    <label htmlFor="">Організація</label>
                    <div className="value">
                        {_.get(user, 'organization.name', '-')}
                    </div>
                </div>
                <div className="item">
                    <label htmlFor="">ЕДРПОУ</label>
                    <div className="value">
                        {_.get(user, 'organization.usreou', '-')}
                    </div>
                </div>
                <div className="item">
                    <label htmlFor="">Посвідчення платника ПДВ</label>
                    <div className="value">
                        {_.get(user, 'organization.pdv', '-')}
                    </div>
                </div>
                <div className="item">
                    <label htmlFor="">ПІБ керуючого</label>
                    <div className="value">
                        {_.get(user, 'organization.headPosition', '-')}
                    </div>
                </div>
                <div className="item">
                    <label htmlFor="">Посада керівника</label>
                    <div className="value">
                        {_.get(user, 'organization.headPosition', '-')}
                    </div>
                </div>
                <div className="item">
                    <label htmlFor="">Місто, область, країна</label>
                    <div className="value">
                        {_.get(user, 'organization.organizationCountry', '-')}
                    </div>
                </div>
                <div className="item">
                    <label htmlFor="">Вулиця</label>
                    <div className="value">
                        {_.get(user, 'organization.organizationStreet', '-')}
                    </div>
                </div>
                <div className="item">
                    <label htmlFor="">Район</label>
                    <div className="value">
                        {_.get(user, 'organization.organizationRegion', '-')}
                    </div>
                </div>
                <div className="item">
                    <label htmlFor="">Індекс</label>
                    <div className="value">
                        {_.get(user, 'organization.organizationIndex', '-')}
                    </div>
                </div>
            </div>

            <div>
                <div className="block-title">Локація складу</div>
                <div className="item">
                    <label htmlFor="">Місто</label>
                    <div className="value">
                        {user.stock ? user.stock.stockCity : '-'}
                    </div>
                </div>
                <div className="item">
                    <label htmlFor="">Область</label>
                    <div className="value">
                        {user.stock ? lookForRegion : '-'}
                    </div>
                </div>

                <div className="item">
                    <label htmlFor="">Країна</label>
                    <div className="value">
                        {user.stock ? user.stock.stockCountry : '-'}
                    </div>
                </div>
                <div className="item">
                    <label htmlFor="">Вулиця</label>
                    <div className="value">
                        {user.stock ? user.stock.stockStreet : '-'}
                    </div>
                </div>
                <div className="item">
                    <label htmlFor="">Номер дома</label>
                    <div className="value">
                        {user.stock ? user.stock.stockBuilding : '-'}
                    </div>
                </div>
            </div>

            <div>
                <div className="block-title">
                    Платіжна <br /> інформація
                </div>

                <div className="item">
                    <label htmlFor="">Назва банку</label>
                    <div className="value">
                        {_.get(user, 'account.bank', '-')}
                    </div>
                </div>
                <div className="item">
                    <label htmlFor="">МФО</label>
                    <div className="value">
                        {_.get(user, 'account.sortCode', '-')}
                    </div>
                </div>
                <div className="item">
                    <label htmlFor="">Розрахунковий рахунок</label>
                    <div className="value">
                        {_.get(user, 'account.currentAccount', '-')}
                    </div>
                </div>
            </div>
        </div>
    );
};

export default KYC;
