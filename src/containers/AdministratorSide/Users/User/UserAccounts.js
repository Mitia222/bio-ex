import React from 'react';
import {Table} from 'antd';
import moment from 'moment';

const Statuses = ({status}) => {
    const classNames= status === 1 ? "bill-red" : status === 2 ? 'bill-green' : "bill-yellow"
    return <div className={classNames}>{status === 1 ? "Несплачено" : status === 2 ? "Сплачено" : "Прострочено"}</div>
}

const UserAccounts = ({bills}) => {
    const columns = [

        {
            title: 'ID транзакції',
            dataIndex: 'id',
            key: 'id',
        },
        {
            title: 'Номер рахунку',
            dataIndex: 'accountNumber',
            key: 'accountNumber',
        },
        {
            title: 'Дата виставлення',
            dataIndex: 'create',
            key: 'create',
            render: (e, {creationDate}) =>  creationDate
                ? moment(
                    creationDate,
                ).format('L')
                : '-',
        },
        {
            title: 'Дійсний до',
            dataIndex: 'expire_on',
            key: 'expire_on',
            render: (e, {expDate}) =>  expDate
                ? moment(
                    expDate,
                ).format('L')
                : '-',
        },
        {
            title: 'Статус',
            dataIndex: 'status',
            key: 'status',
            render: (a, {status}) => <Statuses status={status}/>
        },
        {
            title: 'Сума',
            dataIndex: 'amount',
            key: 'amount',
            render: (e, {amount}) => amount + " грн"
        }
    ];

    return (
        <div className='admin-table'>
            <div className="title-table">Рахунки</div>
            <Table
                dataSource={bills}
                columns={columns}
                scroll={{y: 300}}
                pagination={false}
            />
        </div>
    )
};

export default UserAccounts;
