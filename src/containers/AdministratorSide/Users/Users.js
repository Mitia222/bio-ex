import React  from 'react';
import { connect } from 'react-redux';
import './users.scss';
import Filters from '../components/Filters/Filters';
import { onChangeFilters } from '../../../store/actions/Admin/filters';
import UsersList from './UsersList';

const Users = ({ location, count, users, history, onChangeFilters }) => {
    const openUser = ({ id }) => () => {
        history.push(`/admin/users/${id}/`);
    };

    return (
        <div className="users-page">
            <Filters page="users" location={location} />

            <UsersList
                users={users}
                totalCount={count}
                changePage={onChangeFilters}
                openUser={openUser}
            />
        </div>
    );
};

const mapStateToProps = state => ({
    count: state.allUsers.total,
    users: state.allUsers.users,
});

const mapDispatchToProps = dispatch => ({
    onChangeFilters: (field, val) => dispatch(onChangeFilters(field, val)),
});

export default connect(
    mapStateToProps,
    mapDispatchToProps,
)(Users);
