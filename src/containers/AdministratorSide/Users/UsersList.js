import React from 'react';
import { Table } from 'antd';
import columns from '../../../helpers/columns';

const UsersList = ({ openUser, users, totalCount, changePage }) => {
    console.log(users.length);
    const pagination = {
        pageSize: 10,
        total: totalCount,
        onChange: val => changePage('page', val),
    };
    console.log(users);
    return (
        <div className="admin-table section">
            <div className="title-table">Користувачі</div>
            <Table
                dataSource={users}
                columns={columns.userColumns}
                pagination={pagination}
                scroll={{ x: 900 }}
                onRow={record => ({
                    onClick: openUser(record), // click row
                })}
            />
        </div>
    );
};

export default UsersList;
