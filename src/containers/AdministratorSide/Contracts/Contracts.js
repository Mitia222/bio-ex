import React, { useEffect } from 'react';
import './contracts.scss';
import ContractsStatistics from '../components/ContractsStatistics/ContractsStatistics';
import Filters from '../components/Filters/Filters';
import ContractsList from './ContractsList';
import { connect } from 'react-redux';
import { onChangeFilters } from '../../../store/actions/Admin/filters';
import {
    // fetchContracts,
    fetchContractsStatistics,
    // fetchInitialContract,
} from '../../../store/actions/Admin/contracts';

const Contracts = ({
    history,
    location,
    getStatistic,
    allStatistics,
    contract,
    // getAllContracts,
    onChangeFilters,
}) => {
    useEffect(() => {
        getStatistic();
    }, []);
    const openContract = id => {
        history.push(`./contracts/${id}/`);
    };

    return (
        <div className="contracts-page wr-padding">
            <ContractsStatistics data={allStatistics} />

            <Filters location={location} page="contracts" />

            <ContractsList
                changePage={onChangeFilters}
                openContract={openContract}
                data={contract}
            />
        </div>
    );
};

const mapStateToProps = state => ({
    allStatistics: state.contracts.contractsStatistics,
    contract: state.contracts.allContracts,
});
const mapDispatchToProps = dispatch => ({
    getStatistic: () => dispatch(fetchContractsStatistics()),
    onChangeFilters: (field, val) => dispatch(onChangeFilters(field, val)),
    // getAllContracts: page => dispatch(fetchContracts(page)),
});

export default connect(
    mapStateToProps,
    mapDispatchToProps,
)(Contracts);
