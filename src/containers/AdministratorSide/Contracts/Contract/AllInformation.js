import React from 'react';

const AllInformation = ({ data, handleChange, state, update }) => {
    return (
        <div className="all-info section">
            <div className="seller-block block">
                <div className="title">
                    Продавець
                    <span className="id">id {data.users.seller.id}</span>
                </div>

                <div className="form-tem">
                    <label htmlFor="">Телефон</label>
                    <span>{data.users.seller.phone || 'Відсутній'}</span>
                </div>
                <div className="form-tem">
                    <label htmlFor="">email</label>
                    <span>{data.users.seller.email || 'Відсутній'}</span>
                </div>
            </div>

            <div className="buyer-block block">
                <div className="title">
                    Покупець
                    <span className="id">id {data.users.buyer.id}</span>
                </div>

                <div className="form-tem">
                    <label htmlFor="">Телефон</label>
                    <span>{data.users.buyer.phone || 'Відсутній'}</span>
                </div>
                <div className="form-tem">
                    <label htmlFor="">email</label>
                    <span>{data.users.buyer.email || 'Відсутній'}</span>
                </div>
            </div>

            <div className="detailed-information block">
                <div className="title">Детальна інформація</div>

                <div className="form-tem">
                    <label htmlFor="">Категорія</label>
                    <span>{data.applicationData.category.name}</span>
                </div>
                <div className="form-tem">
                    <label htmlFor="">Ціна</label>
                    <span>{data.price} грн</span>
                </div>
                <div className="form-tem">
                    <label htmlFor="">Обсяг</label>
                    <span>
                        {data.applicationData.purchaseVolume}{' '}
                        {data.applicationData.units.shortName}
                    </span>
                </div>
                <div className="form-tem">
                    <label htmlFor="">Всього</label>
                    <span>{data.sum} грн</span>
                </div>
                <div className="form-tem">
                    <label htmlFor="">Відстань</label>
                    <span>{data.distance.text}</span>
                </div>
                <div className="form-tem">
                    <label htmlFor="">Вартість доставки</label>
                    <span>{data.deliveryPrice} грн</span>
                </div>
                <div className="form-tem">
                    <label htmlFor="">Формат доставки</label>
                    <span>{data.deliveryFormat}</span>
                </div>
            </div>

            <div className="comment block">
                <div className="title">Коментарі</div>

                <div className="form-item">
                    <textarea
                        value={state.comment}
                        name=""
                        id=""
                        cols="30"
                        rows="13"
                        onChange={e => handleChange('comment', e.target.value)}
                    ></textarea>
                </div>

                <button className="btn" onClick={() => update(state)}>
                    Зберегти
                </button>
            </div>
        </div>
    );
};

export default AllInformation;
