import React from 'react';
import moment from 'moment';
import CustomSelect from '../../../CustomSelect';

const ShortInformation = ({
    optionswithDisabling,
    data,
    state,
    handleChange,
}) => {
    return (
        <div className="short-information section">
            <div className="id">
                <span>Угода ID {data.id}</span>
                <span className="date">
                    {`${moment(data.creationDate).format('LT')} / ${moment(
                        data.creationDate,
                    ).format('L')}`}
                </span>
            </div>

            <div className="first-row row">
                <div className="form-item">
                    <label htmlFor="">Замовлення створено</label>
                    <span>{data.type}</span>
                </div>

                {/* <div className="form-item">
                    <label htmlFor="">Орієнтовний термін доставки</label>
                    <Select>
                        <Option value="1">Чекає оплати</Option>
                        <Option value="1">Чекає відгрузки</Option>
                    </Select>
                </div> */}
            </div>

            <div className="second-row row">
                <CustomSelect
                    options={optionswithDisabling}
                    value={state.applicationType}
                    label="Статус угоди"
                    handleChange={val => handleChange('applicationType', val)}
                />
                {/*
                <div className="form-item">
                    <label htmlFor="">Статус рахунка</label>
                    <Select>
                        <Option value="1">Чекає оплати</Option>
                    </Select>
                </div> */}
            </div>
        </div>
    );
};

export default ShortInformation;
