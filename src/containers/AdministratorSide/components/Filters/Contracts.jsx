import React, { useState } from 'react';
import { Icon, DatePicker } from 'antd';
import CustomSelect from '../../../CustomSelect';
import { statuses } from '../../../../helpers/status';

const { RangePicker } = DatePicker;

const ContractsFilters = ({
    filters,
    onChangeFilters,
    allFilters,
    clearFilters,
}) => {
    const [datePicker, setDatepicker] = useState([]);

    return (
        <div className="filters section contracts">
            <div className="form-item search">
                <label htmlFor="">
                    ID контрагента, ID угоди або номер рахунку
                </label>
                <div style={{ position: 'relative' }}>
                    <input
                        type="text"
                        value={allFilters.id}
                        onChange={(e) => onChangeFilters('id', e.target.value)}
                    />
                    <Icon type="search" style={{ right: 10 }} />
                </div>
            </div>
            <CustomSelect
                options={filters}
                value={allFilters.category}
                handleChange={(val) => onChangeFilters('category', val)}
                label="Категорія"
            />

            <CustomSelect
                options={statuses}
                value={allFilters.status}
                handleChange={(val) => onChangeFilters('status', val)}
                label="Статус угоди"
            />

            <div className="form-item">
                <label htmlFor="">Дата</label>
                <RangePicker
                    onChange={(moment, [created_after, created_before]) => {
                        onChangeFilters('date', {
                            created_after,
                            created_before,
                        });
                        setDatepicker(moment);
                    }}
                    value={allFilters.date ? datePicker : null}
                    placeholder={false}
                />
            </div>

            <div className="form-item distance">
                <label htmlFor="">Сумма, грн</label>
                <div className="range-container">
                    <div className="range-input">
                        <div>Від</div>
                        <input
                            type="text"
                            value={allFilters.total_min}
                            onChange={(e) =>
                                onChangeFilters('total_min', e.target.value)
                            }
                            placeholder="Від"
                        />
                    </div>
                    <div className="range-input">
                        <div>До</div>
                        <input
                            type="text"
                            value={allFilters.total_max}
                            placeholder="До"
                            onChange={(e) =>
                                onChangeFilters('total_max', e.target.value)
                            }
                        />
                    </div>
                </div>
            </div>
            <div className="form-item distance">
                <label htmlFor="">Відстань, км</label>
                <div className="range-container">
                    <div className="range-input">
                        <div>Від</div>
                        <input
                            type="text"
                            value={allFilters.distance_min}
                            onChange={(e) =>
                                onChangeFilters('distance_min', e.target.value)
                            }
                            placeholder="Від"
                        />
                    </div>
                    <div className="range-input">
                        <div>До</div>
                        <input
                            type="text"
                            value={allFilters.distance_max}
                            placeholder="До"
                            onChange={(e) =>
                                onChangeFilters('distance_max', e.target.value)
                            }
                        />
                    </div>
                </div>
            </div>

            <div className="form-item distance ct">
                <label htmlFor="">Вартість доставки, грн</label>
                <div className="range-container">
                    <div className="range-input">
                        <div>Від</div>
                        <input
                            type="text"
                            value={allFilters.delivery_price_min}
                            onChange={(e) =>
                                onChangeFilters(
                                    'delivery_price_min',
                                    e.target.value,
                                )
                            }
                            placeholder="Від"
                        />
                    </div>
                    <div className="range-input">
                        <div>До</div>
                        <input
                            type="text"
                            value={allFilters.delivery_price_max}
                            placeholder="До"
                            onChange={(e) =>
                                onChangeFilters(
                                    'delivery_price_max',
                                    e.target.value,
                                )
                            }
                        />
                    </div>
                </div>
            </div>

            <button className="btn reset" onClick={() => clearFilters()}>
                Скасувати фільтри
            </button>
        </div>
    );
};

export default ContractsFilters;
