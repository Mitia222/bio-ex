import React, { useState } from 'react';
import { Icon, DatePicker } from 'antd';
import CustomSelect from '../../../CustomSelect';

const types = [
    {
        id: 'B',
        name: 'Поповнення',
    },
    {
        id: 'CE',
        name: 'Резервування комісії',
    },
    {
        id: 'CR',
        name: 'Повернення комісії',
    },
    {
        id: 'CW',
        name: 'Списання комісії',
    },
    {
        id: 'CG',
        name: 'Отримання комісії',
    },
];

const CommissionFilters = ({ values, onChangeFilters, clearFilters }) => {
    const [dateForComponent, setDate] = useState(null);
    return (
        <div className="filters section">
            <div className="form-item search">
                <input
                    type="text"
                    value={values.search}
                    onChange={(e) => onChangeFilters('search', e.target.value)}
                />
                <Icon type="search" />
                <label htmlFor="">ID контрагента, ID угоди</label>
            </div>

            <CustomSelect
                options={types}
                value={values.type}
                handleChange={(val) => onChangeFilters('type', val)}
                label="Тип"
            />

            <div className="form-item">
                <label htmlFor="">Дата створення</label>
                <DatePicker
                    value={dateForComponent}
                    onChange={(moment, date) => {
                        onChangeFilters('created', date);
                        setDate(moment);
                    }}
                    placeholder={false}
                />
            </div>
            <div className="form-item" style={{ width: 'unset' }}>
                <label htmlFor="">Розмір комісії</label>
                <div className="range-container">
                    <div className="range-input">
                        <div>Від</div>
                        <input
                            type="number"
                            value={values.commission_min}
                            onChange={(e) =>
                                onChangeFilters(
                                    'commission_min',
                                    e.target.value,
                                )
                            }
                            placeholder="Від"
                        />
                    </div>
                    <div className="range-input">
                        <div>До</div>
                        <input
                            type="number"
                            value={values.commission_max}
                            placeholder="До"
                            onChange={(e) =>
                                onChangeFilters(
                                    'commission_max',
                                    e.target.value,
                                )
                            }
                        />
                    </div>
                </div>
            </div>
            <button className="btn reset" onClick={() => clearFilters()}>
                Скасувати фільтри
            </button>
        </div>
    );
};
export default CommissionFilters;
