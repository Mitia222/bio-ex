import React, { useEffect, useState } from 'react';
import { fetchContracts } from '../../../../store/actions/Admin/contracts';
import { getAdminUsers } from '../../../../store/actions/Admin/Users';
import { fetchAllProductsData } from '../../../../store/actions/productsandfilters';
import { fetchCommissions } from '../../../../store/actions/Admin/commissions';
import { onChangeFilters } from '../../../../store/actions/Admin/filters';
import { connect } from 'react-redux';
import CommissionFilters from './Commission';
import ContractsFilters from './Contracts';
import AccountsFilters from './Accounts';
import UsersFilters from './Users';
import './filters.scss';
import { setInitialFilters } from '../../../../store/actions/Admin/filters';
import api from '../../../../sevices/adminSide/settings';
import DefaultFiltersState from '../../../../constants/filtersState';
import { getCategories } from '../../../../helpers/functions';
import { fetchAccounts } from '../../../../store/actions/Admin/accounts';

const Filters = ({
    page,
    allFilters,

    categoryFilters,
    categoryDefaultFilters,
    fetchContracts,
    onChangeFilters,
    setInitialFilters,
    location,
    fetchForUsers,
    fetchAccounts,
    fetchCommissions,
}) => {
    const [defaultFilters, setToDefault] = useState({});
    useEffect(() => {
        if (page === 'contracts') {
            fetchContracts();
        } else if (page === 'users') {
            fetchForUsers();
        } else if (page === 'commissions') {
            fetchCommissions();
        } else if (page === 'accounts') {
            fetchAccounts();
        }
    }, [allFilters]);
    useEffect(() => {
        if (page === 'users') {
            setInitialFilters(DefaultFiltersState[location.pathname]);
            setToDefault(DefaultFiltersState[location.pathname]);
        } else if (page === 'contracts' || page === 'commissions') {
            getFormFitlers();
            // fetchContracts();
        } else if (page === 'accounts') {
            setInitialFilters(DefaultFiltersState[location.pathname]);
        } else {
            categoryFilters();
        }
    }, []);
    const getFormFitlers = () =>
        api.getMinMaxContractsFilters().then(({ data }) => {
            const obj =
                page === 'contracts'
                    ? {
                          ...DefaultFiltersState[location.pathname],
                          total_max: data.totalMax,
                          delivery_price_max: data.deliveryPriceMax,
                          distance_max: data.distanceMax,
                      }
                    : {
                          ...DefaultFiltersState[location.pathname],
                          commission_max: data.commissionMax,
                      };
            setToDefault(obj);
            setInitialFilters(obj);
        });
    if (page === 'users') {
        return (
            <UsersFilters
                onChangeFilters={onChangeFilters}
                values={allFilters}
                clearFilters={() => setInitialFilters(defaultFilters)}
            />
        );
    } else if (page === 'accounts') {
        return (
            <AccountsFilters
                allFilters={allFilters}
                onChangeFilters={onChangeFilters}
                clearFilters={() => setInitialFilters(defaultFilters)}
            />
        );
    } else if (page === 'contracts') {
        return (
            <ContractsFilters
                allFilters={allFilters}
                filters={categoryDefaultFilters}
                onChangeFilters={onChangeFilters}
                clearFilters={() => setInitialFilters(defaultFilters)}
            />
        );
    } else if (page === 'commissions') {
        return (
            <CommissionFilters
                clearFilters={() => setInitialFilters(defaultFilters)}
                values={allFilters}
                onChangeFilters={onChangeFilters}
            />
        );
    }
};

const mapStateToProps = (state) => ({
    allFilters: state.filtersReducer,
    categoryDefaultFilters: getCategories(state.products),
});

const mapDispatchToProps = (dispatch) => ({
    categoryFilters: () => dispatch(fetchAllProductsData()),
    fetchContracts: (page) => dispatch(fetchContracts(page)),
    onChangeFilters: (field, val) => dispatch(onChangeFilters(field, val)),
    fetchForUsers: () => dispatch(getAdminUsers()),
    setInitialFilters: (pathName) => dispatch(setInitialFilters(pathName)),
    fetchCommissions: (filters) => dispatch(fetchCommissions(filters)),
    fetchAccounts: () => dispatch(fetchAccounts()),
});

export default connect(mapStateToProps, mapDispatchToProps)(Filters);
