import React from 'react';
import { Table } from 'antd';
import columns from '../../../../helpers/columns';
import ugodi from '../../../../assets/img/icons/ugodi.svg';

import './contractsstatistics.scss';

const ContractsStatistics = ({ data }) => {
    return (
        <div className="contracts-statistics section">
            <div className="block-title">
                <img src={ugodi} alt="" />
                Угоди
            </div>

            <div className="admin-table">
                <Table
                    dataSource={data}
                    columns={columns.contractsStatisticColumns}
                    pagination={false}
                />
            </div>
        </div>
    );
};

export default ContractsStatistics;
