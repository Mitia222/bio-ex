import React from 'react';

import SideNav, { NavItem, NavIcon, NavText } from '@trendmicro/react-sidenav';
import { withRouter } from 'react-router-dom';
import '@trendmicro/react-sidenav/dist/react-sidenav.css';
import { Icon } from 'antd';
import './navigationbar.scss';

const NavBar = ({ location, history }) => {
    const menu = [
        {
            title: 'Інформаційна панель',
            href: 'dashboard',
            iconName: 'form',
            //   icon: dashboard,
            //   activeIcon: dashboardActive
        },
        {
            title: 'Користувачі',
            href: 'users',
            iconName: 'user',
        },
        //   icon: users,
        //   activeIcon: usersActive

        {
            title: 'Рахунки',
            href: 'accounts',
            iconName: 'container',
            // icon: accounts,
            // activeIcon: accountsActive
        },
        {
            title: 'Угоди',
            href: 'contracts',
            iconName: 'dollar',
            // icon: agreements,
            // activeIcon: agreementsActive
        },
        {
            title: 'Комісія',
            href: 'commissions',
            iconName: 'pie-chart',
            // icon: commissions,
            // activeIcon: commissionsActive
        },
        {
            title: 'Новини',
            href: 'news',
            iconName: 'calculator',
        },

        {
            title: 'Права доступу',
            href: 'access_rights',
            iconName: 'copyright',
        },
        {
            title: 'Юридичні дані',
            href: 'legal_data',
            iconName: 'snippets',
        },
    ];

    return (
        <div className="navigation-bar">
            <SideNav
                onSelect={(selected) => {
                    const to = selected;
                    if (location.pathname !== to) {
                        history.push(to);
                    }
                }}
            >
                <SideNav.Toggle />
                <SideNav.Nav defaultSelected={location.pathname}>
                    {menu.map((item) => (
                        <NavItem
                            eventKey={`/admin/${item.href}`}
                            key={item.title}
                        >
                            <NavIcon>
                                {/* <img src={item.icon} alt="" className="icon" /> */}
                                <Icon
                                    type={item.iconName}
                                    style={{ fontSize: 22 }}
                                />
                            </NavIcon>
                            <NavText style={{ color: '#fff' }}>
                                {item.title}
                            </NavText>
                        </NavItem>
                    ))}
                </SideNav.Nav>
            </SideNav>
        </div>
    );
};

export default withRouter(NavBar);
