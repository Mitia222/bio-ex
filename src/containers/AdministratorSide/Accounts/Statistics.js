import React, {useEffect,useState} from 'react';
import stop from '../../../assets/img/icons/stop.svg';
import paid from '../../../assets/img/icons/paid.svg';
import overdue from '../../../assets/img/icons/overdue.svg';
import api from '../../../sevices/adminSide/Accounts';

const Statistics = () => {
    const [statuses, setStatus] = useState({});
    const getData = async() => {
        try {
            const {data} = await api.getAllAcountsCount();
            setStatus(data)
        } catch (e) {
            console.log(e)
        }
    }
    useEffect(() => {
        getData()
    }, [])
    return(
        <div className="statistics">
            <div className='params-item'>
                <img src={stop} alt=""/>
                <label htmlFor="">Неcплачені рахунки</label>
                <span className='value red'>{statuses.notVerified ? statuses.notVerified : 0}</span>
            </div>
            <div className='params-item'>
                <img src={paid} alt=""/>
                <label htmlFor="">Cплачені</label>
                <span className='value green'>{statuses.verified ? statuses.verified : 0}</span>
            </div>
            <div className='params-item'>
                <img src={overdue} alt=""/>
                <label htmlFor="">Прострочені рахунки</label>
                <span className='value orange'>{statuses.overdue ? statuses.overdue : 0}</span>
            </div>
        </div>
    )
};

export default Statistics;
