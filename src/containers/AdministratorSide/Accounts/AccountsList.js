import React, { useEffect, useState } from 'react';
import {Table,Icon} from 'antd';
import { onChangeFilters } from '../../../store/actions/Admin/filters';
// import pdfIcon from '../../../assets/img/icons/pdf.svg';
import { useDispatch, useSelector } from 'react-redux';
import moment from 'moment';
import CustomSelect from '../../CustomSelect';
import api from "../../../sevices/adminSide/Accounts"

const filters = [ {
    id: 1,
    name: "Несплачені"
},{
    id: 2,
    name: "Сплачені"
},{
    id: 3,
    name: "Прострочені"
}];
const ColSelects = ({status, id}) => {
    const [value, setValue] = useState(status);

    const handleChange = (val) => {

        updateStatus(val).then(el =>   setValue(val))

    };
    useEffect(() => {
        setValue(status)
    } , [status])
    const updateStatus = (val) => api.updateStatus(id, {status: val});
    return <CustomSelect options={filters} value={value} handleChange={handleChange}/>
}

const AccountsList = () => {
    const dispatch = useDispatch();
    const count = useSelector(state => state.accounts.count);
    const items = useSelector(state => state.accounts.data);

    const columns = [
            {
                title: 'ID користувача',
                dataIndex: 'userId',
                key: 'id',
            },
            {
                title: 'Номер рахунку',
                dataIndex: 'accountNumber',
                key: 'accountNumber',
            },

            {
                title: 'Дата виставлення',
                dataIndex: 'date',
                key: 'date',
                render: (e, {creationDate}) =>  creationDate
                    ? moment(
                        creationDate,
                    ).format('L')
                    : '-',
            },
            {
                title: 'Дійсний до',
                dataIndex: 'date2',
                key: 'date2',
                render: (e, {expDate}) =>  expDate
                    ? moment(
                        expDate,
                    ).format('L')
                    : '-',
            },
            {
                title: 'Статус',
                dataIndex: 'status',
                key: 'status',
                // render: (a, {status}) => status === 1 ? "Несплачено" : status === 2 ? "Сплачено" : "Прострочено"
                render: (a, {status, id}) => <ColSelects status={status} id={id}/>
            },
            {
                title: 'Сума',
                dataIndex: 'amount',
                key: 'amount',
                render: (e, {amount}) => amount + " грн"
            },
            {
                title: '',
                dataIndex: 'actions',
                key: 'actions',
                render: (a,{invoiceFile}) => (
                    <a target='_blank' href={invoiceFile} rel="noopener noreferrer" >
                        <Icon style={{color: "#53be4f", fontSize: 25}} type="snippets" />
                    </a>
                )
            },
        ];



    return (
        <div className='admin-table section'>
            <div className="title-table">Рахунки</div>
            <Table
                pagination={{
                    // current: ,
                    pageSize: 10,
                    total: count,
                    onChange: val => dispatch(onChangeFilters('page', val)),
                }}
                dataSource={items}
                columns={columns}
            />
        </div>
    )
};

export default AccountsList;

