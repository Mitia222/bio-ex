import React, { useEffect } from 'react';
import Header from './components/Header/Header';
import NavBar from './components/NavBar/NavBar';
import { Logout } from '../../store/actions/user';
import { connect } from 'react-redux';

const AdministratorSide = props => {
    const logout = () => {
        props.logout();
        props.history.push('/');
    };
    return (
        <div className="admin-side">
            <Header logout={logout} />

            <div className="admin-size-container container">
                <NavBar {...props} />

                <div className="admin-content">{props.children}</div>
            </div>
        </div>
    );
};

const mapStateToProps = state => ({
    user: state.user,
});
const mapDispatchToProps = dispatch => ({
    logout: () => dispatch(Logout()),
});
export default connect(
    mapStateToProps,
    mapDispatchToProps,
)(AdministratorSide);
