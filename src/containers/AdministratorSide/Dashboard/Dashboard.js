import React, { useEffect, useState } from 'react';
import { connect } from 'react-redux';
import './dashboard.scss';
import api from '../../../sevices/adminSide/dashboard';
import SiteStatistics from './SiteStatistics';
import AccountsStatistics from './AccountsStatistics';
import ContractsStatistics from '../components/ContractsStatistics/ContractsStatistics';
import { fetchContractsStatistics } from '../../../store/actions/Admin/contracts';

const Dashboard = ({ allStatistics, getStatistic }) => {
    const [statistic, setStatisctic] = useState({
        totalCommission: 0,
        userCount: 0,
    });

    const fetchData = async () => {
        try {
            const { data } = await api.getStatistic();
            setStatisctic(data);
            // this.setState({ userCount: data.userCount });
        } catch (e) {
            console.log(e);
        }
    };
    useEffect(() => {
        fetchData();
        getStatistic();
    }, []);

    return (
        <div className="dashboard-page">
            <SiteStatistics statistic={statistic} />

            <AccountsStatistics />

            <ContractsStatistics data={allStatistics} />
        </div>
    );
};
const mapStateToProps = state => ({
    allStatistics: state.contracts.contractsStatistics,
});
const mapDispatchToProps = dispatch => ({
    getStatistic: () => dispatch(fetchContractsStatistics()),
});

export default connect(
    mapStateToProps,
    mapDispatchToProps,
)(Dashboard);
