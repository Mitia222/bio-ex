import React, { Component } from 'react';
import api from '../../../sevices/adminSide/legal';
import { notification } from 'antd';

export default class LegalCreate extends Component {
    state = {
        name: '',
        companyType: '',
        organization: '',
        bankName: '',
        settlementAccount: '',
        sortСode: '',
        headType: '',
        headName: '',
        vat: '',
        usreou: '',
    };
    handleChange = e => {
        this.setState({ [e.target.name]: e.target.value });
    };
    createLegal = async () => {
        const { history } = this.props;
        try {
            console.log(this.state);
            await api.createLegal(this.state);
            notification.success({
                message: 'Форма створенна!',
            });
            history.push('/admin/legal_data');
        } catch (e) {
            if (e.response.status === 400) {
                notification.error({
                    message: Object.values(e.response.data)[0],
                });
            }
            console.log(e);
        }
    };
    render() {
        return (
            <div className="legal-form">
                <div className="legal-title">
                    <h2>Створити органіхацію</h2>
                    <button className="btn" onClick={this.createLegal}>
                        Створити
                    </button>
                </div>
                <div className="legal-input-wrapper">
                    <div className="form-item">
                        <label htmlFor="formname">Назва форми розрахунку</label>
                        <input
                            type="text"
                            id="formname"
                            name="name"
                            onChange={this.handleChange}
                        />
                    </div>
                    <div className="form-item">
                        <label htmlFor="companyType">
                            Організаційно правова форма
                        </label>
                        <input
                            type="text"
                            id="companyType"
                            name="companyType"
                            onChange={this.handleChange}
                        />
                    </div>
                    <div className="form-item">
                        <label htmlFor="organization">Назва Організація</label>
                        <input
                            type="text"
                            id="organization"
                            name="organization"
                            onChange={this.handleChange}
                        />
                    </div>
                    <div className="form-item">
                        <label htmlFor="head_name">ПІБ</label>
                        <input
                            type="text"
                            id="head_name"
                            name="headName"
                            onChange={this.handleChange}
                        />
                    </div>
                    <div className="form-item">
                        <label htmlFor="head_type">Посада керівника</label>
                        <input
                            type="text"
                            id="head_type"
                            name="headType"
                            onChange={this.handleChange}
                        />
                    </div>
                    <div className="form-item">
                        <label htmlFor="bankName">Назва банку</label>
                        <input
                            type="text"
                            id="bankName"
                            name="bankName"
                            onChange={this.handleChange}
                        />
                    </div>
                    <div className="form-item">
                        <label htmlFor="settlementAccount">
                            Розрахунковий рахунок
                        </label>
                        <input
                            type="text"
                            id="settlementAccount"
                            name="settlementAccount"
                            onChange={this.handleChange}
                        />
                    </div>
                    <div className="form-item">
                        <label htmlFor="usreou">ЄДЕРПОУ</label>
                        <input
                            type="text"
                            id="usreou"
                            name="usreou"
                            onChange={this.handleChange}
                        />
                    </div>
                    <div className="form-item">
                        <label htmlFor="sort_code">МФО</label>
                        <input
                            type="text"
                            id="sort_code"
                            name="sortCode"
                            onChange={this.handleChange}
                        />
                    </div>
                    <div className="form-item">
                        <label htmlFor="vat">Посвідчення платника ПДВ</label>
                        <input
                            type="text"
                            id="vat"
                            name="vat"
                            onChange={this.handleChange}
                        />
                    </div>
                </div>
            </div>
        );
    }
}
