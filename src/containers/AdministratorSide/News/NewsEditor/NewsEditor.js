import React, { Component } from 'react';
import api from '../../../../sevices/adminSide/news';
import CKEditor from '@ckeditor/ckeditor5-react';
import ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import { notification } from 'antd';
// import Link from "@ckeditor/ckeditor5-link/src/link";

import './newseditor.scss';

class NewsEditor extends Component {
    state = {
        newsTitle: '',
        // news: "",
        newsDescription: '',
    };
    componentDidMount() {
        const { id } = this.props.match.params;

        if (id) {
            this.fetchCurrentNew(+id);
        }
        // if (this.props.location.state) {
        //   console.log(this.props);
        //   this.setState({
        //     newsTitle: this.props.location.state.newsItem.title,
        //     newsDescription: this.props.location.state.newsItem.text
        //   });
        // }
    }
    fetchCurrentNew = async id => {
        try {
            const { data } = await api.getOneNew(id);
            this.setState({
                newsTitle: data.title,
                newsDescription: data.text,
            });
        } catch (e) {
            console.log(e);
        }
    };
    handleSaveNews = async editorBody => {
        const { history } = this.props;
        const { newsTitle } = this.state;
        if (!editorBody.length || !newsTitle.length) {
            notification.error({
                message: "Заповніть всі поля"
            })
            return;
        }
        try {
            const newsData = {
                title: newsTitle,
                text: editorBody,
            };
            await api.createNews(newsData);
            notification.success({
                message: 'Новина створена!',
            });
            history.push('/admin/news');
        } catch (e) {
            notification.error({
                message: 'Помилка сервера',
            });
            console.log(e);
        }
    };
    handleEditNews = async editorBody => {
        const { history } = this.props;
        const { newsTitle } = this.state;
        const { id } = this.props.match.params;
        if (!editorBody || !editorBody.length || !newsTitle.length) {
            notification.error({
                message: "Заповніть всі поля"
            })
            return;
        }
        try {
            const newsData = {
                title: newsTitle,
                text: editorBody,
            };
            await api.editNews(id, newsData);
            notification.success({
                message: 'Новина оновлена!',
            });
            history.push('/admin/news');
        } catch (e) {
            notification.error({
                message: 'Помилка сервера',
            });
            console.log(e);
        }
    };

    render() {
        const { newsTitle, newsDescription } = this.state;
        let editorBody = newsDescription || "";
        return (
            <div className="news-editor-page">
                <div className="new-title section">
                    <div className="form-item">
                        <h2>Заголовок</h2>
                        <input
                            type="text"
                            value={newsTitle}
                            onChange={e =>
                                this.setState({ newsTitle: e.target.value })
                            }
                        />
                    </div>
                    {!this.props.match.params.id ? (
                        <button
                            className="btn"
                            onClick={() => this.handleSaveNews(editorBody)}
                        >
                            Створити
                        </button>
                    ) : (
                        <button
                            className="btn"
                            onClick={() => this.handleEditNews(editorBody)}
                        >
                            Зберегти
                        </button>
                    )}
                </div>

                <div className="section editor">
                    <CKEditor
                        editor={ClassicEditor}
                        config={{
                            toolbar: [
                                'Heading',
                                '|',
                                'Bold',
                                'Italic',
                                'Alignment',
                                'Link',
                                'BlockQuote',
                                'Undo',
                                'Redo',
                            ],
                            removePlugins: [
                                'Image',
                                'ImageCaption',
                                'ImageStyle',
                                'ImageToolbar',
                                'ImageUpload',
                            ],
                        }}
                        data={newsDescription}
                        onInit={editor => {
                        }}
                        onChange={(event, editor) => {
                            const data = editor.getData();
                            editorBody = data;
                        }}
                    />
                </div>
            </div>
        );
    }
}

export default NewsEditor;
