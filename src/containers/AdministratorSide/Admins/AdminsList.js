import React from 'react';
import { Table } from 'antd';
import columns from '../../../helpers/columns';

const AdminsList = ({ list, onRemove, onEditAdmin, update }) => {
    return (
        <div className="admin-table section">
            <div className="title-table">
                Список адміністраторів
                <button
                    className="btn"
                    onClick={() => onEditAdmin('admins-create', null)}
                >
                    Додати
                </button>
            </div>

            <Table
                columns={columns.adminsColumns(onEditAdmin, onRemove, update)}
                dataSource={list}
                rowKey={record => record.id}
                className="admin-table"
                pagination={false}
            />
        </div>
    );
};

export default AdminsList;
