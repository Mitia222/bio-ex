import React from 'react';
import { useValidate } from '../../../Hooks/UseValidate';

const AdminsForm = ({ type, data, handleCloseModal }) => {
    const initialState =
        type === 'admins-create'
            ? {
                  email: '',
                  password: '',
                  confirmPassword: '',
                  phone: '',
                  userCountry: '',
              }
            : {
                  email: data.email || '',
                  userCountry: data.userCountry || '',
                  phone: data.phone || '',
              };
    const { state, handleChange, handleCreateAdmin } = useValidate(
        initialState,
        type,
        handleCloseModal,
        data,
    );
    return (
        <div className="add-admin">
            <div className="window-title">Адміністратор</div>
            <div className="form-item">
                <label htmlFor="">Email </label>
                <input
                    type="email"
                    name="email"
                    value={state.email}
                    onChange={handleChange}
                />
            </div>
            <div className="form-item">
                <label htmlFor=" ">Телефон </label>
                <input
                    type="phone"
                    name="phone"
                    value={state.phone}
                    onChange={handleChange}
                />
            </div>

            {type === 'admins-create' && (
                <div className="form-item">
                    <label htmlFor=" ">Пароль </label>
                    <input
                        type="password"
                        name="password"
                        value={state.password}
                        onChange={handleChange}
                    />
                </div>
            )}
            {type === 'admins-create' && (
                <div className="form-item">
                    <label htmlFor=" ">Підтвердіть пароль </label>
                    <input
                        type="password"
                        name="confirmPassword"
                        value={state.confirmPassword}
                        onChange={handleChange}
                    />
                </div>
            )}

            <div className="form-item">
                <label htmlFor=" ">Країна </label>
                <input
                    type="text"
                    name="userCountry"
                    value={state.userCountry}
                    onChange={handleChange}
                />
            </div>

            <button className="btn" onClick={handleCreateAdmin}>
                {type === 'admins-create' ? 'Створити' : 'Редагувати'}
            </button>
        </div>
    );
};

export default AdminsForm;
