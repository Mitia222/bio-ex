import React, { Component } from 'react';
import {  notification } from 'antd';
import { connect } from 'react-redux';
import { openModal } from '../../../store/actions/modal';
import api from '../../../sevices/adminSide/admins';
import './admins.scss';
import AdminsList from './AdminsList';

class Admins extends Component {
    state = {
        admins: [],

        count: 0,
    };
    componentDidMount() {
        this.getAdmins();
    }
    getAdmins = async () => {
        try {
            const { data } = await api.getAllAdmins();

            this.setState({
                admins: data.results,
                count: data.count,
            });
        } catch (e) {
            console.log(e);
        }
    };

    handleRemoveUser = async id => {
        try {
            await api.deleteAdmin(id);
            this.setState({
                ...this.state,
                admins: this.state.admins.filter(el => el.id !== id),
            });
            notification.success({
                message: 'Користувача видаленно!',
            });
        } catch (e) {
            console.log(e);
        }
    };
    render() {
        const { admins } = this.state;
        const { openModal } = this.props;
        return (
            <div className="admins-page">
                {/* <div className="super-admin section">
                    <span className="role">Superadmin</span>
                    <span className="email">11@gmail.com</span>
                    <span className="password">7h87b14b194b41</span>

                    <button className="btn btn-white">Редагувати</button>
                </div> */}

                <AdminsList
                    list={admins}
                    update={this.getAdmins}
                    onRemove={this.handleRemoveUser}
                    onEditAdmin={openModal}
                />
            </div>
        );
    }
}

const mapDispatchToProps = dispatch => ({
    openModal: (type, i, id) => dispatch(openModal(type, i, id)),
});

export default connect(
    null,
    mapDispatchToProps,
)(Admins);
