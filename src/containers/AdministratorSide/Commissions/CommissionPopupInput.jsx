import React, { useState } from 'react';
import { connect } from 'react-redux';
import { updatecommission } from '../../../store/actions/Admin/commissions';

const CommissionPopupInput = ({ commission, updatecommission }) => {
    const [inputValue, setCommission] = useState(0);
    const sendNewCommission = () => {
        updatecommission({ amount: inputValue });
    };
    return (
        <div className="popup-commission">
            <h2>Комісія біржи {commission}%</h2>
            <h1>Змінити комісію</h1>
            <input
                type="number"
                value={inputValue}
                onChange={e => setCommission(e.target.value)}
                placeholder="Коммісія, %"
            />
            <button
                className="btn commision-btn"
                type="button"
                onClick={sendNewCommission}
            >
                Змінити
            </button>
        </div>
    );
};

const mapStateToProps = state => ({ commission: state.commission.amount });
const mapDispatchToProps = dispatch => ({
    updatecommission: data => dispatch(updatecommission(data)),
});

export default connect(
    mapStateToProps,
    mapDispatchToProps,
)(CommissionPopupInput);
