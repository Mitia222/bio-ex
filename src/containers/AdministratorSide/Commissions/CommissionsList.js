import React from 'react';
import { Table } from 'antd';
import columns from '../../../helpers/columns';

const CommissionsList = ({ openSetting, data, count, onChangeFilters }) => {
    return (
        <div className="admin-table section">
            <div className="title-table">
                <p>Транзакції комісій</p>
                <button className="btn" onClick={openSetting}>
                    Налаштувати
                </button>
            </div>
            <Table
                pagination={{
                    // current: ,
                    pageSize: 10,
                    total: count,
                    onChange: val => onChangeFilters('page', val),
                }}
                scroll={{ x: 900 }}
                dataSource={data}
                columns={columns.adminCommissionColumns}
            />
        </div>
    );
};

export default CommissionsList;
