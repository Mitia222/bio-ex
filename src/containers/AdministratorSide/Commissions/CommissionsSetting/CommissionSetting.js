import React, {Component} from 'react';
import {Link} from 'react-router-dom';
import {Menu, Select, Icon, Input} from 'antd';

import './commissionsetting.scss';

const Option = Select.Option;

class CommissionSetting extends Component {
    state = {};

    onChangePair = (pair) => {

    }

    render() {
        const list = [
            {
                name: 'Товар1/товар2'
            }, {
                name: 'Товар1/товар2'
            }, {
                name: 'Товар1/товар2'
            }, {
                name: 'Товар1/товар2'
            }, {
                name: 'Товар1/товар2'
            }, {
                name: 'Товар1/товар2'
            }, {
                name: 'Товар1/товар2'
            }, {
                name: 'Товар1/товар2'
            }, {
                name: 'Товар1/товар2'
            }, {
                name: 'Товар1/товар2'
            }, {
                name: 'Товар1/товар2'
            },
        ];
        return (
            <div className='commissions-setting-page'>
                <div className='back-link'>
                    <Link to='/admin/commissions'>Комісії</Link>
                    <span>  > Налаштування</span>
                </div>

                <div className='wrap'>
                    <div className='pair-list'>
                        <div className='title'>Оберіть пару</div>

                        <div className='list'>
                            <Menu
                                defaultSelectedKeys={['0']}
                                mode="inline"
                            >
                                {list.map((item, index) => (
                                    <Menu.Item
                                        key={index}
                                        onClick={() => this.onChangePair(item)}
                                    >
                                        <span>{item.name}</span>
                                    </Menu.Item>
                                ))}
                            </Menu>
                        </div>
                    </div>

                    <div className="pair-fee">
                        <div className='title'>Товар 1/товар 2</div>

                        <div className='label-row'>
                            <label htmlFor="">Тип</label>
                            <label htmlFor="">Кроки</label>
                            <label htmlFor="">Відсоток комісії</label>
                        </div>

                        {[0, 1, 2].map(i => (
                            <div className='fee-item'>
                                <div className="form-item type">
                                    <label htmlFor="">Тип</label>
                                    <Select>
                                        <Option value='1'>Купівля</Option>
                                    </Select>
                                </div>

                                <div className="steps">
                                    <div className="form-item from">
                                        <label htmlFor="">Від</label>
                                        <input type="number"/>
                                    </div>

                                    <div className='hyphen'></div>

                                    <div className="form-item to">
                                        <label htmlFor="">До</label>
                                        <input type="number"/>
                                    </div>
                                </div>

                                <Icon type="arrow-right" />
                                
                                <div className='form-item fee'>
                                    <label htmlFor="">Комісія</label>

                                    <Input
                                        suffix='%'
                                        type='number'
                                    />
                                </div>
                            </div>
                        ))}
                    </div>
                </div>
            </div>
        )
    }
}

export default CommissionSetting;