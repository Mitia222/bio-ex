import React  from 'react';
import './commissions.scss';
import { openModal } from '../../../store/actions/modal';

import { onChangeFilters } from '../../../store/actions/Admin/filters';
import { connect } from 'react-redux';
import Filters from '../components/Filters/Filters';
import CommissionsList from './CommissionsList';

const Commissions = ({
    openModal,
    count,
    commissions,
    location,
    onChangeFilters,
}) => {
    const getSettingPage = () => {
        openModal('commission-input');

    };

    return (
        <div className="commissions-page">
            <Filters page="commissions" location={location} />

            <CommissionsList
                onChangeFilters={onChangeFilters}
                count={count}
                openSetting={getSettingPage}
                data={commissions}
            />
        </div>
    );
};

const mapStateToProps = state => ({
    count: state.adminCommissions.count,
    commissions: state.adminCommissions.data,
});

const mapDispatchToProps = dispatch => ({
    openModal: type => dispatch(openModal(type)),
    onChangeFilters: (field, val) => dispatch(onChangeFilters(field, val)),
});

export default connect(
    mapStateToProps,
    mapDispatchToProps,
)(Commissions);
