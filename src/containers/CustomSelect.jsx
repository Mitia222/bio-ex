import React from 'react';
import { Select, Tooltip } from 'antd';

const { Option } = Select;

const CustomSelect = ({
    handleChange,
    value,
    options,
    disabled,
    label,
    placeholder,
    allowClear,
    tooltipTitle = '',
    defaultValue,
    errors,
}) => {
    const renderSelect = () => (
        <div className="form-item">
            <label htmlFor="">{label}</label>
            <Select
                allowClear={allowClear}
                value={value}
                placeholder={placeholder}
                onChange={handleChange}
                disabled={disabled}
                defaultValue={defaultValue}
                className={errors ? "error-class" : ""}
            >
                {options
                    ? options.map(({ id, name, disabled = false }) => (
                          <Option
                              key={id || name}
                              value={id || name}
                              disabled={disabled}
                          >
                              {name}
                          </Option>
                      ))
                    : []}
            </Select>
            {errors && <div className="inputs-error-text">{errors}</div>}
        </div>
    );

    return !disabled ? (
        renderSelect()
    ) : (
        <Tooltip title={tooltipTitle}>{renderSelect()}</Tooltip>
    );
};

export default CustomSelect;
