import * as Yup from 'yup';

const phoneMatch = /^(\+\d{2})-?(\d{2,4})-?(\d{3,4})-?(\d{2,3})-?(\d{2,3})$/;

export const LoginSchema = Yup.object().shape({
    password: Yup.string()
        .min(3, 'Мінімум 3 символи.')
        .required('Введіть пароль.'),

    email: Yup.string()
        .email('Невірний формат email.')
        .required('Введіть email.'),
    captch: Yup.bool().oneOf([true], 'Заповніть captch`у!'),
});

export const ResetPasswordSchema = Yup.object().shape({
    // captch: Yup.bool().oneOf([true], 'Заповніть captch`у!'),
    email: Yup.string()
        .email('Невірний формат email.')
        .required('Введіть email.'),
});

export const RegistrationSchema = Yup.object().shape({
    username: Yup.string()
        .max(20, 'Максимум 20 символів.')
        .min(3, 'Мінімум 3 символи.')
        .required('Заповніть поле з вашим ім`ям!'),

    phone: Yup.string()
        .matches(phoneMatch, 'Невірний телефонний номер!')
        .required('Заповніть поле з вашим телефоним номером!'),
    password: Yup.string()
        .min(8, 'Мінімум 8 символи.')
        .required('Заповніть поле з вашим паролем!'),
    repeatPassword: Yup.string()
        .min(8, 'Мінімум 8 символів.')
        .required('Заповніть поле з вашим паролем!'),
    email: Yup.string()
        .email('Невірний формат email адреси!')
        .required('Заповныть поле email!'),
    permission: Yup.bool().oneOf([true], 'Угода повинна бути прийнята'),
    captch: Yup.bool().oneOf([true], 'Заповніть captch`у!'),
});

export const ChangePasswordSchema = Yup.object().shape({
    oldPassword: Yup.string()
        .min(8, 'Мінімум 8 символи.')
        .required('Заповніть всі поля!'),
    newPassword: Yup.string()
        .min(8, 'Мінімум 8 символи.')
        .required('Заповніть всі поля!'),
    confirmPassword: Yup.string()
        .min(8, 'Мінімум 8 символи.')
        .required('Заповніть всі поля!'),
});
export const updateprofileSchema = Yup.object().shape({
    phone: Yup.string()
        .matches(phoneMatch, 'Невірний формат телефонного номера!')
        .required('Заповніть поле з вашим телефоним номером!'),
    organization: Yup.object({
        name: Yup.string()
            .min(2, 'Назва команії має бути не меньше 2 символів')
            .max(25, 'Назва команії має бути не більше 25 символів')
            .transform((value) => (!value ? null : value))
            .nullable(true),
        pdv: Yup.string()
            .matches(/^\d{12}$/, 'ПДВ: має мати 12 цифр в номері!')
            .transform((value) => (!value ? null : value))
            .nullable(true),
        headPosition: Yup.string()
            .min(2, 'ПІБ: Не меньше 2-х символів')
            .max(50, 'ПІБ: Не більше 50-и символів')
            .transform((value) => (!value ? null : value))
            .nullable(true),
        organizationCountry: Yup.string()
            .min(2, 'Локація організації: Не меньше 2-х символів')
            .max(30, 'Локація організації: Не більше 30-и символів')
            .transform((value) => (!value ? null : value))
            .nullable(true),
        organizationRegion: Yup.string()
            .min(2, 'Район: Не меньше 2-х символів')
            .max(20, 'Район: Не більше 30-и символів')
            .transform((value) => (!value ? null : value))
            .nullable(true),
        organizationStreet: Yup.string()
            .transform((value) => (!value ? null : value))
            .min(2, 'Вулиця: Не меньше 2-х символів')
            .max(20, 'Вулиця: Не більше 30-и символів')
            .nullable(true),
        organizationIndex: Yup.string()
            .transform((value) => (!value ? null : value))
            .matches(/^\d+$/, 'Індекс: Невірний формат!')
            .nullable(true),
    }).nullable(true),
    account: Yup.object({
        bank: Yup.string()
            .min(2, 'Назва банку: Не меньше 2-х символів')
            .max(30, 'Назва банку: Не більше 30-и символів')
            .transform((value) => (!value ? null : value))
            .nullable(true)
            .required('Назва банку: Заповніть це поле!'),
        sortCode: Yup.string()
            .matches(/^\d{6}$/, 'МФО: Має бути 6 цифр в номері!')
            .transform((value) => (!value ? null : value))
            .nullable(true)
            .required('МФО: Заповніть це поле!'),
        // currentAccount: Yup.string()
        //     .transform((value) => (!value ? null : value))
        //     .nullable(true),
    }).nullable(true),
    nin: Yup.string()
        .matches(/^\d{10}$/, 'ІН: Має бути 10 цифр в номері!')
        .nullable(true),
    email: Yup.string()
        .email('Невірний формат email адреси!')
        .required('Заповніть E-mail'),
    lastName: Yup.string()
        .min(3, 'Мінімум 3 символи!')
        .max(20, 'Максимум 20 символів')
        .nullable(true),
    firstName: Yup.string()
        .min(3, 'Мінімум 3 символи!')
        .max(20, 'Максимум 20 символів')
        .nullable(true),
});
export const warehouseSchema = Yup.object().shape({
    stockCity: Yup.string()
        .min(3, 'Мінімум 3 символи!')
        .max(20, 'Максимум 20 символів')
        .required('Обов`язкове поле!'),
    stockRegion: Yup.string()
        .min(3, 'Мінімум 3 символи!')
        .max(20, 'Максимум 20 символів')
        .required('Обов`язкове поле!'),
    stockCountry: Yup.string()
        .min(3, 'Мінімум 3 символи!')
        .max(20, 'Максимум 20 символів')
        .required('Обов`язкове поле!'),
    stockStreet: Yup.string()
        .min(3, 'Мінімум 3 символи!')
        .max(20, 'Максимум 20 символів')
        .required('Обов`язкове поле!'),
    stockBuilding: Yup.string()

        .max(10, 'Максимум 10 символів')
        .required('Обов`язкове поле!'),
});
