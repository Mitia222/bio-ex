import { lazy } from 'react';

const UserNews = lazy(() => import('../containers/UserSide/News/News'));
const Confidantial = lazy(() => import('../containers/Confidantial'));
const HomePage = lazy(() => import('../containers/HomePage'));
const RegistrationPage = lazy(() =>
    import('../containers/Registration/RegistrationPage'),
);
const LoginPage = lazy(() => import('../containers/Login/LoginPage'));
const ResetPasswordPage = lazy(() => import('../containers/ResetPasswordPage'));
const ChangePasswordPage = lazy(() =>
    import('../containers/ChangePasswordPage'),
);
const Exchange = lazy(() => import('../containers/UserSide/Exchange/Exchange'));
const Profile = lazy(() => import('../containers/UserSide/Profile/Profile'));
const Contracts = lazy(() =>
    import('../containers/UserSide/Contracts/Contracts'),
);
const Balance = lazy(() => import('../containers/UserSide/Balance/Balance'));
// const Orders = lazy(() => import('../containers/UserSide/Orders/Orders'));
const Accounts = lazy(() => import('../containers/UserSide/Accounts/Accounts'));
const UsingRights = lazy(() => import('../containers/UsingRights'));

//Admin
const Dashboard = lazy(() =>
    import('../containers/AdministratorSide/Dashboard/Dashboard'),
);
const Users = lazy(() => import('../containers/AdministratorSide/Users/Users'));
const User = lazy(() =>
    import('../containers/AdministratorSide/Users/User/User'),
);
const AdminAccounts = lazy(() =>
    import('../containers/AdministratorSide/Accounts/Accounts'),
);
const AdminContracts = lazy(() =>
    import('../containers/AdministratorSide/Contracts/Contracts'),
);
const AdminContract = lazy(() =>
    import('../containers/AdministratorSide/Contracts/Contract/Contract'),
);
const Commissions = lazy(() =>
    import('../containers/AdministratorSide/Commissions/Commissions'),
);
const CommissionSetting = lazy(() =>
    import(
        '../containers/AdministratorSide/Commissions/CommissionsSetting/CommissionSetting'
    ),
);
const News = lazy(() => import('../containers/AdministratorSide/News/News'));
const Settings = lazy(() =>
    import('../containers/AdministratorSide/Settings/Settings'),
);
const Admins = lazy(() =>
    import('../containers/AdministratorSide/Admins/Admins'),
);
const LegalData = lazy(() =>
    import('../containers/AdministratorSide/LegalData/LegalData'),
);
const NewsEditor = lazy(() =>
    import('../containers/AdministratorSide/News/NewsEditor/NewsEditor'),
);
const LegalCreate = lazy(() =>
    import('../containers/AdministratorSide/LegalData/LegalCreate'),
);
const LegalEdit = lazy(() =>
    import('../containers/AdministratorSide/LegalData/LegalEdit'),
);
const ErrorPage = lazy(() => import('../containers/ErrorPage'));

export const userRoutes = [
    {
        path: '/',
        exact: true,
        component: HomePage,
    },
    {
        path: '/news',
        exact: true,
        component: UserNews,
    },
    {
        path: '/support',
        exact: true,
        component: Confidantial,
    },

    {
        path: '/UsingRights',
        exact: true,
        component: UsingRights,
    },
    {
        path: '/registration',
        exact: true,
        component: RegistrationPage,
    },
    {
        path: '/login',
        exact: false,
        component: LoginPage,
    },
    {
        path: '/reset_password',
        exact: true,
        component: ResetPasswordPage,
    },
    {
        path: '/profile',
        exact: true,
        component: Profile,
    },
    {
        path: '/exchange',
        exact: true,
        component: Exchange,
    },
    {
        path: '/contracts',
        exact: true,
        component: Contracts,
    },
    {
        path: '/balance',
        exact: true,
        component: Balance,
    },
    // {
    //     path: '/orders',
    //     exact: true,
    //     component: Orders,
    // },
    {
        path: '/change_password',
        exact: true,
        component: ChangePasswordPage,
    },
    {
        path: '/accounts',
        exact: true,
        component: Accounts,
    },
    {
        path: '',
        exact: false,
        component: ErrorPage,
    },
];
export const adminRoutes = [
    {
        path: '/admin/dashboard',
        exact: true,
        component: Dashboard,
    },
    {
        path: '/admin/users',
        exact: true,
        component: Users,
    },
    {
        path: '/admin/users/:id',
        exact: true,
        component: User,
    },
    {
        path: '/admin/accounts',
        exact: true,
        component: AdminAccounts,
    },
    {
        path: '/admin/contracts',
        exact: true,
        component: AdminContracts,
    },
    {
        path: '/admin/contracts/:id',
        exact: true,
        component: AdminContract,
    },
    {
        path: '/admin/commissions',
        exact: true,
        component: Commissions,
    },
    {
        path: '/admin/commissions/setting',
        exact: true,
        component: CommissionSetting,
    },
    {
        path: '/admin/news',
        exact: true,
        component: News,
    },
    {
        path: '/admin/create-new/',
        exact: true,
        component: NewsEditor,
    },
    {
        path: '/admin/create-new/:id/',
        exact: true,
        component: NewsEditor,
    },
    {
        path: '/admin/settings',
        exact: true,
        component: Settings,
    },
    {
        path: '/admin/access_rights',
        exact: true,
        component: Admins,
    },
    {
        path: '/admin/legal_data',
        exact: true,
        component: LegalData,
    },
    {
        path: '/admin/legal_data/new',
        exact: true,
        component: LegalCreate,
    },
    {
        path: '/admin/legal_data/edit/:id',
        exact: true,
        component: LegalEdit,
    },
    {
        path: '',
        exact: false,
        component: ErrorPage,
    },
];
