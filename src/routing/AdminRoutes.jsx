import React, { Fragment } from 'react';
import { Route, Redirect } from 'react-router-dom';
import AdministratorSide from '../containers/AdministratorSide/AdministratorSide';
import Dashboard from '../containers/AdministratorSide/Dashboard/Dashboard';
import Users from '../containers/AdministratorSide/Users/Users';
import User from '../containers/AdministratorSide/Users/User/User';
import AdminAccounts from '../containers/AdministratorSide/Accounts/Accounts';
import AdminContracts from '../containers/AdministratorSide/Contracts/Contracts';
import AdminContract from '../containers/AdministratorSide/Contracts/Contract/Contract';
import Commissions from '../containers/AdministratorSide/Commissions/Commissions';
import CommissionSetting from '../containers/AdministratorSide/Commissions/CommissionsSetting/CommissionSetting';
import News from '../containers/AdministratorSide/News/News';
import Settings from '../containers/AdministratorSide/Settings/Settings';
import Admins from '../containers/AdministratorSide/Admins/Admins';
import LegalData from '../containers/AdministratorSide/LegalData/LegalData';
import NewsEditor from '../containers/AdministratorSide/News/NewsEditor/NewsEditor';
import LegalCreate from '../containers/AdministratorSide/LegalData/LegalCreate';
import LegalEdit from '../containers/AdministratorSide/LegalData/LegalEdit';

const AdminRoutes = props => {
    return (
        <AdministratorSide {...props}>
            {localStorage.getItem('ADMINTOKEN') ? (
                <Fragment>
                    <Route path="/admin/dashboard" component={Dashboard} />
                    <Route exact path="/admin/users" component={Users} />
                    <Route path="/admin/users/:id" component={User} />
                    <Route path="/admin/accounts" component={AdminAccounts} />
                    <Route
                        exact
                        path="/admin/contracts"
                        component={AdminContracts}
                    />
                    <Route
                        path="/admin/contracts/:id"
                        component={AdminContract}
                    />
                    <Route
                        exact
                        path="/admin/commissions"
                        component={Commissions}
                    />
                    <Route
                        path="/admin/commissions/setting"
                        component={CommissionSetting}
                    />
                    <Route path="/admin/news" component={News} />
                    <Route
                        exact
                        path="/admin/create-new/"
                        component={NewsEditor}
                    />
                    <Route
                        exact
                        path="/admin/create-new/:id/"
                        component={NewsEditor}
                    />
                    <Route path="/admin/settings" component={Settings} />
                    <Route path="/admin/access_rights" component={Admins} />
                    <Route
                        exact
                        path="/admin/legal_data"
                        component={LegalData}
                    />
                    <Route
                        exact
                        path="/admin/legal_data/new"
                        component={LegalCreate}
                    />
                    <Route
                        exact
                        path="/admin/legal_data/edit/:id"
                        component={LegalEdit}
                    />
                </Fragment>
            ) : (
                <Redirect to="/" />
            )}
        </AdministratorSide>
    );
};
export default AdminRoutes;
