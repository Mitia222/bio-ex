import request from '../API';
import { CONTRACTS, CONTRACTS_STATISTIC } from '../../constants/APIURLS';

export default {
    getContracts: query => request('get', CONTRACTS + query),
    getContractsStatistic: () => request('get', CONTRACTS_STATISTIC),
    getInitialContract: id => request('get', CONTRACTS + `${id}/`),
    updateContract: (id, data) => request('patch', CONTRACTS + `${id}/`, data),
};
