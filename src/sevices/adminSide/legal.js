// import request from "./API";
import request from '../API';
import { LEGALS } from '../../constants/APIURLS';

export default {
    getLegals: (page = 1) => request('get', LEGALS + `?page=${page}`),
    editLegal: (data, id) => request('patch', LEGALS + `${id}/`, data),
    createLegal: data => request('post', LEGALS, data),
    getOneLegal: id => request('get', LEGALS + `${id}/`),
    deleteLegal: id => request('delete', LEGALS + `${id}/`),
};
