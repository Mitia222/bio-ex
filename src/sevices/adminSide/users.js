// import request from "./API";
import request from '../API';
import {
    ALL_USERS,
    UPDATE_USER_BALANCE,
    CHANGE_USER_STATUS,
} from '../../constants/APIURLS';

export default {
    getUsers: query =>
        request('get', !query ? ALL_USERS : ALL_USERS + `${query}`),
    getUserById: id => request('get', ALL_USERS + `${id}/`),
    updateUserProfile: (id, data) =>
        request('patch', ALL_USERS + `${id}/`, data),
    updateBalance: (id, data) =>
        request('patch', UPDATE_USER_BALANCE + `${id}/`, data),
    // getByFilter: query => request('get', ALL_USERS + `?${query}`),
    updateUserStatus: (id, data) =>
        request('patch', CHANGE_USER_STATUS + `${id}/`, data),
};
