import request from '../API';
import { UPDATE_STATUS,  BILLSLIST, BILL_STATISTIC} from '../../constants/APIURLS';

export default {

    getAllAcountsCount: () => request('get', BILL_STATISTIC),
    getAllAccountItems: (query) => request('get', BILLSLIST + query),
    updateStatus: (id, data) => request('patch', UPDATE_STATUS + `${id}/`, data)
    // getByFilter: (query) => request("get", ALL_USERS + `?${query}`)
};
