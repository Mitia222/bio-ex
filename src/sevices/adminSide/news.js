// import request from "./API";
import request from '../API';
import { ALL_NEWS } from '../../constants/APIURLS';

export default {
    getAllNews: (page = 1) => request('get', ALL_NEWS + `?page=${page}`),
    createNews: data => request('post', ALL_NEWS, data),
    deleteNews: id => request('delete', ALL_NEWS + id),
    getOneNew: id => request('get', ALL_NEWS + `${id}/`),
    editNews: (id, data) => request('patch', ALL_NEWS + `${id}/`, data),
    getNews: () => request('get', 'news/'),
};
