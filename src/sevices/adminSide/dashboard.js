import request from '../API';
import { DASHBOARD_STATISCTIC } from '../../constants/APIURLS';

export default {
    getStatistic: () => request('get', DASHBOARD_STATISCTIC),

    // getByFilter: (query) => request("get", ALL_USERS + `?${query}`)
};
