import request from '../API';
import {
    MAIN_REGISTRATION_STATUS,
    UPDATE_MAIN_REGISTER,
    GET_MIN_MAX_FILTERS_VALUES,
} from '../../constants/APIURLS';

export default {
    getRegistrationStatus: () => request('get', MAIN_REGISTRATION_STATUS),
    updateRegister: data => request('patch', UPDATE_MAIN_REGISTER, data),
    getMinMaxContractsFilters: () => request('get', GET_MIN_MAX_FILTERS_VALUES),
};
