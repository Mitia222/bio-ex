import request from './API';

import {
    LOGIN,
    USER_LOGIN_HISTORY,
    REGISTRATION,
    PASSWORD_CHANGE,
    PASSWORD_RESET,
    USER_PROFILE,
    USER_TWO_FACTOR,
    USER_TWO_FACTOR_VALIDATE,
    USER_PROFILE_TWO_FACTOR,
    GET_USER_BALANCE,
    REFRESH_TOKEN,
} from '../constants/APIURLS';

export default {
    refreshToken: token => request('post', REFRESH_TOKEN, token),
    login: data => request('post', LOGIN, data),
    Registration: data => request('post', REGISTRATION, data),
    ChangePassword: data => request('put', PASSWORD_CHANGE, data),
    ResetPassword: data => request('patch', PASSWORD_RESET, data),
    GetUserProfile: data => request('get', USER_PROFILE, data),
    userLoginHistory: page => request('get', USER_LOGIN_HISTORY, page),
    editProfileInfo: values => request('patch', USER_PROFILE, values),
    checkForTwoFactor: data => request('post', USER_TWO_FACTOR, data),
    getUserBalance: () => request('get', GET_USER_BALANCE),
    validateQrCode: number => request('post', USER_TWO_FACTOR_VALIDATE, number),
    updateTwoFactor: data => request('patch', USER_PROFILE_TWO_FACTOR, data),
};
