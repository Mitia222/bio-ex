export async function fileDownloader(data) {
    let link = document.createElement('a');
    link.href = 'data:application/octet-stream;base64,' + data;
    link.download = 'invoice.pdf';
    link.click();
}
