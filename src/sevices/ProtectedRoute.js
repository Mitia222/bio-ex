import React, { useEffect } from 'react';
import {useSelector} from 'react-redux';
import { Route, Redirect } from 'react-router-dom';
// import Preloader from '../components/Preloader';
// import { connect } from 'react-redux';
// import { fetchCommission } from '../store/actions/Users/commission';

const ProtectedRoute = ({ children }) => {
    const isAuthenticated = useSelector(state => state.user.authorized);
    useEffect(() => {
        // getCommission();
        // if (localStorage.getItem('TOKEN')) {
        //     getUserProfile();
        // }
    }, []);
    return isAuthenticated ? children : <Redirect to='/login'/>;
};

export default ProtectedRoute;
