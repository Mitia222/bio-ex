import axios from 'axios';
import { checkExpiredToken } from '../helpers/functions';
import { BASE_URL  } from '../constants/APIURLS';

const http = async (method, url, data,params) => {
    let token = await checkExpiredToken();

    return axios({
        method: method,
        url: BASE_URL + url,
        params,
        data: JSON.stringify(data),
        headers: {
            'Content-Type': 'application/json',
            Authorization: token ? `Bearer ${token}` : null,
        },
    });
};

export default http;
