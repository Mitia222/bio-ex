import React, { useEffect } from 'react';
import { Route, Redirect } from 'react-router-dom';
// import Preloader from '../components/Preloader';
import { connect } from 'react-redux';
import { fetchCommission } from '../store/actions/Users/commission';

const WaitingRoute = ({
    auth,
    component: Component,
    getCommission,
    ...rest
}) => {
    useEffect(() => {
        getCommission();
        // if (localStorage.getItem('TOKEN')) {
        //     getUserProfile();
        // }
    }, []);
    return localStorage.getItem('TOKEN') ? (
        <Route {...rest} component={Component} />
    ) : (
        <Redirect to="/" />
    );
};
const mapStateToProps = state => {
    return {
        auth: state.user.authorized,
    };
};
const mapDispatchToProps = dispatch => ({
    getCommission: () => dispatch(fetchCommission()),
});
export default connect(
    mapStateToProps,
    mapDispatchToProps,
)(WaitingRoute);
