// import request from "./API";
import axios from "axios";

export default {
  getAllCountries: () =>
    axios({
      method: "get",
      url: "https://restcountries-v1.p.rapidapi.com/all",

      headers: {
        "x-rapidapi-host": "restcountries-v1.p.rapidapi.com",
        "x-rapidapi-key": "a5109e33f8msh11750dbea0295e6p1b3372jsn8513409b9498"
      }
    })
};
