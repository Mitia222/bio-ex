import _ from 'lodash';
import axios from 'axios';
import { BASE_URL, REFRESH_TOKEN } from '../constants/APIURLS';
import jwtDecode from 'jwt-decode';

export const removeNulls = (obj) => {
    const newOBj = { ...obj };
    Object.keys(newOBj).forEach((key) => {
        if (newOBj[key] === null) {
            delete newOBj[key];
        } else if (_.isObject(newOBj[key])) {
            Object.keys(newOBj[key]).forEach((key2) => {
                if (newOBj[key][key2] === null) {
                    delete newOBj[key][key2];
                }
            });
        }
    });
    return newOBj;
};
export const productsRestructure = (arr) => {
    return arr.map((el1) => {
        return {
            description: el1.description,
            id: el1.id,
            name: el1.name,
            children: el1.rawMaterials.map((el2) => {
                return {
                    description: el2.description,
                    id: el2.id,
                    name: el2.name,
                    children: el2.categories.map((el) => ({
                        description: el.description,
                        id: el.id,
                        name: el.name,
                        children: el.categories,
                    })),
                };
            }),
        };
    });
};
export const sliceFalseValuesFromList = (data) => {
    return Object.entries(data).reduce((acc, el) => {
        if (el[1] || (el[1] === false && el)) {
            acc[el[0]] = el[1];
        }
        return acc;
    }, {});
};

export const transformIncomeRatingtoStateStracture = (rating) =>
    rating.reduce((acc, rat) => {
        acc[rat.id] = 0;
        return acc;
    }, {});

export const statusColors = {
    відкритий: '#2a2aec',
    активний: '#7ec365',
    закритий: '#c81414',
    скасований: '#c81414',
};

export const makeQueryFromState = (state) => {
    let res = {};
    Object.keys(state).forEach((el) => {
        if (typeof state[el] === 'string' || typeof state[el] === 'number') {
            res[el] = `${el}=${state[el]}`;
        } else if (
            state[el] !== null &&
            state[el] !== undefined &&
            Object.keys(state[el]).length > 0
        ) {
            const innerKeys = Object.keys(state[el]);
            res[el] = `${innerKeys[0]}=${state[el][innerKeys[0]]}&${
                innerKeys[1]
            }=${state[el][innerKeys[1]]}`;
        } else {
            res[el] = ``;
        }
    });

    return Object.values(res)
        .filter((el) => el)
        .join('&');
};

export const getCategories = (arr) => {
    let resa = [];
    const res = (arr) => {
        arr.forEach((el) => {
            el.children ? res(el.children) : resa.push(el);
        });
    };
    res(arr);
    return [...new Set(resa.map((s) => s.id))].map((id) => {
        const result = resa.find((el) => el.id === id);
        return {
            id,
            ...result,
        };
    });
};

export const checkExpiredToken = async () => {
    const tokenString = localStorage.getItem('ADMINTOKEN')
        ? 'ADMINTOKEN'
        : 'TOKEN';
    let token = localStorage.getItem(tokenString)
        ? JSON.parse(localStorage.getItem(tokenString))
        : null;
    if (token) {
        const parseToken = jwtDecode(token);
        const dateNow = Date.now();
        const difference = parseToken.exp * 1000 - dateNow;
        if (difference <= 0) {
            const refresh_token = localStorage.getItem('TOKENREFRESH');
            const { data } = await axios.post(`${BASE_URL}${REFRESH_TOKEN}`, {
                refresh: refresh_token,
            });
            localStorage.setItem(tokenString, data.access);
            localStorage.setItem('TOKENREFRESH', data.refresh);
            return data.access;
        }
    }

    return token;
};
export const getOrdersSum = (data) => {
    return data.reduce(
        (result, { sum, type }) => {
            type === 'Купівля'
                ? (result.buySum += +sum)
                : (result.sellSum += +sum);
            return result;
        },

        { buySum: 0, sellSum: 0 },
    );
};
export const transformWarehouseStateToQuery = (obj) => {
    return Object.keys(obj).reduce((res, field) => {
        if (field === 'stockCity' || field === 'stockCountry') {
            res += `+${obj[field]} `;
        } else {
            res += `${obj[field]}, `;
        }
        return res;
    }, '');
};

export const onDrop = (_files, _type, callback) => {
    const reader = new FileReader();
    reader.onloadend = function () {
        callback(reader.result, _files);
    };
    reader.readAsDataURL(_files[0]);
};
export const checkInActiveOrders = (oreders, id) =>
    oreders.some((o) => o.id === id);
export const flatToUniqueRwMterials = (arr) => {
    if (arr.length === 0) return arr;
    return arr
        .reduce((acc, item) => acc.concat(item.children) || acc, [])
        .reduce(
            (acc, item) =>
                !acc.find((el) => el.id === item.id)
                    ? acc.push(item) && acc
                    : acc,
            [],
        );
};
