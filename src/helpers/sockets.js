import ReconnectingWebSocket from 'reconnectingwebsocket';
// import { notification } from 'antd';
import { SOCKET_URL } from '../constants/APIURLS';
import { notification } from 'antd';

const handleSockets = (cb, errCb, userId) => {
    let attemps = 0;
    let ws = new ReconnectingWebSocket(SOCKET_URL + `?token=${userId}`);

    ws.onerror = err => {
        if (attemps > 4) {
            ws.close();
        }
        console.log(
            'Socket encountered error: ',
            err.message,
            'Closing socket',
        );
        notification.error({
            message: 'Помилка сервера. Спробуйте пізніше',
        });

        attemps += 1;
        errCb(err.message);
    };
    ws.onopen = () => {

        ws.send(
            JSON.stringify({
                command: 'join',
            }),
        );
    };
    ws.onmessage = message => {
        cb(message, ws);
    };
};

export default handleSockets;
