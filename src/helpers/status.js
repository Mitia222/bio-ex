export const statuses = [
    {
        id: 'O',
        name: 'Не підтверджений',
    },
    {
        id: 'A',
        name: 'Підтверджений',
    },
    {
        id: 'WP',
        name: 'Очікує оплати',
    },
    {
        id: 'WS',
        name: 'Очікує відвантаження',
    },
    {
        id: 'C',
        name: 'Виконаний',
    },
];
export const userStatusCodes = [
    {
        id: '1',
        name: 'Відкритий',
    },
    {
        id: '2',
        name: 'Активний',
    },
    {
        id: '3',
        name: 'Закритий',
    },
    {
        id: '4',
        name: 'Скасованиий',
    },

];
export const types = [
    {
        id: 'B',
        name: 'Купівля',
    },
    {
        id: 'S',
        name: 'Продаж',
    },
];
