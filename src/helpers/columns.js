import React from 'react';
import { Icon, Popover } from 'antd';
import star from '../assets/img/star.svg';
import moment from 'moment';
import { statusColors } from '../helpers/functions';

const iconStyle = {
    color: '#53be4f',
    fontSize: '15px',
};
const openStyle = {
    display: 'flex',
};
export default {
    userColumns: [
        {
            title: 'ID "Контрагента"',
            dataIndex: 'id',
            key: 'id',
        },
        {
            title: 'Ім’я',
            dataIndex: 'firstName',
            key: 'firstName',
        },
        {
            title: 'Email',
            dataIndex: 'email',
            key: 'email',
        },
        {
            title: 'Статус верифікації',
            dataIndex: 'verificationStatus',
            key: 'verificationStatus',
            render: (prop, data) => {
                return (
                    <div>
                        {data.verificationStatus === 'V'
                            ? 'Верифікований'
                            : 'Неверифікований'}
                    </div>
                );
            },
        },
        {
            title: 'Статус контрагента',
            dataIndex: 'isActive',
            key: 'isActive',
            render: (prop, data) => {
                return <div>{data.isActive ? 'Активний' : 'Неактивний'}</div>;
            },
        },
        {
            title: 'Дата створення',
            dataIndex: 'createdDate',
            key: 'createdDate',
        },
        {
            title: '',
            dataIndex: 'actions',
            key: 'actions',
            render: () => <Icon type="right" />,
        },
    ],
    userShortInfoCol: [
        {
            title: 'Дата',
            dataIndex: 'loginDatetime',
            key: 'date',
        },
        {
            title: 'IP адреса',
            dataIndex: 'loginIp',
            key: 'IP',
        },
        {
            title: '2-х фаторна',
            dataIndex: 'twoFactorAuthEnabled',
            key: 'twofactor',
        },
        {
            title: 'Локація',
            dataIndex: 'userCountry',
            key: 'location',
        },
    ],
    orderListColumns: (type, sortInfo) => {
        const contentCertificate = (
                <div style={{ border: '1px solid #53BE4F' }}>Є сертифікат</div>
            ),
            contentNotCertificate = (
                <div style={{ border: '1px solid #C10119' }}>
                    Сертифікат відсутній
                </div>
            );
        return [
            {
                title: 'Найменування',
                dataIndex: 'applicationData.product.name',
                key: 'name',
                width: 130,
                ellipsis: true,
                // sorter: true,
                sorter: (a, b) =>
                    a.applicationData.product.name !==
                    b.applicationData.product.name
                        ? a.applicationData.product.name <
                          b.applicationData.product.name
                            ? -1
                            : 1
                        : 0,

                // sortOrder: sortedInfo.columnKey === 'name' && sortedInfo.order,
                render: (name, item) =>
                    type !== 'buy' ? (
                        <span className="certificated">
                            {item && item.haveCertificate ? (
                                <Popover
                                    content={contentCertificate}
                                    title={null}
                                >
                                    <Icon
                                        type="safety-certificate"
                                        theme="filled"
                                    />
                                </Popover>
                            ) : (
                                <Popover
                                    content={contentNotCertificate}
                                    title={null}
                                >
                                    <Icon type="close-circle" theme="filled" />
                                </Popover>
                            )}
                            {name}
                        </span>
                    ) : (
                        <span className="certificated"> {name}</span>
                    ),
            },
            {
                title: 'Ціна ',
                dataIndex: 'price',
                key: 'price',
                width: 120,
                ellipsis: true,
                sorter: (a, b) =>
                    +a.price !== +b.price ? (+a.price < +b.price ? -1 : 1) : 0,
                render: (name, item) => item.price + 'грн',
            },
            {
                title: 'Обсяг',
                dataIndex: 'applicationData.purchaseVolume',
                key: 'amount',
                width: 100,
                ellipsis: true,
                sorter: (a, b) =>
                    a.applicationData.purchaseVolume !==
                    b.applicationData.purchaseVolume
                        ? a.applicationData.purchaseVolume <
                          b.applicationData.purchaseVolume
                            ? -1
                            : 1
                        : 0,
                render: (name, item) =>
                    item.applicationData.purchaseVolume +
                    item.applicationData.units.shortName,
            },
            {
                title: 'Відстань',
                dataIndex: 'distance',
                key: 'distance',
                width: 80,
                ellipsis: true,
                sorter: (a, b) =>
                    a.distance && b.distance
                        ? a.distance.value - b.distance.value
                        : null,

                render: (name, item) =>
                    !item.distance ? (
                        <Icon type="loading" style={iconStyle} />
                    ) : (
                        item.distance.text
                    ),
            },
            {
                title: 'Всього',
                dataIndex: 'sum',
                key: 'sum',
                width: 120,
                ellipsis: true,
                sorter: (a, b) =>
                    +a.sum !== +b.sum ? (+a.sum < +b.sum ? -1 : 1) : 0,
                render: (data, item) => item.sum + 'грн',
            },
            {
                title: 'Рейтинг',
                dataIndex: 'rating',
                key: 'rating',
                width: 80,
                ellipsis: true,
                sorter: true,
                render: (dat, data) => (
                    <span className="rating-order">
                        <img src={star} alt="" />
                        {data.userRating === 0 ? '5' : data.userRating}
                    </span>
                ),
            },
            {
                title: '',
                dataIndex: 'action',
                key: 'action',
                fixed: "right",
                width: 20,
                render: (evt, data) => (
                    <span className="table-action-btn">
                        <Icon type="right" />
                        <Icon type="arrow-right" />
                    </span>
                ),
            },
        ];
    },
    tradeHistoryColumn: [
        {
            title: 'ID угоди',
            dataIndex: 'id',
            key: 'id',
        },
        {
            title: 'Тип',
            dataIndex: 'type',
            key: 'type',
        },
        {
            title: 'Сировина',
            dataIndex: 'applicationData.category.name',
            key: 'category',
            render: (i, data) =>
                data.applicationData && data.applicationData.product.name,
        },
        {
            title: 'Ціна',
            dataIndex: 'price',
            key: 'price',
            render: (name, item) => item.price + 'грн',
        },
        {
            title: 'Обсяг',
            dataIndex: 'applicationData.purchaseVolume',
            key: 'volume',
            render: (name, item) =>
                item.applicationData &&
                item.applicationData.purchaseVolume +
                    item.applicationData.units.shortName,
        },
        {
            title: 'Всього',
            dataIndex: 'sum',
            key: 'sum',
            render: (data, item) => item.sum + 'грн',
        },
        {
            title: 'Відстань',
            dataIndex: 'distance',
            key: 'distance',
            render: (name, item) =>
                !item.distance ? (
                    <Icon type="loading" style={iconStyle} />
                ) : (
                    item.distance.text
                ),
        },
        {
            title: 'Вартість доставки',
            dataIndex: 'deliveryPrice',
            key: 'deliveryPrice',
            render: (i, data) => data.deliveryPrice + ' грн',
        },
        {
            title: 'Дата виконання',
            dataIndex: 'date',
            key: 'date',
            render: (a, data) =>
                data.closed
                    ? `${moment(data.closed).format('LT')} / ${moment(
                          data.closed,
                      ).format('L')}`
                    : '-',
        },
        {
            title: 'Оцінка якості',
            dataIndex: 'rating',
            key: 'rating',
            render: (dat, data) => (
                <span className="rating-order">
                    <img src={star} alt="" />
                    {!data.orderRating ? '5' : data.orderRating}
                </span>
            ),
        },
    ],
    activeContractsColumns: (onOrderSuccess, onOrderDecline) => {
        return [
            {
                title: 'Тип',
                dataIndex: 'type',
                key: 'type',
            },
            {
                title: 'Категорія',
                dataIndex: 'category',
                key: 'category',
            },
            {
                title: 'Ціна',
                dataIndex: 'price',
                key: 'price',
            },
            {
                title: 'Обсяг',
                dataIndex: 'amount',
                key: 'amount',
            },
            {
                title: 'Всього',
                dataIndex: 'total',
                key: 'total',
            },
            {
                title: 'Відстань',
                dataIndex: 'distance',
                key: 'distance',
            },
            {
                title: 'Доставка',
                dataIndex: 'funds',
                key: 'funds',
            },
            {
                title: 'Орієнт. доставка',
                dataIndex: 'date',
                key: 'date',
            },
            {
                title: 'Рахунок фактура',
                dataIndex: 'status',
                key: 'status',
            },
            {
                title: '',
                dataIndex: 'action',
                key: 'action',
                render: () => (
                    <span className="actions">
                        <button
                            onClick={onOrderSuccess}
                            className="btn btn-blue"
                        >
                            Виконати
                        </button>
                        <button
                            onClick={onOrderDecline}
                            className="btn btn-red"
                        >
                            Скасувати
                        </button>
                    </span>
                ),
            },
        ];
    },
    costsColumns: [
        {
            title: 'ID транзакції',
            dataIndex: 'id',
            key: 'id',
        },
        {
            title: 'Тип',
            dataIndex: 'type',
            key: 'type',
        },
        {
            title: 'Номер рахунку',
            dataIndex: 'number',
            key: 'number',
        },
        {
            title: 'Дата',
            dataIndex: 'creationDate',
            key: 'creationDate',
            render: (a, data) =>
                data.creationDate
                    ? `${moment(data.creationDate).format('LT')} / ${moment(
                          data.creationDate,
                      ).format('L')}`
                    : '-',
        },
        {
            title: 'Сума',
            dataIndex: 'amount',
            key: 'amount',
            render: (i, data) => data.amount + ' грн',
        },
    ],
    depositHistoryColumns: [
        {
            title: 'ID транзакції',
            dataIndex: 'id',
            key: 'id',
        },
        {
            title: 'Тип',
            dataIndex: 'type',
            key: 'type',
        },
        {
            title: 'Номер рахунку',
            dataIndex: 'number',
            key: 'number',
        },
        {
            title: 'Дата поповнення',
            dataIndex: 'creationDate',
            key: 'creationDate',
            render: (a, data) =>
                data.creationDate
                    ? `${moment(data.creationDate).format('LT')} / ${moment(
                          data.creationDate,
                      ).format('L')}`
                    : '-',
        },
        {
            title: 'Сума поповнення',
            dataIndex: 'amount',
            key: 'amount',
            render: (i, data) => data.amount + ' грн',
        },
    ],
    orderHistory: [
        {
            title: 'ID угоди',
            dataIndex: 'itemHistory.id',
            key: 'id',
            width: 100,
            ellipsis: true
        },

        {
            title: 'Тип',
            dataIndex: 'itemHistory.type',
            key: 'type',
            width: 100,
            ellipsis: true
        },
        {
            title: 'Форма',
            dataIndex: 'product.name',
            key: 'form',
            width: 120,
            ellipsis: true
        },
        {
            title: 'Сировина',
            dataIndex: 'rawMaterial.name',
            key: 'product',
            ellipsis: true,
            width: 100,

        },
        {
            title: 'Ціна',
            dataIndex: 'price',
            key: 'price',
            render: (i, data) => data.itemHistory.price + 'грн',
            ellipsis: true
        },
        {
            title: 'Обсяг',
            dataIndex: 'itemHistory.volume',
            key: 'volume',
            ellipsis: true
        },
        {
            title: 'Всього',
            dataIndex: 'total',
            key: 'total',

            render: (i, data) => data.itemHistory.total + 'грн',
            ellipsis: true
        },
        {
            title: 'Дата відкриття',
            dataIndex: 'itemHistory.opened',
            key: 'opened',
            ellipsis: true,
            render: (a, data) =>
                data.itemHistory.opened
                    ? `${moment(data.itemHistory.opened).format('LT')} / ${moment(
                          data.itemHistory.opened,
                      ).format('L')}`
                    : '-',
        },
        {
            title: 'Дата закриття',
            dataIndex: 'closed',
            key: 'closed',
            ellipsis: true,
            render: (a, data) =>
                data.itemHistory.closed
                    ? `${moment(data.itemHistory.closed).format('LT')} / ${moment(
                          data.itemHistory.closed,
                      ).format('L')}`
                    : '-',
        },
        {
            title: 'Відстань',
            dataIndex: 'itemHistory.distance',
            key: 'distance',
            ellipsis: true
        },
        {
            title: 'Статус',
            dataIndex: 'status',
            key: 'status',
            ellipsis: true,
            render: (i, data) => (
                <div style={{ color: statusColors[data.itemHistory.status.toLowerCase()] }}>
                    {data.itemHistory.status}
                </div>
            ),
        },
    ],
    adminsColumns: (onEditAdmin, onRemove, update) => [
        {
            title: 'Id',
            dataIndex: 'id',
            key: 'id',
            width: 150,
            render: (i, data) => <span>{data.id}</span>,
        },
        {
            title: 'Email',
            dataIndex: 'email',
            key: 'email',
            width: 200,
        },
        {
            title: 'Country',
            dataIndex: 'userCountry',
            key: 'userCountry',
            width: 200,
        },
        {
            title: '',
            dataIndex: 'actions',
            key: 'actions',
            width: 150,
            render: (e, item) => (
                <div className="action-btn">
                    <button
                        className="admin-btn btn-blue"
                        onClick={() =>
                            onEditAdmin('admins-update', null, {
                                ...item,
                                update,
                            })
                        }
                    >
                        Edit
                    </button>
                    <button
                        className="admin-btn btn-red"
                        onClick={() => onRemove(item.id)}
                    >
                        Delete
                    </button>
                </div>
            ),
        },
    ],
    contractsStatisticColumns: [
        {
            title: 'Не підтверджені',
            dataIndex: 'notVerified',
            key: 'notVerified',
        },
        {
            title: 'Підтверджені',
            dataIndex: 'verified',
            key: 'verified',
        },
        {
            title: 'Чекає оплати',
            dataIndex: 'waitPay',
            key: 'waitPay',
        },
        {
            title: 'Чекають відвантаження',
            dataIndex: 'waitShip',
            key: 'waitShip',
        },
        {
            title: 'Виконанні',
            dataIndex: 'closed',
            key: 'closed',
        },
        {
            title: 'Скасовані',
            dataIndex: 'cancelled',
            key: 'cancelled',
        },
    ],
    allContracts: [
        {
            title: 'ID угоди',
            dataIndex: 'id',
            key: 'id',
        },
        {
            title: 'Статус',
            dataIndex: 'status',
            key: 'status',
        },
        {
            title: 'Категорія',
            dataIndex: 'category',
            key: 'category',
            render: (e, data) => data.applicationData.category.name,
        },
        {
            title: 'Всього',
            dataIndex: 'sum',
            key: 'sum',
            render: (e, data) => data.sum + ' грн',
        },
        {
            title: 'Відстань',
            dataIndex: 'distance',
            key: 'distance',
            render: (e, data) => (data.distance ? data.distance.text : '-'),
        },
        {
            title: 'Вартість доставки',
            dataIndex: 'deliveryPrice',
            key: 'deliveryPrice',
            render: (e, data) => data.deliveryPrice + ' грн',
        },
        // {
        //     title: 'Рахунок-фактура',
        //     dataIndex: 'rah',
        //     key: 'rah',
        // },
        {
            title: 'Дата   створення',
            dataIndex: 'date',
            key: 'date',
            render: (e, data) =>
                data.creationDate
                    ? `${moment(data.creationDate).format('LT')} / ${moment(
                          data.creationDate,
                      ).format('L')}`
                    : '-',
        },
        {
            title: '',
            dataIndex: 'actions',
            key: 'actions',
            render: () => <Icon type="right" />,
        },
    ],
    adminCommissionColumns: [
        {
            title: 'ID контаргента',
            dataIndex: 'userId',
            key: 'userId',
        },
        {
            title: 'ID угоди',
            dataIndex: 'orderId',
            key: 'orderId',
        },
        {
            title: 'Тип',
            dataIndex: 'type',
            key: 'type',
        },
        {
            title: 'Дата транзакції',
            dataIndex: 'creationDate',
            key: 'creationDate',
            render: (i, data) =>
                data.creationDate
                    ? `${moment(data.creationDate).format('LT')} / ${moment(
                          data.creationDate,
                      ).format('L')}`
                    : '-',
        },
        {
            title: 'Всього',
            dataIndex: 'totalPriceForCommission',
            key: 'totalPriceForCommission',
            render: (i, data) => data.totalPriceForCommission + ' грн',
        },
        {
            title: 'Розмір комісії',
            dataIndex: 'amount',
            key: 'amount',
            render: (i, data) => data.amount + ' грн',
        },
    ],
    userAdminsColumns: [
        {
            title: 'Тип',
            dataIndex: 'type',
            key: 'type',
        },
        {
            title: 'Категорія',
            dataIndex: 'applicationData.category.name',
            key: 'name',
        },
        {
            title: 'Ціна',
            dataIndex: 'price',
            key: 'price',
            render: (i, data) => data.price + ' грн',
        },
        {
            title: 'Обсяг',
            dataIndex: 'applicationData.purchaseVolume',
            key: 'purchaseVolume',
            render: (e, data) =>
                data.applicationData.purchaseVolume +
                data.applicationData.units.shortName,
        },
        {
            title: 'Всього',
            dataIndex: 'sum',
            key: 'sum',
            render: (i, data) => data.sum + ' грн',
        },
        {
            title: 'Дата відкр',
            dataIndex: 'opened',
            key: 'opened',
            render: (i, data) =>
                data.opened
                    ? `${moment(data.opened).format('LT')} / ${moment(
                          data.opened,
                      ).format('L')}`
                    : '-',
        },
        {
            title: 'Зарезервовано',
            dataIndex: 'isOpen',
            key: 'isOpen',
            render: (e, data) =>
                data.isOpen ? 'Зерезервовано' : 'Незарезервовано',
        },
        // {
        //     title: 'Статус',
        //     dataIndex: 'date',
        //     key: 'date3',
        // },
    ],
    userAdminsContractsColumns: [
        {
            title: 'ID угоди',
            dataIndex: 'id',
            key: 'id',
        },
        {
            title: 'Статус',
            dataIndex: 'status',
            key: 'status',
        },
        {
            title: 'Категорія',
            dataIndex: 'applicationData.category.name',
            key: 'category',
        },
        {
            title: 'Всього',
            dataIndex: 'sum',
            key: 'sum',
            render: (i, data) => data.sum + ' грн',
        },
        {
            title: 'Відстань',
            dataIndex: 'distance.text',
            key: 'distance',
        },
        {
            title: 'Вартість доставки',
            dataIndex: 'deliveryPrice',
            key: 'deliveryPrice',
            render: (i, data) => data.deliveryPrice + ' грн',
        },
        // {
        //     title: 'Рахунок-фактура',
        //     dataIndex: 'date',
        //     key: 'date2',
        // },
        {
            title: 'Дата   створення',
            dataIndex: 'opened',
            key: 'opened',
            render: (i, data) =>
                data.opened
                    ? `${moment(data.opened).format('LT')} / ${moment(
                          data.opened,
                      ).format('L')}`
                    : '-',
        },
    ],
    userAdminsBalanceCol: [
        {
            title: 'Тип транзакції',
            dataIndex: 'type',
            key: 'type',
        },
        {
            title: 'ID Транзакціі',
            dataIndex: 'id',
            key: 'id',
        },
        {
            title: 'Номер рахунку',
            dataIndex: 'email',
            key: 'email',
        },
        {
            title: 'Дата транзакції',
            dataIndex: 'creationDate',
            key: 'creationDate',
            render: (i, data) =>
                data.creationDate
                    ? `${moment(data.creationDate).format('LT')} / ${moment(
                          data.creationDate,
                      ).format('L')}`
                    : '-',
        },
        {
            title: 'Сума',
            dataIndex: 'amount',
            key: 'amount',
            render: (i, data) => data.amount + ' грн',
        },
    ],
    openOrdersColumns: (declineOpenOrder, acceptOpenOrder, userEmail) => [
        {
            title: 'Тип',
            dataIndex: 'type',
            key: 'type',
            width: 80,
            ellipsis: true,
        },
        {
            title: 'Сировина',
            dataIndex: 'applicationData.product.name',
            key: 'category',
            width: 100,
            ellipsis: true,
        },
        {
            title: 'Ціна',
            dataIndex: 'price',
            key: 'price',
            ellipsis: true,
            width: 150,
            render: (a, data) => `${data.price} грн`,
        },
        {
            title: 'Обсяг',
            dataIndex: 'applicationData.purchaseVolume',
            key: 'amount',
            width: 150,
            ellipsis: true,
            render: (name, item) =>
                `${item.applicationData.purchaseVolume}  ${item.applicationData.units.shortName}`,
        },
        {
            title: 'Всього',
            dataIndex: 'sum',
            ellipsis: true,
            width: 150,

            key: 'total',
            render: (a, data) => `${data.sum} грн`,
        },
        {
            title: 'Дата відкриття',
            dataIndex: 'opened',
            key: 'opened',
            width: 200,
            // ellipsis: true,
            render: (a, data) =>
                `${moment(data.opened).format('LT')} / ${moment(
                    data.opened,
                ).format('L')}`,
        },
        {
            title: 'Зарезервовані кошти',
            dataIndex: 'commission',
            key: 'commission',
            width: 200,
            // ellipsis: true,
            render: (i, data) => data.commission + ' грн',
        },
        // {
        //     title: 'Статус',
        //     dataIndex: 'status',
        //     key: 'status',
        // },
        {
            title: '',
            dataIndex: 'action',
            key: 'action',
            fixed: "right",
            render: (x, data) => {
                return (
                    <div style={openStyle}>
                        <button
                            className="btn btn-red"
                            onClick={() => declineOpenOrder(data.id)}
                        >
                            Скасувати
                        </button>
                        {data.email === userEmail && (
                            <button
                                className="btn btn-blue"
                                onClick={() => acceptOpenOrder(data.id)}
                            >
                                Прийняти
                            </button>
                         )}
                    </div>
                );
            },
        },
    ],
};
