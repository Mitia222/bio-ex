import React from 'react';
import { Link } from 'react-router-dom';

import logo from '../assets/logo_bio.svg';
const nav = [
    // {
    //     title: 'Про нас',
    //     href: 'about',
    // },
    {
        title: 'Умови використання',
        href: 'UsingRights',
    },
    {
        title: 'Політика конфіденційності',
        href: 'support',
    },
    {
        title: 'Новини',
        href: 'news',
    },
];
const Footer = () => {
    return (
        <footer>
            <div className="container footer">
                <div>
                    <img src={logo} alt="" />
                </div>
                <nav className="footer__list">
                    {nav.map((item, index) => (
                        <Link key={index} to={item.href}>
                            {item.title}
                        </Link>
                    ))}
                </nav>
            </div>
        </footer>
    );
};

export default Footer;
