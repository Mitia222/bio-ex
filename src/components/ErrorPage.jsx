import React, { useState } from 'react';
import errorface from './../assets/img/robot.png';
import { Link } from 'react-router-dom';

const defaultErrorDescription = 'Сторінку не знайдено або була видаленно.';

const ErrorPage = ({
    statusCode = '404',
    statusText = defaultErrorDescription,
}) => {

    return (
        <div className="error-page">
            <div className="error-content">
                <h1>{statusCode}</h1>
                <p>{statusText}</p>
                <Link to={'/'} className="btn">
                    На головну
                </Link>
            </div>
            <img src={errorface} />
        </div>
    );
};

export default ErrorPage;
