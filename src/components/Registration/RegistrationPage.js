import React from 'react';
import { Registration } from '../../store/actions/user';
import { connect } from 'react-redux';
import RegistrationForm from './RegistrationForm';
import TopBlock from '../Login/TopBlock';
import TwoFactorAuth from '../TwoFactorAuth';
import { getUserProfile } from '../../store/actions/user';

const RegistrationPage = ({
    Registration,

    history,
    user: { loading, },
}) => {

    return (
        <div className="registration-page separate-form">
            <TopBlock title="Реєстрація" />
            <RegistrationForm
                loading={loading}
                registration={Registration}
                history={history}
            />
        </div>
    );
    // }
};

const mapStateToProps = state => {
    return {
        user: state.user,
    };
};

const mapDispatchToProps = dispatch => {
    return {
        Registration: (data, history) => dispatch(Registration(data, history)),
        getUserProfile: () => dispatch(getUserProfile()),
    };
};

export default connect(
    mapStateToProps,
    mapDispatchToProps,
)(RegistrationPage);
