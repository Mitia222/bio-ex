import React, { useEffect, useRef, useState } from 'react';
import { RegistrationSchema } from '../../schema/user';
import { withFormik, Form } from 'formik';
import CustomSelect from '../CustomSelect';
import Input from '../Input';
import Recaptcha from 'react-recaptcha';
import { NavLink } from 'react-router-dom';
import roles from '../../constants/userRoles';

const RegistrationForm = ({ errors, setFieldValue, values, loading }) => {
    const captchaRef = useRef(null);

    useEffect(() => {
        captchaRef.current.reset();
    }, []);
    return (
        <Form>
            <Input
                handleChange={(e) => setFieldValue('username', e.target.value)}
                label="Ваше ім'я"
                placeholder="Ваше ім'я"
                name="username"
                errors={errors.username}
            />
            <CustomSelect
                label="Роль користувача"
                handleChange={(value) => setFieldValue('userRole', value)}
                placeholder="Виберіть роль"
                options={roles}
                value={values.userRole}
            />
            <Input
                label="Email"
                type="email"
                name="email"
                handleChange={(e) => setFieldValue('email', e.target.value)}
                placeholder="Наприклад: example@gmail.com"
                errors={errors.email}
            />
            <Input
                label="Пароль"
                type="password"
                name="password"
                handleChange={(e) => setFieldValue('password', e.target.value)}
                placeholder="******"
                errors={errors.password}
            />
            <Input
                label="Підтвердження пароля"
                type="password"
                name="repeatPassword"
                handleChange={(e) =>
                    setFieldValue('repeatPassword', e.target.value)
                }
                placeholder="******"
                errors={errors.repeatPassword}
            />
            <Input
                label="Телефон"
                type="phone"
                name="phone"
                handleChange={(e) => setFieldValue('phone', e.target.value)}
                placeholder="+380982992323"
                errors={errors.phone}
            />

            <div className="confirm">
                <Input
                    label="Я приймаю умови конфіденційності"
                    className="checkbox"
                    labelOnclick={() => window.open('/support')}
                    type="checkbox"
                    name="permission"
                    handleChange={(e) =>
                        setFieldValue('permission', e.target.checked)
                    }
                    errors={errors.permission}
                />

                <Recaptcha
                    ref={captchaRef}
                    sitekey="6LeHnuMUAAAAAPsx_PucUJkJ0LIzo3esvyocC6SR"
                    render="explicit"
                    verifyCallback={(e) => setFieldValue('captch', true)}
                />
                {errors.captch && (
                    <div className="inputs-error-text">{errors.captch}</div>
                )}
            </div>

            <button
                // onClick={() => validateForm().then(validate)}
                type="submit"
                className="btn authentication-action-btn"
                disabled={loading}
            >
                Зарeєструватися
            </button>

            <div className="go-to-login">
                <NavLink to="/login">Я вже зареєстрованний</NavLink>
            </div>
        </Form>
    );
};

export default withFormik({
    mapPropsToValues: () => ({
        email: '',
        password: '',
        repeatPassword: '',
        username: '',
        permission: false,
        captch: false,
        userRole: 'BS',
        phone: '',
        onChangeTwoFactor: false,
        // country: '',
    }),
    handleSubmit: (values, { setErrors, props: { registration, history } }) => {
        if (values.password !== values.repeatPassword) {
            setErrors({
                password: 'Паролі не співпадають',
                repeatPassword: 'Паролі не співпадають',
            });
            return;
        }
        registration(
            {
                email: values.email,
                firstName: values.username,
                confirmPassword: values.repeatPassword,
                password: values.password,
                phone: values.phone,
                twoFactorAuthEnabled: values.onChangeTwoFactor,
                userRole: values.userRole,
                // userCountry: values.country,
            },
            // history,
        );
    },

    validationSchema: RegistrationSchema,
    displayName: 'RegistrationForm',
})(RegistrationForm);
