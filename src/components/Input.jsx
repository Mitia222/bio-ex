import React, { memo,  useState } from 'react';

const Input = memo(
    ({
        label,
        handleChange,
        value,
        name,
        type,
        disabled,
        className,
        placeholder,
        step,
        min,
        max,
        notify,

        labelOnclick,
        errors,
        withCached,
    }) => {
        const [isReadable, setReadable] = useState(true);
        const onFocus = () => isReadable && setReadable(false)
        return (
            <div className={`form-item ${className ? className : ''}`}>
                <label onClick={labelOnclick ? labelOnclick : null}>
                    {label}
                </label>
                <input
                    min={min}
                    max={max}
                    step={step}
                    disabled={disabled}
                    type={type || 'text'}
                    name={name}
                    value={value}
                    onFocus={onFocus}
                    placeholder={placeholder}
                    onChange={handleChange}
                    className={errors ? 'error-class' : ''}
                    readOnly={isReadable}
                />
                {errors && <div className="inputs-error-text">{errors}</div>}
                {notify && <div>{notify}</div>}
            </div>
        );
    },
);

export default Input;
