import React from 'react';
import { withFormik } from 'formik';
import { LoginSchema } from '../../schema/user';
import { Form } from 'formik';
import { NavLink } from 'react-router-dom';
import Input from '../Input';
import Recaptch from '../Recaptcha';

const LoginForm = ({
    errors,
    setFieldValue,
    loading,
}) => {

    return (
        <Form>
            <Input
                label="Ваш Email"
                type="email"
                name="email"
                handleChange={(e) =>
                    setFieldValue('email', e.target.value)
                }
                placeholder="Наприклад: example@gmail.com"
                errors={errors.email}
            />
            <Input
                label="Ваш пароль"
                type="password"
                name="password"
                handleChange={(e) =>
                    setFieldValue('password', e.target.value)
                }
                placeholder="*******"
                errors={errors.password}
            />

            <div className="go-to-reset-pas">
                <NavLink to="/reset_password">Забули пароль?</NavLink>
            </div>
            <Recaptch  verifyCallback={(e) => setFieldValue('captch', true)} />
            {errors.captch && (
                <div className="inputs-error-text">{errors.captch}</div>
            )}
            <button
                type="submit"
                className="btn authentication-action-btn"
                disabled={loading}
            >
                Увійти
            </button>
            <div className="go-to-login">
                <NavLink to="/registration">
                    У мене ще немає облікового запису
                </NavLink>
            </div>
        </Form>
    );
};

export default withFormik({
    mapPropsToValues: () => ({
        email: '',
        password: '',
        captch: false,
    }),

    handleSubmit: (values, { props: { login } }) => {

        login({ email: values.email, password: values.password });
    },
    validationSchema: LoginSchema,
    displayName: 'LoginForm',
})(LoginForm);
