import React from 'react';
import { Modal } from 'antd';
import { connect } from 'react-redux';
import { toogleModal } from '../store/actions/modal';
import DialogWindowHandler from '../containers/UserSide/Exchange/dialogWindows/DialogWindowsHandler';
import { putCurrentChoosedPoduct } from '../store/actions/Users/ordersActionsCreators';
import { putTwoFactor } from '../store/actions/user';

const MyModal = ({ toogleModal, modal }) => {
    const handleCloseModal = () => {
        toogleModal({ type: modal.type, isOpen: false, data: null });
    };

    return (
        <Modal
            wrapClassName={
                modal.type === 'show-balance'
                    ? 'balance-modal'
                    : 'default-modal'
            }
            footer={null}
            visible={modal.isOpen}
            onCancel={() => {
                if (modal.type === 'two-factor') {
                } else {
                    handleCloseModal();
                }
            }}
        >
            <DialogWindowHandler
                isOpen={modal.isOpen}
                modalState={modal.type}
                data={modal.data}
                handleCloseModal={handleCloseModal}
            />
        </Modal>
    );
};

const mapStateToProps = (state) => ({
    modal: state.modal,
    user: state.user,
});

const mapDispatchToProps = (dispatch) => ({
    toogleModal: (data) => dispatch(toogleModal(data)),
    putCurrentChoosedPoduct: (val) => dispatch(putCurrentChoosedPoduct(val)),
    initiateTwoFactorAuth: (payload) => dispatch(putTwoFactor(payload)),
});

export default connect(mapStateToProps, mapDispatchToProps)(MyModal);
