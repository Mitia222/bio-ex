import React, { useState } from 'react';
import api from '../sevices/users/userRequests';
import { notification, Icon } from 'antd';
import { connect } from 'react-redux';
import { reset } from '../store/actions/user';

const TwoFactorAuth = ({
    history,
    imageBase64 = '',
    email,
    type,
    loginValidating,
    handleCloseModal,
    resetStep,
}) => {
    const [totp, setTotp] = useState('');
    const handleChange = (e) => {
        setTotp(+e.target.value);
    };
    const handleBack = () => {
        resetStep();
        window.history.back();
    };
    const validateQrCodeInRegistration = async () => {
        if (!totp) {
            notification.error({
                message: 'Невірний код! Спробуйте ще раз.',
            });
            return;
        }
        try {
            await api.validateQrCode({
                token: String(totp),
                email,
                step: type,
            });
            if (type === 'register') {
                resetStep();
                notification.success({
                    title: 'Дякуємо за регістрацію!',
                    message:
                        'Код активацій аккаунту був відісланий на ваш e-mail!',
                });
                history.push('/login');
            } else {
                handleCloseModal();
                notification.success({
                    message: 'Успішно!',
                });
            }
        } catch (e) {
            if (e.response && e.response.status === 409) {
                notification.error({
                    message: 'Невірний код! Спробуйте ще раз.',
                });
            }
        }
    };
    return (
        <div className="qr-code-block">
            <hr />
            {!loginValidating && (
                <div
                    style={{ left: 38 }}
                    className="go-back-btn"
                    onClick={handleBack}
                >
                    <Icon type="left" />
                    Назад
                </div>
            )}

            <div className="description">
                {!loginValidating
                    ? 'Будь ласка відскануйте код за допомогою додатка Google\n' +
                      '                authenticator на вашому смартфоні'
                    : 'Будь ласка введіть код в вашому аутентифікації у вашому Google authenticator додатку для подальшої аутентифікації у системі!'}
            </div>
            {!loginValidating && (
                <div className="qrCode">
                    <img
                        alt="qrcode"
                        src={`data:image/png;base64,${imageBase64}`}
                    />
                </div>
            )}

            <hr />

            <div className="enter-code">
                Введіть отриманий код із додатка Google authenticator
            </div>

            <div>
                <div className="form-item" s>
                    <input
                        type="number"
                        placeholder="Шестизначний код"
                        value={totp}
                        onChange={handleChange}
                    />
                </div>

                <button
                    type="button"
                    className="btn authentication-action-btn"
                    onClick={validateQrCodeInRegistration}
                >
                    Увійти
                </button>
            </div>
        </div>
    );
};

const mapStateToProps = (state) => ({
    imageBase64: state.user.imageBase64,
    email: state.user.email,
});

const mapDispatchToProps = (dispatch) => ({
    resetStep: () => dispatch(reset()),
});
export default connect(mapStateToProps, mapDispatchToProps)(TwoFactorAuth);
