import React, { useRef, useEffect } from 'react';
import Recaptcha from 'react-recaptcha';

const Recaptch = ({ verifyCallback, className }) => {
    const captchaRef = useRef(null);
    useEffect(() => {
        captchaRef.current.reset();
    }, []);
    return (
        <div className={`recaptcha ${className || ''}`}>
            <Recaptcha
                ref={captchaRef}
                sitekey="6LeClsQZAAAAAAF_ZUBkQmwKyzNKpztw2gyFJcfk"
                render="explicit"
                // onloadCallback={this.onLoadRecaptcha}
                verifyCallback={verifyCallback}
            />
        </div>
    );
};

export default Recaptch;
