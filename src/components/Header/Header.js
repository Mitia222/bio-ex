import React from 'react';
import { Link } from 'react-router-dom';
import logo from '../../assets/logo_bio.svg';
import UserHeader from './UserHeader';
import '../../styles/header.scss';
import { useSelector } from 'react-redux';

const Header = ({ roles }) => {
    useSelector((state) => state.router);
    return (
        <div className="header">
            <div className="container header__flex">
                <Link to="/">
                    <img className="logo" src={logo} alt='iamges' />
                </Link>
                {roles === 'user' ? (
                    <UserHeader />
                ) : (
                    <>
                        <div className="mobile-login">
                            <Link to="/login" className="mobile-login-link">
                                Увійти
                            </Link>
                            <svg
                                width="24"
                                height="24"
                                viewBox="0 0 24 24"
                                fill="none"
                                xmlns="http://www.w3.org/2000/svg"
                            >
                                <path
                                    d="M9 2H18C19.1 2 20 2.9 20 4V20C20 21.1 19.1 22 18 22H9C7.9 22 7 21.1 7 20V18H9V20H18V4H9V6H7V4C7 2.9 7.9 2 9 2Z"
                                    fill="#43BC78"
                                />
                                <path
                                    d="M10.09 15.59L11.5 17L16.5 12L11.5 7L10.09 8.41L12.67 11H3V13H12.67L10.09 15.59Z"
                                    fill="#43BC78"
                                />
                            </svg>
                        </div>

                        <Link to="/login" className="btn-base inverted">
                            Увійти
                        </Link>
                    </>
                )}
            </div>
        </div>
    );
};

export default Header;
