import React from 'react';
import { NavLink } from 'react-router-dom';

const GuestHeader = () => {
    return (
        <nav>
            <NavLink activeClassName="active-link" to="/support">
                Підтримка
            </NavLink>
            <NavLink activeClassName="active-link" to="/news">
                Новини
            </NavLink>

            <div className="authentication-link">
                <NavLink to="/registration" className="registration-link">
                    Реєстрація
                </NavLink>

                <NavLink to="/login" className="btn">
                    Увійти
                </NavLink>
            </div>
        </nav>
    );
};

export default GuestHeader;
