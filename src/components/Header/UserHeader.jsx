import React, { Fragment } from 'react';
import { NavLink } from 'react-router-dom';
import { Icon } from 'antd';
import { slide as Menu } from 'react-burger-menu';
const UserHeader = () => {
    return (
        <Fragment>
            <NavLink
                to="/exchange"
                className="btn-base filled excheange-btn"
                style={{ marginLeft: 100 }}
            >
                Перейти до біржі
            </NavLink>

            <nav className="header__user-nav">
                <NavLink to="/contracts" activeClassName="active-link">
                    Угоди
                </NavLink>
                <NavLink to="/balance" activeClassName="active-link">
                    Баланс
                </NavLink>

                <NavLink
                    className="profile-link"
                    activeClassName="active-link"
                    to="/profile"
                >
                    <Icon type="user" />
                    Профіль
                </NavLink>
                {/*<NavLink to="/accounts" activeClassName="active-link">*/}
                {/*    Баланс*/}
                {/*</NavLink>*/}
                {/*<NavLink activeClassName="active-link" to="/orders">*/}
                {/*    Замовлення*/}
                {/*</NavLink>*/}
            </nav>

            <Menu
                isOpen={false}
                right

                customBurgerIcon={
                    <Icon type="menu" className="burger-menu-btn" />
                }
            >
                <div className="menu__mobile">
                    <NavLink
                        to="/exchange"
                        className="btn-base filled"

                    >
                        Перейти до біржі
                    </NavLink>
                    <NavLink to="/contracts" activeClassName="active-link">
                        Угоди
                    </NavLink>
                    <NavLink to="/balance" activeClassName="active-link">
                        Баланс
                    </NavLink>

                    <NavLink
                        className="profile-link"
                        activeClassName="active-link"
                        to="/profile"
                    >
                        <Icon type="user" />
                        Профіль
                    </NavLink>
                </div>
            </Menu>
        </Fragment>
    );
};
export default UserHeader;
