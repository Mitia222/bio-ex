import React from 'react';
import { Spin, Icon } from 'antd';

const antIcon = (
    <Icon type="loading" style={{ fontSize: 56, color: '#1fe544' }} spin />
);

const Preloader = props => {
    const classname = props.inner ? 'preloader-spin inner' : 'preloader-spin';
    return (
        <div className={classname}>
            <Spin indicator={antIcon} />
        </div>
    );
};

export default Preloader;
