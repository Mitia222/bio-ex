import React, { Component } from 'react';
import { Icon } from 'antd';
import Recaptcha from 'react-recaptcha';
import api from '../sevices/users/userRequests';
import logo from '../assets/img/logo_head.svg';
import { notification } from 'antd';
import { Formik, Form, Field } from 'formik';
import { ChangePasswordSchema } from '../schema/user';

class ChangePasswordPage extends Component {
    state = {
        captcha: false,
    };

    validateForm = e => {
        console.log(e);
        if (e.oldPassword || e.confirmPassword || e.newPassword) {
            notification.error({
                message: e.oldPassword || e.confirmPassword || e.newPassword,
            });
        }
    };
    onSubmitForm = async values => {
        if (values.newPassword !== values.confirmPassword) {
            notification.error({
                message: 'Паролі не співпадають!',
            });
            return;
        } else if (!this.state.captcha) {
            notification.error({
                message: 'Заповніть Captch!',
            });
            return;
        } else {
            try {
                const response = await api.ChangePassword(values);
                if (response.status === 200) {
                    notification.success({
                        message: 'Пароль змінено!',
                    });
                    this.props.history.push('/profile');
                }
            } catch (err) {
                console.log('Change PW ERROR', err);
                if (err.response) {
                    notification.error({
                        message: err.response
                            ? err.response.data[
                                  Object.keys(err.response.data)[0]
                              ]
                            : null,

                        description: 'Спробуйте ще раз!',
                    });
                }
            }
        }
    };
    verifyCallback = rest => {
        this.setState({ captcha: true }, () => console.log(this.state));
    };
    render() {
        return (
            <div className="reset-password-page separate-form">
                <div
                    className="go-back-btn"
                    onClick={() => window.history.back()}
                >
                    <Icon type="left" />
                    Назад
                </div>

                <div className="logo">
                    <img src={logo} alt="" />
                </div>

                <div className="form-title">
                    <hr />
                    <span>Зміна пароля</span>
                    <hr />
                </div>
                <Formik
                    initialValues={{
                        oldPassword: '',
                        newPassword: '',
                        confirmPassword: '',
                    }}
                    validationSchema={ChangePasswordSchema}
                    onSubmit={this.onSubmitForm}
                >
                    {({ errors, touched, validateField, validateForm }) => (
                        <Form>
                            <div className="form-item">
                                <label htmlFor="">Актуальний пароль</label>
                                <Field
                                    type="password"
                                    placeholder="**************"
                                    name="oldPassword"
                                    // onChange={this.handleInput}
                                />
                            </div>

                            <div className="form-item">
                                <label htmlFor="">Новий пароль</label>
                                <Field
                                    type="password"
                                    placeholder="**************"
                                    name="newPassword"
                                    // onChange={this.handleInput}
                                />
                            </div>

                            <div className="form-item">
                                <label htmlFor="">Повторіть новий пароль</label>
                                <Field
                                    type="password"
                                    placeholder="**************"
                                    name="confirmPassword"
                                    // onChange={this.handleInput}
                                />
                            </div>

                            <div className="recaptcha">
                                <Recaptcha
                                    sitekey="6LeHnuMUAAAAAPsx_PucUJkJ0LIzo3esvyocC6SR"
                                    render="explicit"
                                    verifyCallback={this.verifyCallback}
                                />
                            </div>

                            <button
                                onClick={() =>
                                    validateForm().then(this.validateForm)
                                }
                                type="submit"
                                className="btn authentication-action-btn"
                            >
                                Змінити пароль
                            </button>
                        </Form>
                    )}
                </Formik>
            </div>
        );
    }
}

export default ChangePasswordPage;
