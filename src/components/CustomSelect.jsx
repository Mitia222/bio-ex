import React from 'react';
import { Select, Tooltip } from 'antd';

const { Option } = Select;

const CustomSelect = ({
    handleChange,
    value,
    options,
    disabled,
    label,
    placeholder,
    allowClear,
    tooltipTitle = '',
    defaultValue,
    errors,
}) => {
    const select = () => (
        <div className="form-item">
            <label htmlFor="">{label}</label>
            <Select
                allowClear={allowClear}
                value={value}
                placeholder={placeholder}
                onChange={handleChange}
                disabled={disabled}
                defaultValue={defaultValue}
                className={errors ? "error-class" : ""}
                // defaultValue={'Обрати'}
                // unselectable="off"
            >
                {options
                    ? options.map(({ id, name, disabled = false }) => (
                          <Option
                              key={id || name}
                              value={id || name}
                              disabled={disabled}
                          >
                              {name}
                          </Option>
                      ))
                    : []}
            </Select>
            {errors && <div className="inputs-error-text">{errors}</div>}
        </div>
    );

    return !disabled ? (
        select()
    ) : (
        <Tooltip title={tooltipTitle}>{select()}</Tooltip>
    );
};

export default CustomSelect;
