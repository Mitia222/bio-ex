import React from 'react';

const Checkbox = ({ label, handleChange, checked }) => {
    return (
        <div className="form-item check">
            <label>{label}</label>
            <label className="container2">
                <input
                    type="checkbox"
                    checked={checked}
                    onChange={handleChange}
                />
                <span className="checkmark"></span>
            </label>
        </div>
    );
};
export default Checkbox;
