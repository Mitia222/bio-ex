import React, { useState, useEffect, useMemo } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { fetchCommission } from '../store/actions/Users/commission';
import { getDelivery } from '../store/actions/Users/delivery';
import { initSocket } from '../store/actions/Users/orders';
import { fetchAllProductsData } from '../store/actions/productsandfilters';
import PublicLayout from '../Layouts/PublicLayout';
import UserLayout from '../Layouts/UserLayout';
import AdminLayout from '../Layouts/AdminLayout';
import { fetchNews } from '../store/actions/Users/news';

const useInitApp = () => {
    const [role, setLoader] = useState(false);
    const dispatch = useDispatch();
    const user = useSelector((state) => state.user);
    useEffect(() => {
        const userRole = localStorage.getItem('TOKEN');
        const adminRole = localStorage.getItem('ADMINTOKEN');

        if (!Boolean(userRole) && !Boolean(adminRole)) {
            setLoader('guest');
            console.log('asd', );
            dispatch(fetchNews());
        }

        if (user.authorized) {
            dispatch(fetchCommission());
            dispatch(getDelivery());
            dispatch(fetchAllProductsData());
            if (user.isStaff) {
                setLoader('admin');
            } else {
                dispatch(fetchNews());
                dispatch(initSocket());
                setLoader('user');
            }
        }
    }, [user.authorized]);
    return [role];
};

const InitApp = ({ children }) => {
    const [role] = useInitApp();

    return useMemo(
        () =>
            role === 'guest' ? (
                <PublicLayout> {children(role)} </PublicLayout>
            ) : role === 'user' ? (
                <UserLayout>{children(role)}</UserLayout>
            ) : role === 'admin' ? (
                <AdminLayout>{children(role)}}</AdminLayout>
            ) : null,
        [role],
    );
};

export default InitApp;
