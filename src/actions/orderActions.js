import api from './request';
import { ORDERS, MY_OPEN_ORDERS } from '../constants/APIURLS';

export const getOrders = () => {
    return api('get', ORDERS);
};

export const getMyOpenOrders = () => {
    return api('get', MY_OPEN_ORDERS);
};
