import api from './request';

import { LOGIN, REGISTRATION, COUNTRIES, REGIONS } from '../constants/APIURLS';

export const login = user => {
    return api('post', LOGIN, user).then(res => {
        sessionStorage.setItem('token', res.token);
    });
};

export const registration = user => {
    return api('post', REGISTRATION, user);
};

export const getCountries = () => {
    return api('get', COUNTRIES);
};

export const getRegions = () => {
    return api('get', REGIONS);
};
