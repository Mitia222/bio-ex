import React, { Suspense } from 'react';
import Preloader from './components/Preloader';
import MyModal from './components/Modal';
import { userRoutes, adminRoutes } from './routing/routes';
import { Route, Switch } from 'react-router-dom';
import MemoizeRoute from './hocs/memoizeRoute';
import useInitApp from './Hooks/useInitApp';
import withTransition from './hocs/withTransition';

const AnimatedComponent = withTransition(MemoizeRoute);

const routListMap = { guest: userRoutes, user: userRoutes, admin: adminRoutes };

const App = () => {
    const [Layout, role] = useInitApp();
    // console.log('role', role);
    if (!role) return null;
    return (
        <div className="App">
            <Layout>
                <Suspense fallback={<Preloader inner />}>
                    <Switch>
                        {routListMap[role].map(({ path, exact, component }) => (
                            <Route
                                key={path}
                                exact={exact}
                                render={(props) => (
                                    <AnimatedComponent
                                        component={component}
                                        {...props}
                                    />
                                )}
                                path={path}
                            />
                        ))}
                    </Switch>
                </Suspense>
            </Layout>
            <MyModal />
        </div>
    );
};

export default App;
